<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Sunspikes\\Ratelimit\\' => array($vendorDir . '/sunspikes/php-ratelimiter/src'),
    'Predis\\' => array($vendorDir . '/predis/predis/src'),
    'PhpAmqpLib\\' => array($vendorDir . '/php-amqplib/php-amqplib/PhpAmqpLib'),
    'Firebase\\JWT\\' => array($vendorDir . '/firebase/php-jwt/src'),
    'Desarrolla2\\Test\\Cache\\' => array($vendorDir . '/desarrolla2/cache/test'),
    'Desarrolla2\\Cache\\' => array($vendorDir . '/desarrolla2/cache/src'),
);
