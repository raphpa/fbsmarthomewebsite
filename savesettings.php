<?php
	if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		return;
	}

    $basedir = realpath(__DIR__);
    
	require_once($basedir . '/fritzbox/auxiliary.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
	require_once($basedir . '/fritzbox/postgresql.inc.php');
    require_once($basedir . '/alexa/oauth.inc.php');
    require_once($basedir . '/alexa/eventgateway.inc.php');
    require_once($basedir . '/assistant/homegraph.inc.php');

    header("X-Robots-Tag: noindex", true);
        
    session_set_cookie_params(0, "/", "", true, true);
	session_start();

	if(!isset($_SESSION['customerId']) || filter_input(INPUT_POST,"customerId",FILTER_SANITIZE_STRING) != $_SESSION['customerId'])
	{
		echo "Amazon ID passt nicht zur Session. Bitte erneut einloggen.";
		return;
	}
	
	if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "delete") {
        
        // Get current db customer data
        $customer = getCustomerRDS($_SESSION['customerId']);
            
        // If customer is using Alexa, send add/update to event gateway
        if(!empty(trim($customer->AccessToken)))
        {
            $url = "";
            $sendUpdate = true;
            
            // Check if access token is still valid. If not, try to update first
            if($customer->TokenValidity < time())
            {
                $result = refreshAccessToken($customer);
                
                if($result == null)
                {
                    // Updating token failed, stop sending update
                    $sendUpdate = false;
                }
                else
                {
                    // Else update customer
                    $customer = $result;
                }           
            }
            
            // If still sending update, get customer region
            if($sendUpdate)
            {
                
                // Check for region
                if(trim($customer->UserRegion) === "EU")
                {
                    $url = "https://api.eu.amazonalexa.com/v3/events";
                }
                elseif(trim($customer->UserRegion) === "NA")
                {
                    $url = "https://api.amazonalexa.com/v3/events";
                }
                else
                {
                    $sendUpdate = false;
                }
            }

            // Send delete message to event gateway
            if($sendUpdate)
            {
                $header = array(
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $customer->AccessToken
                );
                
                $data = new AlexaDeleteReport($customer, new Customer());
                $jsonData = json_encode($data);
                
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 

                if(!empty($data->event->payload->endpoints))
                {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                    $result = curl_exec($ch);
                }
                
                // If event gateway returns 401 Unauthorized, try updating the access token and try again
                if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 401)
                {
                    $result = refreshAccessToken($customer);
                    
                    if($result == null)
                    {
                        // Updating token failed, stop sending update
                        $sendUpdate = false;
                    }
                    else
                    {
                        // Else update customer
                        $customer = $result;
                    }

                    if($sendUpdate)
                    {
                        $header = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $customer->AccessToken
                        );
                        
                        $data = new AlexaDeleteReport($customer, new Customer());
                        $jsonData = json_encode($data);
                        
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                        $result = curl_exec($ch);
                    }
                }
 
                curl_close($ch);
            }            
        }
        
        // Delete data
        $_SESSION['customer'] = new Customer();
        $_SESSION['customer']->CustomerId = $customer->CustomerId;

        $deleted = deleteCustomerRDS($_SESSION['customer']);
                
        // If customer is google assistant user, tell assistant to update devices
        if($customer != null && $customer->IsGoogleAssistant)
        {
            $url = 'https://homegraph.googleapis.com/v1/devices:requestSync?key=' . $_SERVER['GOOGLE_API_KEY'];
            
            $data = array(
              'agent_user_id' => $_SESSION['customer']->CustomerId
            );
            $jsonData = json_encode($data);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
            $result = curl_exec($ch);
            curl_close($ch);
        }

		if($deleted)
		{
			echo "OK";
		}
		else
		{
			echo "NOK";
		}
		return;
	}
	
	if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "setAdditionalSwitch") {
		$switchId = filter_input(INPUT_POST,"switchId",FILTER_SANITIZE_STRING);
		$status = filter_input(INPUT_POST,"status",FILTER_SANITIZE_STRING);
		 
		if ($_SESSION['customer']->hasDevice($switchId))
		{
            foreach($_SESSION['customer']->DeviceList as $key => $device) {
                if($switchId == $device->Identifier)
                {
                    $_SESSION['customer']->DeviceList[$key]->AdditionalSwitch = ($status == "true");
                }
            }

            echo "OK";
		}
		else
        {
            echo "NOK";
        }
		
		return;
	}
    
	if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "setTemperature") {
        $selector = filter_input(INPUT_POST,"selector",FILTER_SANITIZE_STRING);
		$thermostatId = filter_input(INPUT_POST,"thermostatId",FILTER_SANITIZE_STRING);
        $valueOff = floatval(filter_input(INPUT_POST,"valueOff",FILTER_SANITIZE_NUMBER_INT));
		$valueOn = floatval(filter_input(INPUT_POST,"valueOn",FILTER_SANITIZE_NUMBER_INT));
		
        $returnValues['identifier'] = $thermostatId;
        $returnValues['off'] = 0;
        $returnValues['on'] = 0;
        
        if($_SESSION['customer']->hasDevice($thermostatId))
        {
            foreach($_SESSION['customer']->DeviceList as $key => $device) {
                if($thermostatId == $device->Identifier)
                {
                    $_SESSION['customer']->DeviceList[$key]->TemperatureOff = $valueOff;
                    $_SESSION['customer']->DeviceList[$key]->TemperatureOn = $valueOn;
                    
                    $returnValues['off'] = $valueOff;
                    $returnValues['on'] = $valueOn;
                }
            }
        }

		echo json_encode($returnValues);
        return;
	}
	
	if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "save") {

        // Get current db customer data
        $customer = getCustomerRDS($_SESSION['customerId']);
 
        // Copy over assistant flag
        $_SESSION['customer']->IsGoogleAssistant = $customer->IsGoogleAssistant;

        // Remove connections which are not configured
        foreach($_SESSION['customer']->ConnectionList as $key => $connection)
        {
            if($connection->LastConnectionState == CONN_NOTCONFIGURED)
            {
                $_SESSION['customer']->removeDevices($connection->Number);
                unset($_SESSION['customer']->ConnectionList[$key]);
            }
        }
        $_SESSION['customer']->ConnectionList = array_values($_SESSION['customer']->ConnectionList );
        
        $sendUpdateMessage = false;
        $sendDeleteMessage = false;
        
        // check if new devices have been added. If not, do not send update
        foreach($_SESSION['customer']->DeviceList as $device)
        {
            $result = $customer->hasDevice($device->Identifier);
            // if device is not in old device list, send update message
            if($result == false)
            {
                $sendUpdateMessage = true;
                break;
            }
            if($_SESSION['customer']->hasVirtual($device->Identifier) == true && $customer->hasVirtual($device->Identifier) == false)
            {
                $sendUpdateMessage = true;
                break;
            }
        }
        // check if new devices have been deleted. If not, do not send update
        foreach($customer->DeviceList as $device)
        {
            $result = $_SESSION['customer']->hasDevice($device->Identifier);
            // if device is not in current device list, send delete message
            if($result == false)
            {
                $sendDeleteMessage = true;
                break;
            }
            if($customer->hasVirtual($device->Identifier) == true && $_SESSION['customer']->hasVirtual($device->Identifier) == false)
            {
                $sendDeleteMessage = true;
                break;
            }
        }
        
        $sendUpdate = false;
        if($sendUpdateMessage || $sendDeleteMessage)
        {
            $sendUpdate = true;
        }           
        
        // If customer is using Alexa, send add/update to event gateway
        if($sendUpdate && !empty(trim($_SESSION['customer']->AccessToken)))
        {
            // Check if access token is still valid. If not, try to update first
            if($_SESSION['customer']->TokenValidity < time())
            {
                $result = refreshAccessToken($_SESSION['customer']);
                
                if($result == null)
                {
                    // Updating token failed, stop sending update
                    $sendUpdate = false;
                }
                else
                {
                    // Else update customer
                    $_SESSION['customer'] = $result;
                }           
            }
            
            $url = "";
            // If still sending update, get customer region
            if($sendUpdate)
            {
                
                // Check for region
                if(trim($_SESSION['customer']->UserRegion) === "EU")
                {
                    $url = "https://api.eu.amazonalexa.com/v3/events";
                }
                elseif(trim($_SESSION['customer']->UserRegion) === "NA")
                {
                    $url = "https://api.amazonalexa.com/v3/events";
                }
                else
                {
                    $sendUpdate = false;
                }
            }
            
            // If still sending update, send add/update message to event gateway
            if($sendUpdate && $sendUpdateMessage)
            {
                $header = array(
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $_SESSION['customer']->AccessToken
                );
                
                $data = new AlexaAddOrUpdate($_SESSION['customer']);
                $jsonData = json_encode($data);
                
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_POST, true);  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
                $result = curl_exec($ch);
                
                // If event gateway returns 401 Unauthorized, try updating the access token and try again
                if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 401)
                {
                    $result = refreshAccessToken($_SESSION['customer']);
                    
                    if($result == null)
                    {
                        // Updating token failed, stop sending update
                        $sendUpdate = false;
                    }
                    else
                    {
                        // Else update customer
                        $_SESSION['customer'] = $result;
                    }

                    if($sendUpdate)
                    {
                        $header = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $_SESSION['customer']->AccessToken
                        );
                        
                        $data = new AlexaAddOrUpdate($_SESSION['customer']);
                        $jsonData = json_encode($data);
                        
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                        $result = curl_exec($ch);
                    }
                }
                // If event gateway returns 403 Forbidden, delete region
                elseif(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
                {
                    $_SESSION['customer']->UserRegion = "";
                    
                    $sendUpdate = false;
                }
                
                // If event gateway returns anything other than 202 Accepted, do not continue sending
                if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) != 202)
                {
                    $sendUpdate = false;
                }
                
                curl_close($ch);
            }                     
            // If devices were deleted, send delete message to event gateway
            if($sendUpdate && $sendDeleteMessage)
            {
                $header = array(
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $_SESSION['customer']->AccessToken
                );

                $data = new AlexaDeleteReport($customer, $_SESSION['customer']);
                $jsonData = json_encode($data);
                    
                if(!empty($data->event->payload->endpoints))
                {
                    $ch = curl_init($url);
                    curl_setopt($ch, CURLOPT_POST, true);  
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
                    $result = curl_exec($ch);
                    
                    // If event gateway returns 401 Unauthorized, try updating the access token and try again
                    if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 401)
                    {
                        $result = refreshAccessToken($_SESSION['customer']);
                        
                        if($result == null)
                        {
                            // Updating token failed, stop sending update
                            $sendUpdate = false;
                        }
                        else
                        {
                            // Else update customer
                            $_SESSION['customer'] = $result;
                        }

                        if($sendUpdate)
                        {
                            $header = array(
                                'Content-Type: application/json',
                                'Authorization: Bearer ' . $_SESSION['customer']->AccessToken
                            );
                            
                            $data = new AlexaDeleteReport($customer, $_SESSION['customer']);
                            $jsonData = json_encode($data);
                            
                            curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                            $result = curl_exec($ch);
                        }
                    }
                    // If event gateway returns 403 Forbidden, delete region
                    elseif(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
                    {
                        $_SESSION['customer']->UserRegion = "";
                        
                        $sendUpdate = false;
                    }
                    
                    // If event gateway returns anything other than 202 Accepted, do not continue sending
                    if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) != 202)
                    {
                        $sendUpdate = false;
                    }
                    
                    curl_close($ch);
                    
                }
            }
        }
        
        // Save customer to db
        $customer = saveCustomerRDS($_SESSION['customer']);
        
        // If customer is google assistant user, tell assistant to update devices
        if($_SESSION['customer'] != null && $_SESSION['customer']->IsGoogleAssistant && ($sendUpdateMessage || $sendDeleteMessage))
        {
            $ch = curl_init();
            requestSync($ch, $_SESSION['customer']);
            curl_close($ch);
        }
                
		if($customer == null)
		{
			echo "NOK";
		}
		else
		{
			echo "OK";
		}

		return;
	}
	
	echo "NOK";
	return;
?>
