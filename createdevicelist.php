<?php
	$basedir = realpath(__DIR__);
    require_once($basedir . '/fritzbox/auxiliary.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
	require_once($basedir . '/fritzbox/device.inc.php');
    require_once($basedir . '/fritzbox/connection.inc.php');

    header("X-Robots-Tag: noindex", true);
    
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		return;
	}
    
    session_set_cookie_params(0, "/", "", true, true);
	session_start();

    if(!is_int($_SESSION['connectionNumber']))
    {
        $_SESSION['connectionNumber'] = CONNECTION_DEFAULT;
    }
    $connection = $_SESSION['customer']->getConnection($_SESSION['connectionNumber']);
?>

<div class="col-11 mx-auto mb-5 text-left"><button id="saveSettings2" type="submit" class="btn btn-success clickable savesettings">Daten speichern</button></div>
<div class="col-12 mb-2"><h4>Gefundene Geräte</h4></div>

<?php
    if($connection->Legacy)
    {
?>
<div class="col-11 mx-auto">
    <p>Ihre FRITZ!OS-Version ist sehr alt. Sie können Steckdosen bedienen, aber der angezeigte Zustand in der Alexa-App stimmt nicht unbedingt mit dem realen Zustand überein.</p>
</div>
<?php
    }
?>

<div class="col-11 mx-auto">
	<table class="table table-sm">
		<thead class="table-danger">
			<tr>
<?php
            if(count($_SESSION['customer']->ConnectionList) > 1)
            {
?>
                <th></th>
<?php       
            }
?>
				<th class="d-none d-sm-table-cell">Identifier</th>
				<th>Name</th>
				<th class="d-none d-sm-table-cell">Produkt</th>
				<th class="text-center">Typ</th>
				<th class="text-center">Einstellungen
					<i class="fas fa-info-circle" aria-hidden="true" data-toggle="tooltip" data-html="true" data-placement="top" 
						title="
						<p class='text-left'>
						<strong>Ein/Aus:</strong><br>
						Temperatureinstellung, auf die das Thermostat bei Befehl 'Ein' oder 'Aus' gesetzt werden soll
                        <br><br>
                        <strong>Schalter:</strong><br>
                        Zusätzlichen Schalter an Alexa melden, um Ein/Aus in Routinen verwenden zu können
						</p>">
					</i>
				</th>
			</tr>
		</thead>
		<tbody>
		
<?php
        // sort by connection number
        usort($_SESSION['customer']->DeviceList, function($a, $b) 
        {
            return $a->ConnectionNumber <=> $b->ConnectionNumber;
        });
        
		foreach($_SESSION['customer']->DeviceList as $device)
		{           
            if (!$device->isDeviceClass(CLASS_TEMPERATURESENSOR)  
                && !$device->isDeviceClass(CLASS_THERMOSTAT)  
                && !$device->isDeviceClass(CLASS_POWERSWITCH) 
                && !$device->isDeviceClass(CLASS_TEMPLATE))
            {
                continue;
            }
?>
			<tr>
<?php
            if(count($_SESSION['customer']->ConnectionList) > 1)
            {
?>
                <td translated><strong><?php echo htmlspecialchars($device->ConnectionNumber); ?></strong></td>
<?php       
            }
?>
				<td translated class="d-none d-sm-table-cell"><?php echo htmlspecialchars($device->Identifier); ?></td>
				<td translated><?php echo $device->Name; ?></td>
				<td class="d-none d-sm-table-cell"><?php echo htmlspecialchars($device->ProductName); ?></td>
				<td>
                <table class="table-borderless">
                    <?php if ($device->isDeviceClass(CLASS_TEMPERATURESENSOR) 
                                || ($device->isDeviceClass(CLASS_THERMOSTAT) && $device->IsGroup)) 
                          { 
                    ?>
                        <tr class="text-center text-nowrap"><i class="align-middle wi fa-2x wi-celsius mr-3"></i><strong><span translated>
                        <?php if ($device->IsGroup)
                        {
                            if(count($device->GroupMembers) > 0)
                            {
                                foreach($_SESSION['customer']->DeviceList as $deviceItem)
                                {
                                    if($deviceItem->Id == $device->GroupMembers[0])
                                    {
                                        echo htmlspecialchars($deviceItem->Temperature);
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                echo htmlspecialchars($device->TemperatureActualValue);
                            }
                        } 
                        else
                        {
                            echo htmlspecialchars(round($device->Temperature)); 
                        }
                        ?>
                        </span></strong></tr>
                    <?php } 
                    if ($device->isDeviceClass(CLASS_THERMOSTAT)) {  ?> 
                        <tr class="text-center">
                            <td><i class="fas fa-2x fa-thermometer-half mr-2"></i></td>
                            <td><table class="table-borderless">
                                <tr class="text-nowrap"><td><i class="align-middle wi-fw far fa-dot-circle mr-1"></i><strong>
                                <?php 
                                    if($device->TemperatureSetpoint == TEMPERATURE_ON) 
                                    { 
                                        echo "Ein"; 
                                    } 
                                    elseif ($device->TemperatureSetpoint == TEMPERATURE_OFF) 
                                    { 
                                        echo "Aus"; 
                                    }
                                    else 
                                    {
                                        echo "<span translated>" . htmlspecialchars(round($device->TemperatureSetpoint)) . "</span>"; 
                                    }
                                ?>
                                </strong></td></tr>
                                <tr class="text-nowrap"><td><i class="align-middle wi wi-fw wi-day-sunny mr-1"></i><strong>
                                <?php 
                                    if($device->TemperatureComfort == TEMPERATURE_ON) 
                                    { 
                                        echo "Ein"; 
                                    } 
                                    elseif ($device->TemperatureComfort == TEMPERATURE_OFF) 
                                    { 
                                        echo "Aus"; 
                                    }
                                    else 
                                    {
                                        echo "<span translated>" . htmlspecialchars(round($device->TemperatureComfort)) . "</span>"; ; 
                                    }
                                ?>
                                </strong></td></tr>
                                <tr class="text-nowrap"><td><i class="align-middle wi wi-fw wi-night-clear mr-1"></i><strong>
                                <?php 
                                    if($device->TemperatureSetback == TEMPERATURE_ON) 
                                    { 
                                        echo "Ein"; 
                                    } 
                                    elseif ($device->TemperatureSetback == TEMPERATURE_OFF) 
                                    { 
                                        echo "Aus"; 
                                    }
                                    else 
                                    {
                                        echo "<span translated>" . htmlspecialchars(round($device->TemperatureSetback)) . "</span>"; ; 
                                    }
                                ?>
                                </strong></td></tr>
                            </table></td>
                        </tr>
                    <?php 
                    } 
					if ($device->isDeviceClass(CLASS_POWERSWITCH)) { ?> 
                       <tr class="text-center text-nowrap">
                            <td><i class="fas fa-2x mr-2 fa-plug"></i><strong>
                    <?php
                        if ($device->SwitchedOn == true) 
                        {
                            echo "Ein";
                        }
                        else
                        {
                            echo "Aus";
                        }
                    ?>
                            </strong></td>
                       </tr>
                    <?php 
                    } 
                    if ($device->isDeviceClass(CLASS_TEMPLATE)) { ?>
                        <tr class="text-center text-nowrap">
                            <td><i class="far fa-2x mr-2 fa-clipboard"></i></td>
                       </tr>
                    <?php 
                    }
					?>
                </table> 
				</td>
				<td class="text-center">
<?php
			if ($device->isDeviceClass(CLASS_THERMOSTAT))
			{
?>                  <strong>Aus</strong><br>
                    <select class="selectpicker mb-2 select-temperature" data-width="fit" data-window-padding="150" data-identifier="<?php echo htmlspecialchars($device->Identifier); ?>" data-temperature="off">
                      <optgroup label="Vorgabe">
                        <option value="252" <?php if($device->TemperatureOff == TEMPERATURE_NOCHANGE) echo 'selected';?>>Keine Änderung</option>
                        <option value="253" <?php if($device->TemperatureOff == TEMPERATURE_OFF) echo 'selected';?>>Aus</option>
                        <option value="255" <?php if($device->TemperatureOff == TEMPERATURE_ECO) echo 'selected';?>>Spar</option>
                        <option value="256" <?php if($device->TemperatureOff == TEMPERATURE_COMFORT) echo 'selected';?>>Komfort</option>
                      </optgroup>
                      <optgroup label="Temperatur">
                        <option value="8" <?php if($device->TemperatureOff == 8) echo 'selected';?>>8 °C</option>
                        <option value="9" <?php if($device->TemperatureOff == 9) echo 'selected';?>>9 °C</option>
                        <option value="10" <?php if($device->TemperatureOff == 10) echo 'selected';?>>10 °C</option>
                        <option value="11" <?php if($device->TemperatureOff == 11) echo 'selected';?>>11 °C</option>
                        <option value="12" <?php if($device->TemperatureOff == 12) echo 'selected';?>>12 °C</option>
                        <option value="13" <?php if($device->TemperatureOff == 13) echo 'selected';?>>13 °C</option>
                        <option value="14" <?php if($device->TemperatureOff == 14) echo 'selected';?>>14 °C</option>
                        <option value="15" <?php if($device->TemperatureOff == 15) echo 'selected';?>>15 °C</option>
                        <option value="16" <?php if($device->TemperatureOff == 16) echo 'selected';?>>16 °C</option>
                        <option value="17" <?php if($device->TemperatureOff == 17) echo 'selected';?>>17 °C</option>
                        <option value="18" <?php if($device->TemperatureOff == 18) echo 'selected';?>>18 °C</option>
                        <option value="19" <?php if($device->TemperatureOff == 19) echo 'selected';?>>19 °C</option>
                        <option value="20" <?php if($device->TemperatureOff == 20) echo 'selected';?>>20 °C</option>
                        <option value="21" <?php if($device->TemperatureOff == 21) echo 'selected';?>>21 °C</option>
                        <option value="22" <?php if($device->TemperatureOff == 22) echo 'selected';?>>22 °C</option>
                        <option value="23" <?php if($device->TemperatureOff == 23) echo 'selected';?>>23 °C</option>
                        <option value="24" <?php if($device->TemperatureOff == 24) echo 'selected';?>>24 °C</option>
                        <option value="25" <?php if($device->TemperatureOff == 25) echo 'selected';?>>25 °C</option>
                        <option value="26" <?php if($device->TemperatureOff == 26) echo 'selected';?>>26 °C</option>
                        <option value="27" <?php if($device->TemperatureOff == 27) echo 'selected';?>>27 °C</option>
                        <option value="28" <?php if($device->TemperatureOff == 28) echo 'selected';?>>28 °C</option>
                      </optgroup>
                    </select>
                    <br>
                    <strong>Ein</strong><br>
                    <select class="selectpicker mb-2 select-temperature" data-width="fit" data-window-padding="150" data-identifier="<?php echo htmlspecialchars($device->Identifier); ?>" data-temperature="on">
                      <optgroup label="Vorgabe">
                        <option value="252" <?php if($device->TemperatureOn == TEMPERATURE_NOCHANGE) echo 'selected';?>>Keine Änderung</option>
                        <option value="253" <?php if($device->TemperatureOn == TEMPERATURE_OFF) echo 'selected';?>>Aus</option>
                        <option value="255" <?php if($device->TemperatureOn == TEMPERATURE_ECO) echo 'selected';?>>Spar</option>
                        <option value="256" <?php if($device->TemperatureOn == TEMPERATURE_COMFORT) echo 'selected';?>>Komfort</option>
                      </optgroup>
                      <optgroup label="Temperatur">
                        <option value="8" <?php if($device->TemperatureOn == 8) echo 'selected';?>>8 °C</option>
                        <option value="9" <?php if($device->TemperatureOn == 9) echo 'selected';?>>9 °C</option>
                        <option value="10" <?php if($device->TemperatureOn == 10) echo 'selected';?>>10 °C</option>
                        <option value="11" <?php if($device->TemperatureOn == 11) echo 'selected';?>>11 °C</option>
                        <option value="12" <?php if($device->TemperatureOn == 12) echo 'selected';?>>12 °C</option>
                        <option value="13" <?php if($device->TemperatureOn == 13) echo 'selected';?>>13 °C</option>
                        <option value="14" <?php if($device->TemperatureOn == 14) echo 'selected';?>>14 °C</option>
                        <option value="15" <?php if($device->TemperatureOn == 15) echo 'selected';?>>15 °C</option>
                        <option value="16" <?php if($device->TemperatureOn == 16) echo 'selected';?>>16 °C</option>
                        <option value="17" <?php if($device->TemperatureOn == 17) echo 'selected';?>>17 °C</option>
                        <option value="18" <?php if($device->TemperatureOn == 18) echo 'selected';?>>18 °C</option>
                        <option value="19" <?php if($device->TemperatureOn == 19) echo 'selected';?>>19 °C</option>
                        <option value="20" <?php if($device->TemperatureOn == 20) echo 'selected';?>>20 °C</option>
                        <option value="21" <?php if($device->TemperatureOn == 21) echo 'selected';?>>21 °C</option>
                        <option value="22" <?php if($device->TemperatureOn == 22) echo 'selected';?>>22 °C</option>
                        <option value="23" <?php if($device->TemperatureOn == 23) echo 'selected';?>>23 °C</option>
                        <option value="24" <?php if($device->TemperatureOn == 24) echo 'selected';?>>24 °C</option>
                        <option value="25" <?php if($device->TemperatureOn == 25) echo 'selected';?>>25 °C</option>
                        <option value="26" <?php if($device->TemperatureOn == 26) echo 'selected';?>>26 °C</option>
                        <option value="27" <?php if($device->TemperatureOn == 27) echo 'selected';?>>27 °C</option>
                        <option value="28" <?php if($device->TemperatureOn == 28) echo 'selected';?>>28 °C</option>
                      </optgroup>
                    </select>
                    <br><br>
                    <strong>Schalter</strong><br>
                    <label class="custom-control custom-checkbox">
						<input type="checkbox" class="custom-control-input" id="<?php echo $device->Identifier; ?>" 
							<?php if ($device->AdditionalSwitch) { echo "checked"; } ?>>
						<span class="custom-control-indicator"></span>
					</label>
<?php
			}
?>
				</td>
<?php
			if($device->Locked)
			{
?>
				<td class="text-center">
					<i class="fas fa-exclamation-triangle fa-2x " aria-hidden="true" style="color:red" data-toggle="tooltip" data-html="true" data-placement="top"
						title="
						<p class='text-left'>
						Zugriff über die Smart Home Schnittstelle ist für dieses Gerät gesperrt.<br><br>
						Siehe Allgemeine Fragen & Antworten.
						</p>">
					</i><br>
                    In FRITZ!Box gesperrt!
                </td>
<?php
			} 
            elseif (!$device->Present)
            {
?>
				<td class="text-center">
					<i class="fas fa-exclamation-triangle fa-2x " aria-hidden="true" style="color:red" data-toggle="tooltip" data-html="true" data-placement="top"
						title="
						<p class='text-left'>
						Gerät ist von FRITZ!Box nicht erreichbar.<br><br>
						</p>">
					</i><br>
                    Gerät nicht erreichbar!
                </td>
<?php
            }
            elseif ($device->BatteryLow)
            {
?>
				<td class="text-center">
					<i class="fas fa-exclamation-triangle fa-2x " aria-hidden="true" style="color:red" data-toggle="tooltip" data-html="true" data-placement="top"
						title="
						<p class='text-left'>
						Batteriestand des Gerätes ist niedrig.<br><br>
						</p>">
					</i><br>
                    Batteriestand niedrig!
                </td>
<?php  
            }
?>
			</tr>	
<?php
		}
?>
		</tbody>
	</table>
</div>
<div class="col-11 mx-auto mb-5 text-left"><button id="saveSettings" type="submit" class="btn btn-success clickable savesettings">Daten speichern</button></div>


<div class="col-12 mt-3 accordion" id="Webhookaccordion">
  <div class="card">
    <div class="card-header" id="heading">
      <h5 class="mb-0">
        <button class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapseWebhook" aria-expanded="true" aria-controls="collapseWebhook">
          Webhooks
        </button>
      </h5>
    </div>

    <div id="collapseWebhook" class="collapse hide" aria-labelledby="heading" data-parent="#Webhookaccordion">
      <div class="card-body">

        <div class="col-11 mb-4 mx-auto">
            <form id="webhooks" class="mb-4 mt-2">
            <div class="form-row align-items-start">
              <div class="col-auto">
                <label class="col-form-label" for="apikey">API Schlüssel</label>
              </div>
            </div>
            <div class="form-row align-items-start ml-3 mb-3">
              <div class="col-auto mb-2">
                <input type="text" class="form-control" id="apikey" value="<?php echo htmlspecialchars($_SESSION['customer']->ApiKey) ?>" readonly>
              </div>
              <div class="col-auto">
                <button type="button" class="btn btn-danger clickable" id="generate">Generieren</button>
                <script>
                $('#generate').click(function(event) {
                    $.ajax({
                        url: '/usersettings.php',
                        type: 'post',
                        data: {
                            'action': 'generateApiKey'
                        },
                        success: function (data, status) {
                            $( "#apikey" ).val(data.trim());
                            setWebhook();
                        },
                        error: function (xhr, desc, err) {
                                console.log(xhr);
                                console.log("Details: " + desc + "\nError:" + err);
                        }
                    }); // end .ajax
                });
                </script>
              </div>
            </div>
            <div class="form-row align-items-start">
                <label class="col-form-label" for="apikey">Webhook</label>
            </div>
            <div class="form-row align-items-start ml-3 mb-2">
                <div class="col-auto mb-1">
                    Schalte
                </div>
                <div class="col-auto mb-1" translated>
                    <select class="selectpicker select-webhookdevice" id="webhookdevice" data-width="fit" data-window-padding="150">
                    <option></option>
        <?php
                    foreach($_SESSION['customer']->DeviceList as $device)
                    {
                        $class = "";
                        if($device->isDeviceClass(CLASS_POWERSWITCH))
                        {
                            $class = "switch";
                        }
                        else if($device->isDeviceClass(CLASS_THERMOSTAT))
                        {
                            $class = "thermostat";
                        }
                        else if($device->isDeviceClass(CLASS_TEMPLATE))
                        {
                            $class = "template";
                        }
                        else
                        {
                            continue;
                        }
        ?>
                        <option value="<?php echo $device->Identifier . "," . $class; ?>"><?php echo $device->Name; ?></option>
        <?php            
                    }
        ?>
                    </select>
                </div>

                <div class="col-auto mb-1" id="webhookset" style="display: none;">
                    auf
                </div>
                <div class="col-auto mb-1" id="webhookswitch" style="display: none;">
                    <select class="selectpicker" id="webhookswitchvalue" data-width="fit" data-window-padding="150">
                        <option value="turnon">Ein</option>
                        <option value="turnoff">Aus</option>
                    </select>
                </div>
                <div class="col-auto mb-1" id="webhookthermostat" style="display: none;">
                    <select class="selectpicker mb-2" id="webhookthermostatvalue" data-width="fit" data-window-padding="150">
                      <optgroup label="Vorgabe">
                        <option value="253">Aus</option>
                        <option value="255">Spar</option>
                        <option value="256">Komfort</option>
                      </optgroup>
                      <optgroup label="Temperatur">
                        <option value="8">8 °C</option>
                        <option value="9">9 °C</option>
                        <option value="10">10 °C</option>
                        <option value="11">11 °C</option>
                        <option value="12">12 °C</option>
                        <option value="13">13 °C</option>
                        <option value="14">14 °C</option>
                        <option value="15">15 °C</option>
                        <option value="16">16 °C</option>
                        <option value="17">17 °C</option>
                        <option value="18">18 °C</option>
                        <option value="19">19 °C</option>
                        <option value="20">20 °C</option>
                        <option value="21">21 °C</option>
                        <option value="22">22 °C</option>
                        <option value="23">23 °C</option>
                        <option value="24">24 °C</option>
                        <option value="25">25 °C</option>
                        <option value="26">26 °C</option>
                        <option value="27">27 °C</option>
                        <option value="28">28 °C</option>
                      </optgroup>
                    </select>
                </div>
            </div>
            <div class="form-row align-items-start ml-3 mb-1">
                <textarea translated class="form-control" id="webhook" readonly></textarea>
            </div>
            <div class="form-row align-items-start ml-3 mb-3">
                <button id="copyToClipboard" type="button" class="btn btn-secondary">Zwischenablage</button>
            </div>
            </form>
            <script>
            function setWebhook()
            {
                if($('#webhookdevice') == null || $('#webhookdevice').val() == "")
                {
                    $('#webhook').val("");
                    return;
                }
         
                $item = $('#webhookdevice').val().split(',');
                if($item[0] == null || $item[1] == null)
                {
                    $('#webhook').val("");
                    return;
                }
                
                $identifier = $item[0].trim();
                $devicetype = $item[1].trim();
                
                if ($identifier == "" || $identifier == null || $devicetype == "" || $devicetype == null)
                {
                    $('#webhook').val("");
                    return;
                }
                if($("#customerId") == null || $("#customerId").val() == "")
                {
                    $('#webhook').val("");
                    return;
                }
                if($("#apikey") == null || $("#apikey").val() == "")
                {
                    $('#webhook').val("");
                    return;
                }
                
                if($devicetype == "switch"){
                    if($("#webhookswitchvalue") == null || $("#webhookswitchvalue").val() == "")
                    {
                        $('#webhook').val("");
                        return;
                    }
                    
                    $('#webhook').val("https://api.fbsmarthome.site/webhooks/v1/" 
                    + $( "#customerId" ).val().trim() 
                    + "/" + $( "#apikey" ).val().trim() 
                    + "/" + $( "#webhookswitchvalue" ).val().trim() 
                    + "/" + $identifier);
                }
                else if ($devicetype == "thermostat"){
                    if($("#webhookthermostatvalue") == null || $("#webhookthermostatvalue").val() == "")
                    {
                        $('#webhook').val("");
                        return;
                    }
                    
                    $('#webhook').val("https://api.fbsmarthome.site/webhooks/v1/" 
                    + $( "#customerId" ).val().trim() 
                    + "/" + $( "#apikey" ).val().trim() 
                    + "/settemperature/" + $identifier
                    + "/" + $( "#webhookthermostatvalue" ).val().trim() );
                }
                else if($devicetype == "template"){
                    $('#webhook').val("https://api.fbsmarthome.site/webhooks/v1/" 
                    + $( "#customerId" ).val().trim() 
                    + "/" + $( "#apikey" ).val().trim() 
                    + "/applytemplate/" + $identifier);
                }
                else{
                    $('#webhook').val("");
                    return;
                }
                
            }

            $('#webhookdevice').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {
                if($(this) == null || $(this).val() == "")
                {
                    $('#webhookset').hide();
                    $('#webhookswitch').hide();
                    $('#webhookthermostat').hide();
                    setWebhook();
                    return;
                }
                
                $item = $(this).val().split(',');
                if($item[0] == null || $item[1] == null)
                {
                    $('#webhookset').hide();
                    $('#webhookswitch').hide();
                    $('#webhookthermostat').hide();
                    setWebhook();
                    return;
                }
                        
                $identifier = $item[0].trim();
                $devicetype = $item[1].trim();
                
                if ($identifier == "" || $identifier == null || $devicetype == "" || $devicetype == null)
                {
                    $('#webhookset').hide();
                    $('#webhookswitch').hide();
                    $('#webhookthermostat').hide();
                    setWebhook();
                    return;
                }
                
                if($devicetype == "switch"){
                    $('#webhookset').show();
                    $('#webhookswitch').show();
                    $('#webhookthermostat').hide();
                    setWebhook();
                }
                else if ($devicetype == "thermostat"){
                    $('#webhookset').show();
                    $('#webhookswitch').hide();
                    $('#webhookthermostat').show();
                    setWebhook();
                }
                else if($devicetype == "template"){
                    $('#webhookset').hide();
                    $('#webhookswitch').hide();
                    $('#webhookthermostat').hide();
                    setWebhook();
                }
                else{
                    setWebhook();
                    return;
                }
            });
            $('#webhookswitchvalue').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {    
                setWebhook();
            });
            $('#webhookthermostatvalue').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {    
                setWebhook();
            });
            $('#copyToClipboard').on('click', function() {
                var $temp = $("<input>");
                $("body").append($temp);
                $temp.val($('#webhook').val()).select();
                document.execCommand("copy");
                $temp.remove();
            });
            </script>
        </div>

        <div class="col-11 mx-auto mt-5 mb-2 text-left">
            <button id="saveSettings" type="submit" class="btn btn-success clickable savesettings">Daten speichern</button>
        </div>

      </div>
    </div>
  </div>
</div>

<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
$('.custom-control-input').click(function(event) {
	var $id = $(this).attr("id");
	var $checked = this.checked;
	$.ajax({
		url: '/savesettings.php',
		type: 'post',
		data: {
			'action' : 'setAdditionalSwitch',
			'customerId' : $("#customerId").val(),
			'switchId' : $id,
			'status' : $checked,
		},
	success: function (data, status) {
			console.log(data);
			if (data.trim() != "OK")
			{
				document.getElementById($id).checked = !$checked;
			} 
	},
	error: function (xhr, desc, err) {
		document.getElementById($id).checked = !$checked;
	}
	}); // end .ajax
  });
$('.savesettings').click(function(event) {
	$('.savesettings').prop('disabled', true);
	$('.savesettings').html(i18nextify.i18next.t('Wird gespeichert...'));
	$.ajax({
		url: '/savesettings.php',
		type: 'post',
		data: {
			'action' : 'save',
			'customerId' : $("#customerId").val()
		},
	success: function (data, status) {
			console.log(data);
			if (data.trim() != "OK")
			{
				$('.savesettings').html(i18nextify.i18next.t('Fehlgeschlagen'));
			} 
			else {
				$('.savesettings').html(i18nextify.i18next.t('Gespeichert'));
			}
            $( "#usersettings" ).load( "usersettings.php" );
            setTimeout(function() {
                $('.savesettings').prop('disabled', false);
                $('.savesettings').html(i18nextify.i18next.t('Daten speichern'));
            }, 4000);
	},
	error: function (xhr, desc, err) {
		$('.savesettings').html(i18nextify.i18next.t('Fehlgeschlagen'));
        setTimeout(function() {
                $('.savesettings').prop('disabled', false);
                $('.savesettings').html(i18nextify.i18next.t('Daten speichern'));
        }, 4000);
	}
	}); // end .ajax

  });
$.fn.selectpicker.Constructor.BootstrapVersion = '4';
$('.selectpicker').selectpicker('setStyle', 'btn-default btn-sm');
$('.select-temperature').on('changed.bs.select', function (event, clickedIndex, isSelected, previousValue) {
    $identifier = $(this).data("identifier");
    
    if ($identifier == "" || $identifier == null)
    {
        return;
    }
    
    $selector = $(this).data("temperature");
    $valueOff = $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="off"]').val();
    $valueOn = $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="on"]').val();
    
    $.ajax({
        dataType: 'json',
		url: '/savesettings.php',
		type: 'post',
		data: {
            'customerId' : $("#customerId").val(),
			'action' : 'setTemperature',
            'selector' : $selector,
			'thermostatId' : $identifier,
            'valueOff' : $valueOff,
            'valueOn' : $valueOn
		},
	success: function (data, status) {
			console.log(data);
            if(data.off == 0 && $selector == "off")
            {
                $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="off"]').val(previousValue);
                $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="off"]').selectpicker('render');
            }
            else if (data.on == 0 && $selector == "on")
            {
                $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="on"]').val(previousValue);
                $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="on"]').selectpicker('render');
            }
            else
            {
                if(data.off != $valueOff && data.off != 0)
                {
                    $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="off"]').val(data.off);
                    $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="off"]').selectpicker('render');
                }
                if(data.on != $valueOn && data.on != 0)
                {
                    $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="on"]').val(data.on);
                    $('.select-temperature[data-identifier="' + $identifier + '"][data-temperature="on"]').selectpicker('render');
                }
                
            }
            
	},
	error: function (xhr, desc, err) {
            console.log(err);
	}
	}); // end .ajax
});

</script>
