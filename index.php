<?php
session_set_cookie_params(0, "/", "", true, true);
session_start();

function generateCacheLink($filename)
{
	$timestamp = filemtime($filename);
	return $filename . "?" . $timestamp;
}

function generateTimestamp($filename)
{
	$timestamp = filemtime($filename);
	return $timestamp;
}

$locale = strstr(filter_input(INPUT_GET,"lng",FILTER_SANITIZE_STRING), "_", true);
if ($locale == false)
{
   $locale = filter_input(INPUT_GET,"lng",FILTER_SANITIZE_STRING); 
}

if($locale != "en")
{
    $locale = "de";
}

$fblocale = strstr(filter_input(INPUT_GET,"fb_locale",FILTER_SANITIZE_STRING), "_", true);
if($fblocale == "en")
{
    $locale = "en";
}

?>
<!DOCTYPE html>
<?php
    if($locale == "en")
    {
?>
<html lang="en">
<?php 
    } else {
?>
<html lang="de">
<?php 
    }
?>
<head>
	<meta charset="utf-8">
    
    <?php
    if($locale == "en")
    {
    ?>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Integrating FRITZ!Box Smart Home devices in Amazon Alexa and Google Assistant. Supported are all FRITZ!Box connected Smart Plugs and Thermostats.">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
	<link rel="manifest" href="/img/manifest.json">
	<link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon.ico">
    <link rel="alternate" hreflang="x-default" href="https://www.fbsmarthome.site/" />
    <link rel="alternate" hreflang="de" href="https://www.fbsmarthome.site/?lng=de" />
    <link rel="alternate" hreflang="en" href="https://www.fbsmarthome.site/?lng=en" />
	<meta name="msapplication-config" content="/img/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<meta name="application-name" content="FB Smart Home"/>
	<meta property="og:url" content="https://www.fbsmarthome.site/?lng=en" />
	<meta property="og:title" content="FB Smart Home: Alexa and Google Assistant for FRITZ!Box Smart Home" />
	<meta property="og:description" content="Integrating FRITZ!Box Smart Home devices in Amazon Alexa and Google Assistant. Supported are all FRITZ!Box connected Smart Plugs and Thermostats." />
	<meta property="og:image" content="https://www.fbsmarthome.site/img/icon.jpg" />
	<meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="de_DE" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="399738323796137" />
	
	<title>FB Smart Home: Alexa and Google Assistant for FRITZ!Box Smart Home</title>

    <?php
    }
    else
    {
    ?>
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Einbindung von FRITZ!Box Smart Home Geräten in Amazon Alexa und Google Assistant. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate.">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
	<link rel="manifest" href="/img/manifest.json">
	<link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon.ico">
    <link rel="alternate" hreflang="x-default" href="https://www.fbsmarthome.site/" />
    <link rel="alternate" hreflang="de" href="https://www.fbsmarthome.site/?lng=de" />
    <link rel="alternate" hreflang="en" href="https://www.fbsmarthome.site/?lng=en" />
	<meta name="msapplication-config" content="/img/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<meta name="application-name" content="FB Smart Home"/>
	<meta property="og:url" content="https://www.fbsmarthome.site" />
	<meta property="og:title" content="FB Smart Home: Alexa und Google Assistant für FRITZ!Box Smart Home" />
	<meta property="og:description" content="Einbindung von FRITZ!Box Smart Home Geräten in Amazon Alexa und Google Assistant. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate." />
	<meta property="og:image" content="https://www.fbsmarthome.site/img/icon.jpg" />
	<meta property="og:locale" content="de_DE" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="399738323796137" />
	
	<title>FB Smart Home: Alexa und Google Assistant für FRITZ!Box Smart Home</title>

    <?php
    }
    ?>
    

	<link rel="stylesheet" href="<?php echo generateCacheLink("css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/template.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/fontawesome-all.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo generateCacheLink("css/weather-icons.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/googlefonts.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/smartphoto.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/shariff.min.css"); ?>">
    <link rel="stylesheet" href="<?php echo generateCacheLink("css/bootstrap-select.min.css"); ?>">
    
</head>

<body>
    <?php
    if($locale == "en")
    {
    ?>
    
	<div itemscope itemtype="http://schema.org/WebApplication">
		<meta itemprop="name" content="FB Smart Home: Alexa and Google Assistant for FRITZ!Box Smart Home" />
        <meta itemprop="applicationCategory" content="HomeApplication" />
        <meta itemprop="operatingSystem" content="Alexa, Google Assistant, Webhooks" />
		<meta itemprop="image" content="https://www.fbsmarthome.site/img/icon.jpg" />
		<meta itemprop="description" content="Integrating FRITZ!Box Smart Home devices in Amazon Alexa and Google Assistant. Supported are all FRITZ!Box connected Smart Plugs and Thermostats." />
	</div>
	<script type="application/ld+json">
    {
		"@context": "http://schema.org",
		"@type": "WebApplication",
		"name": "FB Smart Home: Alexa and Google Assistant for FRITZ!Box Smart Home",
        "applicationCategory": "HomeApplication",
        "operatingSystem": "Alexa, Google Assistant, Webhooks",
		"url": "https://www.fbsmarthome.site/?lng=en",
		"logo": "https://www.fbsmarthome.site/img/icon.jpg",
        "image": "https://www.fbsmarthome.site/img/icon.jpg",
        "description": "Integrating FRITZ!Box Smart Home devices in Amazon Alexa and Google Assistant. Supported are all FRITZ!Box connected Smart Plugs and Thermostats."
	}
    </script>
    
    <?php
    } else {
    ?>
    
    <div itemscope itemtype="http://schema.org/WebApplication">
		<meta itemprop="name" content="FB Smart Home: Alexa und Google Assistant für FRITZ!Box Smart Home" />
        <meta itemprop="applicationCategory" content="HomeApplication" />
        <meta itemprop="operatingSystem" content="Alexa, Google Assistant, Webhooks" />
		<meta itemprop="image" content="https://www.fbsmarthome.site/img/icon.jpg" />
		<meta itemprop="description" content="Einbindung von FRITZ!Box Smart Home Geräten in Amazon Alexa und Google Assistant. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate." />
	</div>
	<script type="application/ld+json">
    {
		"@context": "http://schema.org",
		"@type": "WebApplication",
		"name": "FB Smart Home: Alexa und Google Assistant für FRITZ!Box Smart Home",
        "applicationCategory": "HomeApplication",
        "operatingSystem": "Alexa, Google Assistant, Webhooks",
		"url": "https://www.fbsmarthome.site/?lng=de",
		"logo": "https://www.fbsmarthome.site/img/icon.jpg",
        "image": "https://www.fbsmarthome.site/img/icon.jpg",
        "description": "Einbindung von FRITZ!Box Smart Home Geräten in Amazon Alexa und Google Assistant. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate."
	}
    </script>
    
    <?php
    }
    ?>
    
	<noscript>
		<style>
			.collapse {
				display: block;
			}
		</style>
    </noscript>
	<section id="top">

		<!-- Navigation -->
		<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
			<div class="container">
				<a class="navbar-brand js-scroll-trigger" href="#top">
                <img src="/img/favicon-32x32.png" width="32" height="32" class="d-inline-block align-top" alt="">
                    FB Smart Home
                </a>
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Navigation umschalten">
          <span class="navbar-toggler-icon"></span>
        </button>

				<div class="collapse navbar-collapse" id="navbarResponsive">
					<ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-center" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-language fa-2x"></i>
                            </a>
                            <div class="dropdown-menu text-center" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item text-center" href="/?lng=de">Deutsch</a>
                                <a class="dropdown-item text-center" href="/?lng=en">English</a>
                            </div>
                        </li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#einstellungen">Einstellungen</a>
						</li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#skill">Über</a>
						</li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#fragen">Fragen</a>
						</li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#datenschutz">Datenschutzhinweise</a>
						</li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#kontakt">Kontakt</a>
						</li>
						<li class="nav-item mt-1">
							<a class="nav-link js-scroll-trigger text-center" href="#teilen">Teilen</a>
						</li>   
					</ul>
				</div>
			</div>
		</nav>

		<header class="masthead text-center text-white d-flex">
			<div class="container my-auto">
				<div class="row">
					<div class="col-7 mx-auto text-dark">
						<h1 class="text-uppercase"><strong>FB Smart Home</strong></h1>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-7 mx-auto">
						<p class="text-dark text-center mb-4">Hier finden Sie alle Informationen zu FB Smart Home
                        <br>
                        für<br>
                        <a href="https://{{www.amazon.de}}/gp/product/B078KLZL1R" target="_blank">Amazon Alexa</a><br>und<br>
                        <a href="https://{{assistant.google.com/services/a/uid/00000013134602b8?hl=de}}" target="_blank">Google Assistant</a>
                        <br><br>
						Bitte verbinden Sie unter 'Einstellungen' Ihre FRITZ!Box</p>
					</div>
				</div>
				<div class="row">
					<div class ="mx-auto">
						<a class="btn btn-primary btn-xl js-scroll-trigger" href="#einstellungen">Einstellungen</a>
					</div>
				</div>
			</div>
		</header>
	</section>
	<section class="bg-light text-dark" id="einstellungen">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<h2 class="section-heading text-dark">Einstellungen</h2>
					<hr class="my-4">
				</div>
			</div>
			<div class="row mb-5">
				<div class="col-10 mx-auto text-left">
					<p>
						Zur Steuerung nehmen Sie bitte folgende Einstellungen in Ihrer Fritzbox vor
                    </p>
						<div id="gallery">
							<div class="row">
								<div class="col-1 text-right"><i class="fas fa-chevron-right" aria-hidden="true"></i></div>
								<div class="col-11 mx-auto mb-4">
									<strong>Aktivieren Sie das MyFRITZ! Konto</strong><br>Stellen Sie sicher, dass der Internetzugriff auf Ihre FRITZ!Box erlaubt ist<br>
									<a href="img/settings1.png" class="js-smartphoto" data-group="0" id="settings1"><img src="img/settings1.png" width="0" alt="MyFritz Konto in FritzBox einschalten"/><span class="far fa-question-circle fa-2x mr-2"></span>My!FRITZ-Bildschirm</a>
								</div>
							</div>	
							<div class="row">
								<div class="col-1 text-right"><i class="fas fa-chevron-right" aria-hidden="true"></i></div>
								<div class="col-11 mx-auto mb-4">
									<strong>Legen Sie einen neuen Benutzer an</strong><br>
									Wählen Sie ein sicheres Passwort und geben Sie den Zugang nur für Smart Home frei<br>
									<a href="img/settings2.png" class="js-smartphoto" data-group="0" id="settings2"><img src="img/settings2.png" width="0" alt="Neuen Benutzer für Alexa anlegen"/><span class="far fa-question-circle fa-2x mr-2 mb-2"></span>Benutzer-Bildschirm</a><br>
									<a href="img/settings3.png" class="js-smartphoto" data-group="0" id="settings3"><img src="img/settings3.png" width="0" alt="Alexa Benutzer nur Zugriff auf Smart Home Einstellungen der FritzBox erlauben"/><span class="far fa-question-circle fa-2x mr-2"></span>Benutzer-Einstellungen</a>
								</div>
							</div>	
							<div class="row">
							<div class="col-1 text-right"><i class="fas fa-chevron-right" aria-hidden="true"></i></div>
								<div class="col-11 mx-auto">
									<strong>Verknüpfen Sie die Einstellungen mit Alexa oder Google Assistant.</strong><br>
									Melden Sie sich dazu unten mit Ihrem Amazon-Konto an ('Login with Amazon') und tragen die 
									Verbindungsdaten für Ihre FRITZ!Box ein. Testen Sie die Verbindung und speichern Sie bei Erfolg die Daten.
								</div>
							</div>
						</div>
					
					
				</div>
			</div>
			<div class="row mb-3">
                <a id="loggedin" style="padding-top: 100px; margin-top: -100px;"></a>
                <div id="amazon-root"></div>
                <script>
                    window.onAmazonLoginReady = function () {
                        amazon.Login.setClientId('amzn1.application-oa2-client.bd930c120b9c46f08140a93b8d984eb6');
                        amazon.Login.setUseCookie(false);
                    };
                    ( function ( d ) {
                        var a = d.createElement( 'script' );
                        a.type = 'text/javascript';
                        a.async = true;
                        a.id = 'amazon-login-sdk';
                        a.src = 'https://api-cdn.amazon.com/sdk/login1.js';
                        d.getElementById( 'amazon-root' ).appendChild( a );
                    } )( document );
                </script>

				<div class="col-12" id="usersettings">
					<noscript>
						<p class="text-center">Bitte aktivieren Sie Javascript, um sich mit Amazon anzumelden.</p>
					</noscript>
					<!-- User settings and login button will be shown here! -->
				</div>
			</div>
			<div class="row col-10 mx-auto mb-4" id="error"><!-- error will be shown here ! --></div>
			<div class="row" id="devices"><!-- device list will be shown here ! --></div>
			<div class="row mt-5 hide" id="deleteDataText">
				<div class="col-11 mx-auto">
					<strong>Alle Daten löschen</strong><br>Hier können Sie permanent Ihre Daten aus der Datenbank löschen
				</div>
			</div>
			<div class="row hide" id="deleteDataButton">
				<div class="col-11 mx-auto mt-2 text-left"><button id="deleteData" type="button" class="btn btn-primary clickable" data-toggle="modal" data-target="#confirmDelete">Daten Löschen</button></div>
				<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="modallabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
							<strong>Sollen die Zugangsdaten wirklich gelöscht werden?</strong>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default clickable" data-dismiss="modal">Abbrechen</button>
								<button id="delete" type="button" class="btn btn-danger clickable">Löschen</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</section>

	<section class="text-dark" id="skill">
		<div class="container">
			<div class="row">
				<div class="col-11 mx-auto text-center">
					<h2 class="section-heading">Über FB Smart Home</h2>
					<hr class="my-4">
					<p class="mb-5">
					<Strong>Dieser <a href="https://{{www.amazon.de}}/gp/product/B078KLZL1R" target="_blank">Skill</a> und diese <a href="https://{{assistant.google.com/services/a/uid/00000013134602b8?hl=de}}">Action</a> wird 
					nicht von <a href="https://avm.de/" target="_blank">AVM</a> bereitgestellt und nicht von AVM unterstützt.</strong><br><br>
					Er entstand im Rahmen eines Privatprojektes und wird unentgeltlich zur Verfügung gestellt.
					</p>
					<p class="mb-5 text-left">
					Sie können die Smart Home Geräte der FRITZ!Box mit Amazon Alexa oder Google Assistant steuern. 
					Folgende Sprachbefehle stehen zur Verfügung:
					</p>
				</div>
			</div>
			<div class="row">
				<div class="offset-1">
					<p><i class="fas fa-thermometer-half fa-2x mr-3"></i>Heizkörperthermostat</p>
				</div>
			</div>
			<div class="row mb-3">
				<div class="offset-2">
					<p>Stelle Heizung auf 22 Grad</p>
					<p>Stelle Heizung 2 Grad höher/niedriger</p>
                    <p>Stelle Heizung wärmer/kälter</p>
					<p>Erhöhe/verringere Temperatur von Heizung um 2 Grad</p>
					<p>Stelle Heizung auf (Modus) Heizen <small>(Stellt auf Komforttemperatur)</small></p>
					<p>Stelle Heizung auf (Modus) Eco <small>(Stellt auf Spartemperatur)</small></p>
					<p>Stelle Heizung auf Ausgestellt</p>
					<p>Wie ist Heizung eingestellt?</p>
					<p>Wie ist die Temperatur von Heizung? <small>(Alexa)</small></p>
                    <p>Wie warm ist Heizung? <small>(Google)</small></p>
					<p>Schalte Heizung Ein</p>
					<p>Schalte Heizung Aus</p>
				</div>
			</div>
			<div class="row">
				<div class="offset-1">
					<p><i class="fas fa-plug fa-2x mr-3"></i>Schaltsteckdose</p>
				</div>
			</div>
			<div class="row">
				<div class="offset-2">
					<p>Schalte Steckdose Ein</p>
					<p>Schalte Steckdose Aus</p>
                    <p>Wie ist die Temperatur von Steckdose? <small>(Alexa)</small></p>
                    <p>Wie warm ist Steckdose? <small>(Google)</small></p>
				</div>
			</div>
            <div class="row">
				<div class="offset-1">
					<p><i class="far fa-clipboard fa-2x mr-3"></i>Vorlage</p>
				</div>
			</div>
			<div class="row">
				<div class="offset-2">
					<p>Aktiviere Vorlage</p>
					<p>Schalte Vorlage ein</p>
				</div>
			</div>
		</div>
	</section>

	<section class="bg-light text-dark" id="fragen">
		<div class="container text-left">
			<div class="col-lg-8 mx-auto text-center">
				<h2 class="section-heading text-dark">Fragen & Antworten</h2>
				<hr class="my-4">
			</div>
			<h3>Allgemein</h3>
			<div class="panel-group mb-5" id="Allgemeinaccordion" role="tablist">

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage2">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse2" aria-expanded="false" aria-controls="allgemeincollapse2">
							Können Befehle oder Antworten umformuliert werden?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse2" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage2" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
							<p><strong>Nein.</strong><br><br>Für Smart Home haben Amazon und Google die Befehle und Antworten fest vorgegeben. Diese sind nicht anpassbar.</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage5">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse5" aria-expanded="false" aria-controls="allgemeincollapse5">
							Wie kann ich Geräte wieder aus Alexa löschen?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse5" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage5" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							Sie können angelegte Geräte über die Alexa-App oder die Alexa-Website <a target="_blank" href="https://{{alexa.amazon.de}}">https://alexa.amazon.de</a> löschen.<br>
							Die Geräte finden Sie dort unter 'Smart Home', 'Geräte'.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage6">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse6" aria-expanded="false" aria-controls="allgemeincollapse6">
							Alexa sagt 'Das Gerät reagiert nicht', obwohl es wieder an der FRITZ!Box angemeldet ist.<br>
							Die Website meldet: 'In FRITZ!Box gesperrt'
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse6" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage6" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							Es dauert einige Minuten, bis der aktuelle Zustand des Gerätes wieder zur Verfügung steht. Bitte versuchen Sie es dann erneut.<br><br>
							Stellen Sie außerdem sicher, dass das Gerät für die Nutzung über die Schnittstelle freigeschaltet ist<br>
							<a href="img/popupAPI.png" class="image-link ml-4 js-smartphoto" data-group="2" id="api1"><img src="img/popupAPI.png" width="0" alt="Heizungsthermostat für API-Steuerung freigeben"/>
								<span class="far fa-question-circle fa-2x mr-2"></span>Heizungsthermostat</a><br>
							<a href="img/popupAPI2.png" class="image-link ml-4 js-smartphoto" data-group="2" id="api2"><img src="img/popupAPI2.png" width="0" alt="Steckdose für API-Steuerung freigeben"/>
								<span class="far fa-question-circle fa-2x mr-2"></span>Schaltsteckdose</a>
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage7">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse7" aria-expanded="false" aria-controls="allgemeincollapse7">
							Warum werden manchmal veraltete Informationen gemeldet? Warum tauchen neu angelegte Gruppen/Geräte erst nach einiger Zeit auf?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse7" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage7" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							Alexa wartet maximal 8 Sekunden auf eine Antwort der Geräte. Da die FRITZ!Box gerade beim Verbindungsaufbau sehr
							langsam antworten kann, werden zwischengespeicherte Daten an Alexa gemeldet. Der Abruf der aktuellen Daten aus der FRITZ!Box erfolgt 
							anschließend im Hintergrund. Auch das Schalten von Steckdosen wird hierdurch leicht verzögert.<br><br>
							Werden Geräte in der Alexa-App betrachtet, stellt Alexa etwa alle 3 Sekunden eine neue Statusanfrage. Um die Verbindung zur FRITZ!Box
							nicht zu überlasten wird in diesem Fall ausschließlich auf die zwischengespeicherten Daten zurückgegriffen. Eine Aktualisierung erfolgt, 
							wenn die Daten längere Zeit nicht aktualisiert wurden oder wenn der letzte Verbindungsversuch fehlgeschlagen ist.
							<br><br>
							Wenn Ihre Geräte oder Gruppen nicht sofort auftauchen, versuchen Sie es nach 10 Minuten bitte erneut.
                            <br><br>
                            Google aktualisiert Smart Home Geräte nicht automatisch, ein erneuter Verbindungstest auf dieser Seite macht die Geräte bei Google bekannt.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage8">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse8" aria-expanded="false" aria-controls="allgemeincollapse8">
							Warum kann meine FRITZ!Box nicht erreicht werden, obwohl sie online ist?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse8" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage8" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							Möglicherweise antwortet Ihre FRITZ!Box sehr langsam auf Anfragen. Die Website und der Skill warten nur eine gewisse Zeit auf eine Antwort.<br>
							Eventuell hilft ein Neustart der FRITZ!Box.<br><br>
							Ältere Modelle der FRITZ!Box antworten generell sehr langsam auf Anfragen auf die Web-Schnittstelle. Leider wird die Benutzung des Skills hier
							wahrscheinlich fehlschlagen.
						</p>
						</div>
					</div>
				</div>
                
                <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage12">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse12" aria-expanded="false" aria-controls="allgemeincollapse12">
							Warum dauert es so lange, bis Temperatureinstellungen übernommen werden?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse12" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage12" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p> 
                            Die FRITZ!Box überträgt nur alle 15 Minuten neue Informationen an die angeschlossenen Thermostate. Es kann somit
                            bis zu 15 Minuten dauern, bis eine neue Temperatureinstellung vom Thermostat übernommen wird.
						</p>
						</div>
					</div>
				</div>
                
                <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage9">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse9" aria-expanded="false" aria-controls="allgemeincollapse9">
							Warum funktionieren bei mir manche Geräte und manche nicht? Warum funktionieren Geräte erst wenn sie umbenannt worden sind?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse9" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage9" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							Wenn Sie vorher einen anderen Skill für diese Smart Home Geräte verwendet haben, überprüfen Sie folgendes:<br>
                            Besuchen Sie die Webseite <a href="https://{{alexa.amazon.de}}" target="_blank">https://alexa.amazon.de</a>, Smart Home, Geräte 
                            und löschen Sie dort die Geräte, die als (Offline) angezeigt werden.<br><br>
                            Weitere Möglichkeiten:<br>
                            <span class="ml-3">- Deaktivieren Sie beide Skills</span><br>
                            <span class="ml-3">- Löschen Sie alle Geräte der Skills aus Smart Home (über die App oder Webseite)</span><br>
                            <span class="ml-3">- Aktivieren Sie den Skill erneut und suchen Sie nach Smart Home Geräten</span><br><br>
                            Die gefundenen Geräte sollten anschließend funktionieren.
						</p>
						</div>
					</div>
				</div>
                
                <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage11">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse11" aria-expanded="false" aria-controls="allgemeincollapse11">
							Warum kann ich für Google Assistant nicht mein Google-Konto verwenden?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse11" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage11" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p> 
                            Für die Verknüpfung der Action benötigt Google Zugangsdaten eines OAuth-Dienstes, allerdings erlaubt Google die Nutzung des eigenen OAuth-Dienstes für diesen Zweck nicht. Der einzig verbleibende kostenlose OAuth-Anbieter ist Amazons <a href="https://login.amazon.com/" target="_blank">Login-With-Amazon</a>
						</p>
						</div>
					</div>
				</div>
                
                <div class="panel panel-default">
					<div class="panel-heading" role="tab" id="AllgemeinFrage10">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#allgemeincollapse10" aria-expanded="false" aria-controls="allgemeincollapse10">
							Kann ich etwas spenden?
						</a>
						</h4>
					</div>
					<div id="allgemeincollapse10" class="panel-collapse collapse in" aria-labelledby="AllgemeinFrage10" role="tabpanel" data-parent="#Allgemeinaccordion">
						<div class="panel-body text-dark">
						<p>
							<strong>Ja.</strong><br><br>
                            Hier einige Vorschläge:<br>
                            <span class="ml-2"><a href="https://www.krebshilfe.de/spenden-aktiv-werden/" target="_blank">https://www.krebshilfe.de/spenden-aktiv-werden/</a></span><br>
                            <span class="ml-2"><a href="https://www.dmsg.de/spenden-und-helfen/" target="_blank">https://www.dmsg.de/spenden-und-helfen/</a></span><br>
                            <span class="ml-2"><a href="https://www.worldvision.de/spenden" target="_blank">https://www.worldvision.de/spenden</a></span><br>
                            
                            
						</p>
						</div>
					</div>
				</div>
				
			</div>

			<h3>Technische Fragen</h3>
			<div class="panel-group mb-5" id="Technischaccordion" role="tablist">

				<div class="panel panel-default">
                    <div class="panel-heading" id="TechnischFrage5">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#technischcollapse5" aria-expanded="false" aria-controls="technischcollapse5">
							Kann ich IFTTT nutzen?
						</a>
						</h4>
					</div>
                    <div id="technischcollapse5" class="panel-collapse collapse in" aria-labelledby="TechnischFrage5" role="tabpanel" data-parent="#Technischaccordion">
						<div class="panel-body text-dark">
							<div>
							<p>
								<strong>Ja.</strong><br><br>
								IFTTT kann über Webhooks genutzt werden. Die verfügbaren Webhooks werden nach einem erfolgreichen Verbindungstest
                                angezeigt. Für die Nutzung muss ein API Schlüssel generiert und mit 'Daten speichern' gespeichert werden. Der Zugriff
                                wird auf eine gewisse Anzahl an Schaltbefehlen pro 10 Minuten limitiert.<br><br>
                                Auf IFTTT wird für den 'THAT'-Teil 'Webhooks' > 'Make a web request' ausgewählt.<br>
                                Folgende Einstellungen sind zu tätigen:<br>
                                <span class="ml-3">- URL: Die generierte URL</span><br>
                                <span class="ml-3">- Method: GET</span><br>
                                <span class="ml-3">- Content Type: text/plain </span><br><br>
							</p>
							</div>
						</div>
					</div>
					<div class="panel-heading" id="TechnischFrage2">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#technischcollapse2" aria-expanded="false" aria-controls="technischcollapse2">
							Kann ich den Skill oder die Action benutzen, wenn meine Fritz!Box nur per IPv6 erreichbar ist (ds-lite Anschluss)?
						</a>
						</h4>
					</div>
					<div id="technischcollapse2" class="panel-collapse collapse in" aria-labelledby="TechnischFrage2" role="tabpanel" data-parent="#Technischaccordion">
						<div class="panel-body text-dark">
							<div>
							<p>
								<strong>Ja.</strong><br><br>
								Verbindungen über IPv6 sollten automatisch erkannt und entsprechend verarbeitet werden. Damit sind auch Anschlüsse erreichbar, die nur
								über DS-Lite angebunden sind.
							</p>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" id="TechnischFrage1">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#technischcollapse1" aria-expanded="false" aria-controls="technischcollapse1">
							Was passiert, wenn Alexa oder Google Assistant einen Befehl ausführt? 
						</a>
						</h4>
					</div>
					<div id="technischcollapse1" class="panel-collapse collapse in" aria-labelledby="TechnischFrage1" role="tabpanel" data-parent="#Technischaccordion">
						<div class="panel-body text-dark">
						<p>
							Alexa ruft eine festgelegte Funktion in <a href="https://aws.amazon.com/de/lambda/details/" target="_blank">AWS Lambda</a> auf. Derzeit
							kann von Alexa ausschließlich auf Lambda zugegriffen werden.<br>
							Das übergebene Benutzer-Token wird über OAuth durch <a href="https://login.amazon.com/" target="_blank">LWA</a> gegen die Amazon-ID des
							Benutzers eingetauscht, dessen Daten anschließend aus <a href="https://aws.amazon.com/de/rds/" target="_blank">AWS RDS</a> geladen werden.
							Dort wurden durch die Website die Zugangsdaten der FRITZ!Box sowie weitere Einstellungen hinterlegt, beispielsweise ob eine IPv6-Verbindung
							genutzt werden muss.<br><br>
							Mit den Zugangsdaten wird auf die FRITZ!Box zugegriffen und die entsprechende Schalthandlung ausgelöst. Anschließend werden noch die verfügbaren 
							Informationen der Smart Home Geräte geladen und zurück in die Datenbank geschrieben. Fragt Alexa nach einem Gerätezustand, so werden bevorzugt diese
							zwischengespeicherten Daten für eine Antwort verwendet.<br><br>
							Der Skill legt die gewünschte Schalthandlung in einer Warteschlange ab, die durch PHP-Skripte abgearbeitet wird. Diese übernehmen die Kommunikation mit der
                            FRITZ!Box und sind auf <a href="https://aws.amazon.com/de/ec2/" target="_blank">AWS EC2</a> gehostet. Ist die Warteschlange nicht verfügbar, so wird die Nachricht via <a href="https://aws.amazon.com/de/sns/" target="_blank">AWS SNS</a> 
                            an eine zweite Lambda-Funktion gesendet, die die Kommunikation mit der FRITZ!Box übernimmt.<br><br>
                            Google Assistant greift über das <a href="https://aws.amazon.com/de/api-gateway/" target="_blank">AWS API Gateway</a> auf die gleiche Lambda-Funktion zu wie Alexa.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" id="TechnischFrage4">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#technischcollapse4" aria-expanded="false" aria-controls="technischcollapse4">
							Wie wird auf die FRITZ!Box zugegriffen?
						</a>
						</h4>
					</div>
					<div id="technischcollapse4" class="panel-collapse collapse in" aria-labelledby="TechnischFrage4" role="tabpanel" data-parent="#Technischaccordion">
						<div class="panel-body text-dark">
							<div>
							<p>
								Für den Zugriff auf die FRITZ!Box wird das 
								<a href="https://avm.de/fileadmin/user_upload/Global/Service/Schnittstellen/AHA-HTTP-Interface.pdf" target="_blank"> Home Automation HTTP Interface</a> 
								der FRITZ!Box benutzt.
							</p>
							</div>
						</div>
					</div>
				</div>
                
                <div class="panel panel-default">
					<div class="panel-heading" id="TechnischFrage3">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#technischcollapse3" aria-expanded="false" aria-controls="technischcollapse3">
							Ist der Quellcode einsehbar?
						</a>
						</h4>
					</div>
					<div id="technischcollapse3" class="panel-collapse collapse in" aria-labelledby="TechnischFrage3" role="tabpanel" data-parent="#Technischaccordion">
						<div class="panel-body text-dark">
							<div>
							<p>
								<strong>Ja.</strong><br><br>
								Das Repository befindet sich auf <a href="https://bitbucket.org/raphpa/fbsmarthome" target="_blank">BitBucket</a><br><br>
								Da ich hauptberuflich eher mit Siemens SPS arbeite und noch nicht allzu fit in C# bin, bitte ich um Nachsicht was
								die Quellcode-Qualität angeht. Änderungen gerne per Pull-Request auf den develop-Zweig.<br><br>
                                Das Repository der Website befindet sich ebenfalls auf <a href="https://bitbucket.org/raphpa/fbsmarthomewebsite" target="_blank">BitBucket</a>
							</p>
							</div>
						</div>
					</div>
				</div>
				
			</div>

		</div>

	</section>
	<section class="text-dark" id="datenschutz">
		<div class="container">
			<div class="col-lg-8 mx-auto text-center">
				<h2 class="section-heading text-dark">Datenschutzhinweise</h2>
				<hr class="my-4 mb-5">
			</div>
			<div class="panel-group mb-5" id="datenschutzaccordion" role="tablist">

				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz1">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse1" aria-expanded="false" aria-controls="datenschutzcollapse1">
							Welche Daten werden verarbeitet?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse1" class="panel-collapse collapse in" aria-labelledby="Datenschutz1" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
							<p>Die Datenverarbeitung erfolgt unter dem Grundsatz der Datensparsamkeit.<br><br>
							Dauerhaft in <a href="https://aws.amazon.com/de/rds/" target="_blank">AWS RDS</a> gespeichert werden: Amazon ID, FRITZ!Box Adresse, FRITZ!Box Benutzername, FRITZ!Box Passwort sowie eine Liste der gefundenen Smart Home Geräte. Diese Daten sind
							für die Verwendung des Skills oder der Action technisch zwingend erforderlich und können über das Menü 'Einstellungen' wieder aus der Datenbank gelöscht werden. Eine Nutzung des Skills oder der Action ist dann nicht mehr möglich
							und Bedarf einer erneuten Verbindung durch das Einstellungs-Menü.<br>Nach drei Monaten Inaktivität werden die Daten automatisch aus der Datenbank gelöscht.
                            <br>Ein Deaktivieren des Skills in der Alexa-App oder auf der <a target="_blank" href="https://{{alexa.amazon.de}}">Alexa-Website</a>
							löscht die Daten nicht, da Alexa bei Deaktivierung keine Nachricht an den verarbeitenden Dienst übergibt.<br>Dies gilt ebenso für Google Assistant.
							<br><br>
                            Das FRITZ!Box Passwort wird als Klartext gespeichert. Dies ist für die technische Umsetzung zwingend erforderlich.
                            <br><br>
							Die Amazon ID wird für jede Anwendung neu generiert und erlaubt ausschließlich Amazon eine Zuordnung zu einer Person.<br><br>
							Name und eMail-Adresse des Amazon-Kontos werden nicht abgerufen und übertragen.
							<br><br>
							Technisch bedingt wird die Nutzung des Skills durch Amazon und die Nutzung der Action durch Google protokolliert. Fehlermeldungen und Betriebsdaten werden in <a href="https://aws.amazon.com/de/cloudwatch/" target="_blank">AWS CloudWatch</a> aufbereitet. Diese sind nur mit der Amazon ID gekennzeichnet.
							<br><br>
							Der Zugriff auf den Webserver wird anonymisiert protokolliert.
							</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz2">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse2" aria-expanded="false" aria-controls="datenschutzcollapse2">
							Wo findet die Verarbeitung statt?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse2" class="panel-collapse collapse in" aria-labelledby="Datenschutz2" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
						<p>
							Die <a href="https://aws.amazon.com/de/lambda/details/" target="_blank">Lambda-Funktion</a> des Skills befindet sich in der AWS Region 'eu-west-1' (Irland). Dort werden auch Adresse, Benutzername und Passwort der 
							FRITZ!Box in <a href="https://aws.amazon.com/de/rds/" target="_blank">AWS RDS</a> gespeichert. 
							Anbieter ist <a href="https://aws.amazon.com/" target="_blank">Amazon Web Services AWS</a>.<br><br>
							Die Website wird ebenfalls in dieser Region durch <a href="https://aws.amazon.com/de/ec2/" target="_blank">AWS EC2</a> gehostet.<br><br>
                            Wird der Skill über amazon.com bezogen, befindet sich die verarbeitende Lambda-Funktion in der AWS Region 'us-east-1' (USA).
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz3">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse3" aria-expanded="false" aria-controls="datenschutzcollapse3">
							Werden Daten verschlüsselt übertragen?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse3" class="panel-collapse collapse in" aria-labelledby="Datenschutz3" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
						<p>
							Die Daten werden verschlüsselt zwischen <a href="https://aws.amazon.com/" target="_blank">Amazon AWS</a> und der FRITZ!Box ausgetauscht.
							<br><br>
							Die Verbindung zur Website wird über <a href="https://www.cloudflare.com/" target="_blank">CloudFlare</a> geleitet, so ist es CloudFlare grundsätzlich möglich die Daten mitzulesen, da die 
							verschlüsselte Verbindung bei CloudFlare unterbrochen wird. Die Verbindung zwischen CloudFlare, der Website und dem Endnutzer ist aber
							insgesamt verschlüsselt.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz4">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse4" aria-expanded="false" aria-controls="datenschutzcollapse4">
							Werden Daten weitergegeben?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse4" class="panel-collapse collapse in" aria-labelledby="Datenschutz4" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
						<p>Es werden FRITZ!Box Adresse, Benutzername und Passwort zwischen <a href="https://aws.amazon.com/" target="_blank">Amazon AWS</a> und der FRITZ!Box ausgetauscht.<br>
						Es werden keine Daten an Dritte weitergegeben.<br><br>
						Die Verbindung zur Website wird über <a href="https://www.cloudflare.com/" target="_blank">CloudFlare</a> geleitet, so ist es CloudFlare grundsätzlich möglich die Daten mitzulesen, da die 
						verschlüsselte Verbindung bei CloudFlare unterbrochen wird. Die Verbindung zwischen CloudFlare, der Website und dem Endnutzer ist aber
						insgesamt verschlüsselt.</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz5">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse5" aria-expanded="false" aria-controls="datenschutzcollapse5">
							Wird die Nutzung analysiert?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse5" class="panel-collapse collapse in" aria-labelledby="Datenschutz5" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
						<p>
							Die Nutzung wird nicht systematisch analysiert. <br>Es werden Daten zur Fehlerbehebung aus dem Skill ausgeleitet und in 
							<a href="https://aws.amazon.com/de/cloudwatch/" target="_blank">AWS CloudWatch</a> aufbereitet. Diese werden nach drei Tagen gelöscht
							und sind nur mit der Amazon ID gekennzeichnet.<br><br>
							Die Nutzung der Website wird nicht analysiert und nur anonymisiert protokolliert.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz7">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse7" aria-expanded="false" aria-controls="datenschutzcollapse7">
							Erfassen die Social Media Plugins Daten?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse7" class="panel-collapse collapse in" aria-labelledby="Datenschutz7" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
						<p>
							Die Social Media Plugins erfassen zunächst keine Daten über den Besuch der Website. 
							Erst wenn Sie auf einen Link zum Teilen klicken, werden Daten an den entsprechenden Drittanbieter übertragen.<br><br>
							Es wird die <a href="http://ct.de/-2467514" target="_blank">Shariff</a> Lösung von c't eingesetzt.
						</p>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="Datenschutz6">
						<h4 class="panel-title text-dark">
						<a role="button" class="collapsed" data-toggle="collapse" href="#datenschutzcollapse6" aria-expanded="false" aria-controls="datenschutzcollapse6">
							Werden Cookies eingesetzt?
						</a>
						</h4>
					</div>
					<div id="datenschutzcollapse6" class="panel-collapse collapse in" aria-labelledby="Datenschutz6" role="tabpanel" data-parent="#datenschutzaccordion">
						<div class="panel-body text-dark">
							<p>Es wird ein sogenanntes Session-Cookie gesetzt, das Sie während Ihres Aufenthaltes auf der Website - sofern Sie eingeloggt sind - identifiziert. Ihre Amazon ID wird durch den
							Anmeldevorgang bei Amazon bezogen und in dieser 'Session' abgelegt. Dies dient dem Schutz Ihrer Daten vor Fremdzugriffen. Das Session-Cookie ist nur begrenzte Zeit gültig und
							wird in der Regel bei Beenden des Browsers automatisch gelöscht.<br><br>
							Amazon setzt während des Anmeldevorganges zusätzlich ein eigenes Session-Cookie.<br><br>
							CloudFlare setzt beim Seitenaufruf ein eigenes Session-Cookie.<br><br>
							Alle Cookies sind keine Werbecookies, sondern dienen der technischen Umsetzung der Website.<br>
							Das Setzen der Cookies kann im Browser deaktiviert werden. Ein Abschalten der Cookies für diese Website
							führt dazu, dass sie nicht mehr ordnungsgemäß benutzt werden kann.
							</p>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	<section class="bg-light text-dark" id="kontakt">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 mx-auto text-center">
					<h2 class="section-heading">Kontakt</h2>
					<hr class="my-4">
					<p class="mb-5">Fragen per eMail oder direkt in den Issue Tracker<br><br>
					<strong>Bitte beachten Sie: Da dies ein privates Projekt ist, 
					ist Support leider nur eingeschränkt möglich</strong>
					</p>
				</div>
			</div>
			<div class="row">
							
				<div class="col-sm-3 mx-auto text-center mb-3">
					<i translated class="far fa-envelope fa-3x mb-3 sr-contact"></i>
					<p>
						<a href="mailto:alexa@fbsmarthome.site">alexa@fbsmarthome.site</a>
					</p>
				</div>
                
				<div class="col-sm-3 mx-auto text-center mb-3">
					<i translated class="fas fa-cogs fa-3x mb-3 sr-contact"></i>
					<p>
						<a href="https://p.datadoghq.com/sb/5772b0c26-cb9bd14dd4" target="_blank">Dienststatus</a>
					</p>
				</div>
				
				<div class="col-sm-3 mx-auto text-center mb-3">
					<i translated class="fas fa-bug fa-3x mb-3 sr-contact"></i>
					<p>
						<a href="https://bitbucket.org/raphpa/fbsmarthome/issues" target="_blank">Issue Tracker</a>
					</p>
				</div>
				
				<div class="col-sm-3 mx-auto text-center mb-3">
					<i class="far fa-edit fa-3x mb-3 sr-contact"></i>
					<p>
						Raphael Pala<br>
						Leimbachstraße 89<br>
						57074 Siegen
					</p>
				</div>
			</div>

			<div class="row mt-5" id="teilen">
                    <?php
                        $dataurl = "https://www.fbsmarthome.site";
                        if($locale == "en")
                        {
                            $dataurl = "https://www.fbsmarthome.site/?lng=en";
                        }
                    ?>
					<div translated class="col-6 mx-auto shariff" data-lang="<?php echo $locale; ?>" data-theme="standard" data-services="[&quot;twitter&quot;,&quot;facebook&quot;,&quot;whatsapp&quot;,&quot;info&quot;]" data-url="<?php echo $dataurl; ?>" data-backend-url="/shariff/"></div>
			</div>
			<div class="spacer-350"></div>
		</div>
	</section>

	<!-- Bootstrap core JavaScript -->
	<script src="<?php echo generateCacheLink("js/jquery.min.js"); ?>"></script>
	<script src="<?php echo generateCacheLink("js/bootstrap.bundle.min.js"); ?>"></script>
	
	<!-- Plugin JavaScript -->
	<script src="<?php echo generateCacheLink("js/jquery.easing.min.js"); ?>"></script>
	<script src="<?php echo generateCacheLink("js/scrollreveal.min.js"); ?>"></script>
	<script src="<?php echo generateCacheLink("js/shariff.min.js"); ?>"></script>
    <script src="<?php echo generateCacheLink("js/bootstrap-select.min.js"); ?>"></script>
    
    <!-- Custom scripts for this template -->
    <script src="<?php echo generateCacheLink("js/smartphoto.min.js"); ?>"></script>
    
    <!-- Internationalization -->
    <script src="<?php echo generateCacheLink("js/i18nextify.min.js"); ?>"></script>
    <script>
      window.i18nextify.init({
        backend: {
          loadPath: '/locales/{{lng}}/{{ns}}.json'
        },
        fallbackLng: 'de',
        whitelist: ['de', 'en'],
        load: 'languageOnly',
        nonExplicitWhitelist: true,
        autorun: true, // setting to false init will return an object with start function
        ele: document.body, // pass in another element if you like to translate another html element
        ignoreTags: ['SCRIPT'], // tags to ignore
        detector: { order: ['querystring', 'cookie', 'localStorage', 'navigator', 'htmlTag'] },
        
        // attributes to translate
        translateAttributes: ['placeholder', 'title', 'alt', 'value#input.type=button', 'value#input.type=submit'],

        // merging content (eg. a tags in p tags)
        mergeTags: [], // tags to merge innerHtml to one key
        inlineTags: [], // tags to inline (eg. a, span, abbr, ...)
        ignoreInlineOn: [], // tags to ignore inlining tags under inlineTags

        // cleanup for keys
        cleanIndent: true, // removes indent, eg. if a p tag spans multiple lines
        ignoreCleanIndentFor: ['PRE', 'CODE'], // ignores cleaning up of indent for those tags needing that extra spaceing
        cleanWhitespace: true, // removes surrounding whitespace from key

        namespace: false, // set a filename - default namespace will be translation
        namespaceFromPath: false, // set true will use namepace based on window.location.pathname

        onInitialTranslate: () =>
        {
            $('#settings1').attr('href', 'img/' + i18nextify.i18next.t('settings1.png'));
            $('#settings2').attr('href', 'img/' + i18nextify.i18next.t('settings2.png'));
            $('#settings3').attr('href', 'img/' + i18nextify.i18next.t('settings3.png'));
    
            $('#api1').attr('href', 'img/' + i18nextify.i18next.t('popupAPI.png'));
            $('#api2').attr('href', 'img/' + i18nextify.i18next.t('popupAPI2.png'));
      
            $(".js-smartphoto").SmartPhoto({
                nav: false
            });
        }
      });
    </script>
    <script>
        (function($) {
          "use strict"; // Start of use strict

          // Smooth scrolling using jQuery easing
          $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
              var target = $(this.hash);
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              if (target.length) {
                $('html, body').animate({
                  scrollTop: (target.offset().top - 57)
                }, 1000, "easeInOutExpo");
                return false;
              }
            }
          });

          // Closes responsive menu when a scroll trigger link is clicked
          $('.js-scroll-trigger').click(function() {
            $('.navbar-collapse').collapse('hide');
          });

          // Activate scrollspy to add active class to navbar items on scroll
          $('body').scrollspy({
            target: '#mainNav',
            offset: 57
          });

          // Collapse Navbar
          var navbarCollapse = function() {
            if ($("#mainNav").offset().top > 30) {
              $("#mainNav").addClass("navbar-shrink");
            } else {
              $("#mainNav").removeClass("navbar-shrink");
            }
          };
          // Collapse now if page is not at top
          navbarCollapse();
          // Collapse the navbar when page is scrolled
          $(window).scroll(navbarCollapse);

          // Scroll reveal calls
          window.sr = ScrollReveal();
          sr.reveal('.sr-icons', {
            duration: 600,
            scale: 0.3,
            distance: '0px'
          }, 200);
          sr.reveal('.sr-button', {
            duration: 1000,
            delay: 200
          });
          sr.reveal('.sr-contact', {
            duration: 600,
            scale: 0.3,
            distance: '0px'
          }, 300);

        })(jQuery); // End of use strict

        $( document ).ready(function() {   
            $( "#usersettings" ).load( "usersettings.php" );
            
            $('#delete').click(function(event) {
                $('#delete').prop('disabled', true);
                $.ajax({
                    url: '/savesettings.php',
                    type: 'post',
                    data: {
                        'action' : 'delete',
                        'customerId' : $("#customerId").val()
                    },
                success: function (data, status) {
                        console.log(data);
                        if(data.trim() == "OK") {
                            $('#fritzboxAdress').val('');
                            $('#fritzboxUser').val('');
                            $('#fritzboxPassword').val('');
                            $( "#devices" ).html('');
                            $('#confirmDelete').modal('hide');
                            $( "#usersettings" ).load( "usersettings.php?action=logout" );
                            $( "#devices" ).html('');
                            //auth.signOut();
                            amazon.Login.logout();
                        }	
                },
                error: function (xhr, desc, err) {
                    console.log(err);;
                }
                }); // end .ajax
                $('#delete').prop('disabled', false);
              });
        });

        !function(){"use strict";if(navigator.userAgent.match(/IEMobile\/10\.0/)){var e=document.createElement("style");e.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}")),document.querySelector("head").appendChild(e)}}();
    </script>
</body>

</html>
