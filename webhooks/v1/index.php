<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$basedir = realpath($_SERVER['DOCUMENT_ROOT']);
require_once($basedir . '/vendor/autoload.php');
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/postgresql.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/connection.inc.php');

// get and parse request path
$path = ltrim($_SERVER['REQUEST_URI'], '/');
$path = ltrim($path , 'webhooks/v1');

// put into array
$elements = explode('/', $path);

// check if customer id and api key exist
// if not, end with 400

if(!isset($elements[0]) || !isset($elements[1]))
{
    http_response_code(400);
    die('Incorrect parameters');
}

// check for valid command
if(!isset($elements[2]) || 
    ($elements[2] != "turnon" && 
     $elements[2] != "turnoff" && 
     $elements[2] != "settemperature" && 
     $elements[2] != "applytemplate"))
     {
        http_response_code(400);
        die('Incorrect parameters');
     }
    
// check for device
if(!isset($elements[3]))
{
    http_response_code(400);
    die('Incorrect parameters');
}

// check for parameters on set temperature
if($elements[2] == "settemperature")
{
    if(!isset($elements[4]))
    {
        http_response_code(400);
        die('Incorrect parameters');
    }
    
    $temperature = intval($elements[4]);
    if(($temperature < TEMPERATURE_MINVALUE) || ($temperature > TEMPERATURE_MAXVALUE))
    {
        if(($temperature != TEMPERATURE_OFF) && ($temperature != TEMPERATURE_ECO) && ($temperature != TEMPERATURE_COMFORT)) 
        {
            http_response_code(400);
            die('Incorrect parameters');
        }
    }
}

// Create redis client
$redis = new Predis\Client([
    'scheme'   => 'tcp',
    'host'     => 'redis',
    'port'     => 6379,
    'database' => REDIS_APILIMIT,
    'password' => $_SERVER['REDIS_PASS']
]);

// Create rate limiter
$predisCacheAdapter = new Desarrolla2\Cache\Adapter\Predis($redis);
$cacheAdapter = new Sunspikes\Ratelimit\Cache\Adapter\DesarrollaCacheAdapter($predisCacheAdapter);
$timeAdapter = new Sunspikes\Ratelimit\Time\PhpTimeAdapter();
$throttlerFactory = new Sunspikes\Ratelimit\Throttle\Factory\TimeAwareThrottlerFactory($cacheAdapter, $timeAdapter);
$hydratorFactory = new Sunspikes\Ratelimit\Throttle\Hydrator\HydratorFactory();
$settings = new Sunspikes\Ratelimit\Throttle\Settings\MovingWindowSettings(100, 600);
$ratelimiter = new Sunspikes\Ratelimit\RateLimiter($throttlerFactory, $hydratorFactory, $settings);


// first element is customer id, check for rate limiting
$apiThrottler = $ratelimiter->get($elements[0]);

// return 429 if access is throttled
if(!$apiThrottler->access())
{    
    $retry = ($loginThrottler->getRetryTimeout()/1000);
    http_response_code(429);
    header('Retry-After: ' . $retry);
    die('Too many requests, retry after ' . $retry . ' seconds');
}

//load user from database
$customer = getCustomerRDS($elements[0]);
    
// check if customer exist
if($customer == null)
{
    http_response_code(401);
    die('Unauthorized');
}

// check if api key is correct
if($customer->ApiKey != $elements[1])
{
    http_response_code(401);
    die('Unauthorized');
}

// check if device exists
if(!$customer->hasDevice($elements[3]))
{
    http_response_code(400);
    die('Incorrect parameters');
}

// get device
$device = $customer->getDevice($elements[3]);

// create message array
$jsonarray = array();

// request is turn on
if($elements[2] == "turnon")
{
    // check if device is switch
    if(!$device->isDeviceClass(CLASS_POWERSWITCH))
    {
        http_response_code(400);
        die('Incorrect parameters');
    }
    
    // create message
    $jsonarray = array(
        "message" => "TurnOnDevice",
        "customer" => $customer->CustomerId,
        "device" => $device->Identifier
    );
}
else if($elements[2] == "turnoff")
{
    // check if device is switch
    if(!$device->isDeviceClass(CLASS_POWERSWITCH))
    {
        http_response_code(400);
        die('Incorrect parameters');
    }
    
    // create message
    $jsonarray = array(
        "message" => "TurnOffDevice",
        "customer" => $customer->CustomerId,
        "device" => $device->Identifier
    );
}
else if($elements[2] == "settemperature")
{
    // check if device is thermostat
    if(!$device->isDeviceClass(CLASS_THERMOSTAT))
    {
        http_response_code(400);
        die('Incorrect parameters');
    }
    
    // get temperature
    $temperature = intval($elements[4]);
    if($temperature == TEMPERATURE_ECO)
    {
        $temperature = (int)$device->TemperatureSetback;
    }
    elseif($temperature == TEMPERATURE_COMFORT)
    {
        $temperature = (int)$device->TemperatureComfort;
    }
    
    // create message
    $jsonarray = array(
        "message" => "SetTemperature",
        "customer" => $customer->CustomerId,
        "device" => $device->Identifier,
        "temperature" => (string)$temperature 
    );
}
else if($elements[2] == "applytemplate")
{
    // check if device is template
    if(!$device->isDeviceClass(CLASS_TEMPLATE))
    {
        http_response_code(400);
        die('Incorrect parameters');
    }
    
    // create message
    $jsonarray = array(
        "message" => "ApplyTemplate",
        "customer" => $customer->CustomerId,
        "device" => $device->Identifier
    );
}

// publish request to RabbitMQ
$message = new PhpAmqpLib\Message\AMQPMessage(json_encode($jsonarray),array('expiration' => 60000));
$connection = new PhpAmqpLib\Connection\AMQPStreamConnection('rabbitmq', 5672, $_SERVER['MQ_USER'], $_SERVER['MQ_PASS'], '/', false, 'AMQPLAIN', null, 'en_US', 3.0, 120.0, null, false, 60);
$channel = $connection->channel();
$channel->basic_publish($message, 'fbsmarthome-exchange', 'fbsmarthome');
$channel->close();
$connection->close();
    
// All OK, return device id
http_response_code(202);
echo "OK";
exit();   
?>
