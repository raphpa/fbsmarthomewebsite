<?php
    $basedir = realpath(__DIR__);
	require_once($basedir . '/fritzbox/auxiliary.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
	require_once($basedir . '/fritzbox/device.inc.php');
    require_once($basedir . '/fritzbox/connection.inc.php');
	require_once($basedir . '/fritzbox/fritzbox.inc.php');
    require_once($basedir . '/ipinrange.php');

    header("X-Robots-Tag: noindex", true);
    
	if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		return;
	}
    
    session_set_cookie_params(0, "/", "", true, true);
	session_start();

	if(!isset($_SESSION['customerId']) || filter_input(INPUT_POST,"customerId",FILTER_SANITIZE_STRING) != $_SESSION['customerId'])
	{
		echo "Amazon ID passt nicht zur Session. Bitte erneut anmelden.";
		return;
	}

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 13); 
   	curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

    $connectionNumber = intval(filter_input(INPUT_POST,"connectionNumber",FILTER_SANITIZE_NUMBER_INT));
	$fritzBoxUrl = filter_input(INPUT_POST,"fritzBoxUrl",FILTER_SANITIZE_URL);
	$fritzBoxUsername = filter_input(INPUT_POST,"fritzBoxUsername");
	$fritzBoxPassword = filter_input(INPUT_POST,"fritzBoxPassword");
    $connection = new Connection();
    $connection->Number = $connectionNumber;
    $connection->UseIPv6 = false;
    
	if(strpos($fritzBoxUrl,"://") === false) 
	{
		$fritzBoxUrl = ltrim($fritzBoxUrl, '/');
		$fritzBoxUrl = "https://".$fritzBoxUrl;
	}

	$connection->FritzBoxUrl = "https://" . parse_url($fritzBoxUrl, PHP_URL_HOST);	

	$hostnamearray = explode(".", parse_url($fritzBoxUrl, PHP_URL_HOST));
    $tld = end($hostnamearray);
    if($tld == "myfritz")
	{
		$connection->FritzBoxUrl = $connection->FritzBoxUrl . ".net";
	}

	if (parse_url($fritzBoxUrl, PHP_URL_PORT) != "")
	{
		$connection->FritzBoxUrl .= ":" . parse_url($fritzBoxUrl, PHP_URL_PORT);
	}		
	$connection->FritzBoxUsername = trim($fritzBoxUsername);
	$connection->FritzBoxPassword = trim($fritzBoxPassword);

	$hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));

	if($hostname == "fritz.box")
	{
		error_log("Local access not available.");
		echo "Zugriff nicht lokal über https://fritz.box möglich. Bitte geben Sie die MyFRITZ!-Adresse ein.";
		curl_close($ch);
		return;
	}
	elseif($hostname == "myfritz.net" || $hostname == "www.myfritz.net" || $hostname == "" || strpos($fritzBoxUrl, '@') !== false)
	{
		error_log("myfritz not personalized.");
		echo "Falsche Adresse. Bitte geben Sie die personalisierte MyFRITZ!-Adresse Ihrer FRITZ!Box ein.";
		curl_close($ch);
		return;
	}
	elseif(filter_var(str_replace(array( '[', ']' ), '', $hostname), FILTER_VALIDATE_IP) !== false 
            || substr_count ($hostname, ':') >= 2)
	{
		error_log("ip access not available.");
		echo "Zugriff nicht direkt über die IP möglich. Bitte geben Sie die MyFRITZ!-Adresse ein.";
		curl_close($ch);
		return;
	}
    
    foreach($_SESSION['customer']->ConnectionList as $connectionItem)
    {
        if($connection->FritzBoxUrl === $connectionItem->FritzBoxUrl && $connection->Number != $connectionItem->Number)
        {
            error_log("url duplicate.");
            echo "MyFRITZ!-Adresse ist bereits in einer anderen Verbindung konfiguriert.";
            curl_close($ch);
            return;
        }
    }
    
	$ip4Address = @dns_get_record($hostname, DNS_A);
    $ip6Address = @dns_get_record($hostname, DNS_AAAA);

    $isPrivate = false;
    $isCarrierNAT = false;
    $hasIp4 = ($ip4Address !== false && isset($ip4Address[0]['ip']));
    $hasIp6 = ($ip6Address !== false && isset($ip6Address[0]['ipv6']));
    
	if ($hasIp4)
	{
        $isPrivate = ipv4_in_range($ip4Address[0]['ip'], "10/8") || ipv4_in_range($ip4Address[0]['ip'], "172.16/12") || ipv4_in_range($ip4Address[0]['ip'], "192.168/16");
        $isCarrierNAT = ipv4_in_range($ip4Address[0]['ip'], "100.64/10");
        
        if($isPrivate && !$hasIp6)
        {
			error_log("In private range.");
			echo "FRITZ!Box hat eine private IP-Adresse und ist aus dem Internet nicht erreichbar.";
			curl_close($ch);
			return;
        }

        if($isCarrierNAT && !$hasIp6)
        {
			error_log("Is carrier nat.");
			echo 'Ihr Provider nutzt <a href="https://de.wikipedia.org/wiki/Carrier-grade_NAT" target="_blank">Carrier-grade NAT</a>. 
            FRITZ!Box ist aus dem Internet nicht erreichbar. Dieses Problem kann nur mit Unterstützung des Providers gelöst werden.';
			curl_close($ch);
			return;
        }
        
        $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
		$requestUrl = "https://" . $ip4Address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4Address[0]['ip']));
	} else
	{
		if ($hasIp6)
		{
            $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
			$requestUrl = "https://[" . $ip6Address[0]['ipv6'] . "]";
            if (strlen(trim($port)) > 0)
            {
                $requestUrl .= ":";
                $requestUrl .= $port;
            }
            $requestUrl .= "/";
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6Address[0]['ipv6']));
			//IPv6 connection was successfull
			$connection->UseIPv6 = true;
		}
		else
		{
			error_log("Not accessible even with ipv6.");
			echo "FRITZ!Box ist nicht erreichbar. Adresse konnte nicht gefunden werden. Bitte prüfen, ob MyFRITZ!-Zugang aktiv ist.";
			curl_close($ch);
			return;
		}
	}

	//Login to FRITZ!Box. Call login page and grab challenge
	$url = $requestUrl . "login_sid.lua";
	curl_setopt($ch, CURLOPT_URL, $url);
    
    $challengeXML = false;
    if (!$isPrivate && !$isCarrierNAT) 
    {
        $challengeXML = curl_exec($ch);
    }

	//Cannot get challenge
	if (!$challengeXML)
	{
		// try again with Ipv6 if not tried yet
		if ($connection->UseIPv6 == false)
		{
			$ip6Address = @dns_get_record($hostname, DNS_AAAA);
			if ($ip6Address !== false && isset($ip6Address[0]['ipv6']))
			{
                    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
					$requestUrl = "https://[" . $ip6Address[0]['ipv6'] . "]";
                    if (strlen(trim($port)) > 0)
                    {
                        $requestUrl .= ":";
                        $requestUrl .= $port;
                    }
                    $requestUrl .= "/";
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6Address[0]['ipv6']));
					//IPv6 connection was successfull
					$connection->UseIPv6 = true;
			}
			else
			{
					error_log("Not accessible even with ipv6 after ipv4 failed.");
					echo "FRITZ!Box ist nicht erreichbar. Adresse konnte nicht gefunden werden. Bitte prüfen, ob MyFRITZ!-Zugang aktiv ist.";
					curl_close($ch);
					return;
			}
			$url = $requestUrl . "login_sid.lua";
			curl_setopt($ch, CURLOPT_URL, $url);
        	$challengeXML = curl_exec($ch);

			if (!$challengeXML)
        	{
				if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
				{
					error_log("Timeout getting challenge with ipv6.");
					echo "FRITZ!Box hat nicht innerhalb der vorgegebenen Zeit geantwortet.";
				} 
				elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT && strpos(curl_error($ch), "refused") !== false) 
				{
					error_log("Refused getting challenge with ipv6.");
					echo "FRITZ!Box hat Verbindungsversuch abgelehnt. Eventuell Port überprüfen.";
				}
				else
				{
					error_log("Failed while getting challenge with ipv6.");
					echo "FRITZ!Box ist nicht erreichbar";
				}
				
                curl_close($ch);
                return;
			}

		} else {
			if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
			{
				error_log("Timeout getting challenge.");
				echo "FRITZ!Box hat nicht innerhalb der vorgegebenen Zeit geantwortet.";
			} 
			elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT && strpos(curl_error($ch), "refused") !== false) 
			{
					error_log("Refused getting challenge.");
					echo "FRITZ!Box hat Verbindungsversuch abgelehnt. Eventuell Port überprüfen.";
			}
			else
			{
				error_log("Failed getting challenge.");
				echo "FRITZ!Box ist nicht erreichbar";
			}
			
			curl_close($ch);
			return;
		}
	}

	$challengeObject = @simplexml_load_string($challengeXML);
    
	//Check if correct data was transmitted at all
	if(!isset($challengeObject->Challenge))
	{       
		error_log("Challenge was not set.");
		echo "Gerät hat geantwortet, ist aber keine FRITZ!Box";
        curl_close($ch);
        return;	
	}
    
	$challenge = $challengeObject->Challenge;

	//Calculate challenge response
    $challengeResponse = $challenge . "-" . $connection->FritzBoxPassword ;
	$encodedChallenge = md5(mb_convert_encoding($challengeResponse, "UCS-2LE", "UTF-8"));

	//Login to FRITZ!Box and get SID
	$url = $requestUrl . "login_sid.lua?username=" . $connection->FritzBoxUsername . "&response=" . $challenge . "-" . $encodedChallenge;
	curl_setopt($ch, CURLOPT_URL, $url);
	$loginXML = curl_exec($ch);

	if (!$loginXML)
	{
		error_log("Failed to get login.");
		echo "FRITZ!Box ist nicht erreichbar";
		curl_close($ch);
		return;
	}

	$loginObject= @simplexml_load_string($loginXML);

	//Check if correct data was transmitted at all
	if(!isset($loginObject->BlockTime))
	{       
		error_log("Blocktime was not set.");
		echo "Gerät hat geantwortet, ist aber keine FRITZ!Box";
        curl_close($ch);
        return;	
	}
    
	//Read SID
	$sid = trim($loginObject->SID);

	if($sid == "0000000000000000")
	{
		error_log("Failed to get sid. Blocktime: " . $loginObject->BlockTime);
		echo "<span merge>Login fehlgeschlagen. Benutzer oder Passwort falsch. <br>Sperre bis zum nächsten Versuch</span>: " . $loginObject->BlockTime . " <span merge>Sekunden</span>";
		curl_close($ch);
		return;
	}

	//Check if correct rights are set
		$index = 0;
		foreach ($loginObject->Rights->Name as $name) {
			if ($name != "HomeAuto" && $loginObject->Rights->Access[$index] != "0") {
				error_log("Too many rights.");
				echo "Benutzer hat zu viele Rechte. <br>Bitte beschränken Sie die Rechte des Benutzers in Ihrer FRITZ!Box auf ausschließlich 
				Smart Home oder legen Sie einen neuen Benutzer an. (Siehe 'Legen Sie einen neuen Benutzer an' oben)";
				curl_close($ch);
				return;
			}
			
			if($name == "HomeAuto" && $loginObject->Rights->Access[$index] != "2") {
				error_log("Too little rights.");
				echo "Benutzer hat keine Smart Home Rechte. <br>Bitte geben Sie Smart Home für diesen Benutzer frei.";
				curl_close($ch);
				return;
			}
			$index++;
		}

	//Get device list
	$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getdevicelistinfos&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$deviceXML = curl_exec($ch);
    $switchList = "";
    $connection->Legacy = false;

	if (!$deviceXML || empty($deviceXML))
	{
        //If no device list is available, check if it is a legacy device. Pull switch list
        $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchlist&sid=" . $sid;
        curl_setopt($ch, CURLOPT_URL, $url);
        $switchList = curl_exec($ch);
        
        if(!$switchList)
        {
            error_log("Failed to get devices.");
            echo "Geräteliste konnte nicht abgerufen werden. Möglicherweise sind die erforderlichen Rechte für Smart Home für diesen Benutzer nicht gesetzt oder die verwendete FRITZ!OS Version wird nicht unterstützt.";
            curl_close($ch);
            return;
        }
        else
        {
            $connection->Legacy = true;
        }
	}

    $deviceListArray = array();
    
    if ($connection->Legacy)
    {
        //Get all switch information
        $switchList = str_replace(array("\r", "\n"), '', $switchList);
        $switchArray = explode(",", $switchList);
        
        foreach($switchArray as $switch)
        {
            $switch = trim($switch);
            $switch = str_replace(array("\r", "\n"), '', $switch);
            
            //Get name
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchname&sid=" . $sid . "&ain=" . $switch;
            curl_setopt($ch, CURLOPT_URL, $url);
            $switchName = curl_exec($ch);
            $switchName = @str_replace(array("\r", "\n"), '', $switchName);
            
            //Get state
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchstate&sid=" . $sid . "&ain=" . $switch;
            curl_setopt($ch, CURLOPT_URL, $url);
            $switchState = curl_exec($ch);
            $switchState = @str_replace(array("\r", "\n"), '', $switchState);
            
            //Get switch present
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchpresent&sid=" . $sid . "&ain=" . $switch;
            curl_setopt($ch, CURLOPT_URL, $url);
            $switchPresent = curl_exec($ch);
            $switchPresent = @str_replace(array("\r", "\n"), '', $switchPresent);
            
            //Create device
            $device = new Device();
			$device->DeviceClasses[] = CLASS_POWERSWITCH;
            $device->Identifier = $switch;
            $device->Manufacturer = "Fritz!Box";
            $device->Name = isset($switchName) ? $switchName : "";
            $device->Present = ($switchPresent == "1");
            $device->SwitchedOn = ($switchState == "1");
            
            $deviceListArray[] = $device;
        }
    }
    else
    {
        //Parse device list
        $deviceList = @simplexml_load_string($deviceXML);

        if ($deviceList)
        {
            foreach($deviceList->device as $device)
            {
                $deviceListArray[] = parseDevice($device);
            }
            
            foreach($deviceList->group as $group)
            {
                $deviceListArray[] = parseDevice($group);
            }
            
            //Get template list
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=gettemplatelistinfos&sid=" . $sid;
            curl_setopt($ch, CURLOPT_URL, $url);
            $templateXML = curl_exec($ch);
            $templateList = @simplexml_load_string($templateXML);
            if ($templateList)
            {
                foreach($templateList->template as $template)
                {
                    $deviceListArray[] = parseTemplate($template);
                }
            }
            
            foreach($deviceListArray as $device)
            {
                // set connection number
                $device->ConnectionNumber = $connectionNumber;
                
                // transfer user input values if possible
                if(!$device->isDeviceClass(CLASS_THERMOSTAT))
                {
                    continue;
                }
                
                $customerDevice = $_SESSION['customer']->getDevice($device->Identifier);
                if ($customerDevice != null) 
                {
                    $device->TemperatureOff = $customerDevice->TemperatureOff;
                    $device->TemperatureOn = $customerDevice->TemperatureOn;
                    $device->AdditionalSwitch = $customerDevice->AdditionalSwitch;
                }
            }
        }
    }
    
    $_SESSION['customer']->removeDevices($connectionNumber);
    
    // Delete devices which are duplicate in other connections
    foreach($deviceListArray as $device)
    {
        $_SESSION['customer']->removeDevice($device);
    }
    
	$_SESSION['customer']->DeviceList = array_merge($_SESSION['customer']->DeviceList, $deviceListArray);
        
    $_SESSION['customer']->TTL = strtotime('+3 months');
    
	$connection->LastConnectionState = CONN_OK;
	$connection->LastUpdatedDevices = time();
    $_SESSION['customer']->addOrUpdateConnection($connection);
 
	//Logout
	$url = $requestUrl . "login_sid.lua?logout=1&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$logoutXML = curl_exec($ch);

    // Delete devices with an unknown connection number
    foreach($_SESSION['customer']->DeviceList as $device)
    {
        if(!$_SESSION['customer']->isConnection($device->ConnectionNumber))
        {
            $_SESSION['customer']->removeDevice($device);
        }            
    }
    
	//All OK
	curl_close($ch);
	if ($connection->UseIPv6)
	{
		echo "OK6";
	} else
	{
		echo "OK4";
	}
    
    
	return;
?>