<?php

require_once __DIR__.'/vendor/autoload.php';

use Heise\Shariff\Backend;

class ShariffBackend
{
    public $configuration = array(
        'cache' => [
            'ttl' => 60
        ],
        'domains' => [
            'www.fbsmarthome.site'
        ],
        'services' => [
            'Facebook'
        ],
        'Facebook' => [
            'app_id' => '399738323796137'
        ]
    );

    public function run()
    {
        header('Content-type: application/json');
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        
        $url = isset($_GET['url']) ? $_GET['url'] : '';
        if ($url) {
            $shariff = new Backend($this->configuration);
            echo json_encode($shariff->get($url));
        } else {
            echo json_encode(null);
        }
    }
}
$backend = new ShariffBackend();
$backend->configuration['Facebook']['secret'] = $_SERVER['FB_SECRET'];
$backend->run();

