<?php

global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/postgresql.inc.php');

function refreshAccessToken($customer)
{
    if ($customer == null || empty(trim($customer->RefreshToken))) {
        return null;
    }

    // Refresh token on LWA
    $url = 'https://api.amazon.com/auth/o2/token/';

    $header = array('Content-Type: application/x-www-form-urlencoded;charset=UTF-8');

    $data = array(
        'grant_type' => 'refresh_token',
        'client_id' => $_SERVER['EVENT_CLIENT_ID'],
        'client_secret' => $_SERVER['EVENT_CLIENT_SECRET'],
        'refresh_token' => $customer->RefreshToken
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    $result = curl_exec($ch);

    if (curl_getinfo($ch, CURLINFO_HTTP_CODE) === 200) {
        $decoded = json_decode($result);
        // Got new access token
        if (isset($decoded->access_token) && strpos($decoded->access_token, "Atza") === 0) {
            // Save to customer data
            $customer->AccessToken = $decoded->access_token;
            $customer->RefreshToken = $decoded->refresh_token;
            $customer->TokenValidity = time() + $decoded->expires_in;

            saveCustomerRDS($customer); 

            return $customer;
        }
    } else {
        error_log("Token refresh failed: " . curl_getinfo($ch, CURLINFO_HTTP_CODE) . " " . $result);
    }


    unset($ch);
    return null;
}
?>