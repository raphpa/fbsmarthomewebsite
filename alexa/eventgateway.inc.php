<?php

global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/alexa/oauth.inc.php');

class AlexaAddOrUpdateHeader
{
    public $messageId;
    public $namespace = "Alexa.Discovery";
    public $name = "AddOrUpdateReport";
    public $payloadVersion = "3";

	function __construct() {
		$this->messageId = guidv4(openssl_random_pseudo_bytes(16));
	}
}

class AlexaDeleteReportHeader
{
    public $messageId;
    public $namespace = "Alexa.Discovery";
    public $name = "DeleteReport";
    public $payloadVersion = "3";

	function __construct() {
		$this->messageId = guidv4(openssl_random_pseudo_bytes(16));
	}
}

class AlexaStateReportHeader
{
    public $messageId;
    public $correlationToken;
    public $namespace = "Alexa";
    public $name = "StateReport";
    public $payloadVersion = "3";

	function __construct($correlationToken) {
        $this->messageId = guidv4(openssl_random_pseudo_bytes(16));
        $this->correlationToken = $correlationToken;
	}
}

class AlexaAddOrUpdatePayload
{
    public $endpoints = array();
    public $scope;
    
    function __construct($customer) {
        $this->scope = new AlexaScope($customer);
        
        foreach($customer->DeviceList as $device)
		{
            $endpoint = new AlexaEndpoint();
            
            // endpoint name
            $endpoint->endpointId = $device->Identifier;
            $endpoint->manufacturerName = $device->Manufacturer;
            $endpoint->friendlyName = $device->Name;
            $endpoint->description = $device->ProductName;

            // add additional attributes
            $endpoint->additionalAttributes = new AlexaAdditionalAttributes();
            $endpoint->additionalAttributes->manufacturer = $device->Manufacturer;
            $endpoint->additionalAttributes->model = $device->ProductName;
            $endpoint->additionalAttributes->customIdentifier = $device->Identifier;

            if(!empty($device->Firmware))
            {
                $endpoint->additionalAttributes->firmwareVersion = $device->Firmware;
            }

            $endpoint->connections[]= array("type" => "UNKNOWN", "value" => "DECT");
 
            // add capabilites
            // temperature sensor
            if($device->isDeviceClass(CLASS_TEMPERATURESENSOR) || ($device->isDeviceClass(CLASS_THERMOSTAT) && $device->IsGroup))
            {
                $endpoint->displayCategories[] = "TEMPERATURE_SENSOR";
                
                // add temperature sensor capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.TemperatureSensor";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "temperature";
                $property->proactivelyReported = false;
                $property->retrievable = true;
                
                $capability->properties = $property;
                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
            }
            
            // thermostat
            if($device->isDeviceClass(CLASS_THERMOSTAT))
            {
                $endpoint->displayCategories[] = "THERMOSTAT";
                
                // add thermostat capability                
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.ThermostatController";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "targetSetpoint";
                $property->supported[]["name"] = "thermostatMode";
                $property->proactivelyReported = false;
                $property->retrievable = true;
                
                $capability->properties = $property;

                // add configuration
                $capability->configuration = new AlexaConfiguration();
                $capability->configuration->supportsScheduling = false;
                $capability->configuration->supportedModes[] = "HEAT";
                $capability->configuration->supportedModes[] = "ECO";
                $capability->configuration->supportedModes[] = "OFF";
                $endpoint->capabilities[] = $capability;

                // add power controller capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.PowerController";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "powerState";
                $property->proactivelyReported = false;
                $property->retrievable = true;
                
                $capability->properties = $property;
                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
            }
            
            // power switch
            if($device->isDeviceClass(CLASS_POWERSWITCH))
            {
                $endpoint->displayCategories[] = "SMARTPLUG";
                
                // add power controller capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.PowerController";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "powerState";
                $property->proactivelyReported = false;
                $property->retrievable = true;
                
                $capability->properties = $property;
                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
            }
            
            // all devices
            if($device->isDeviceClass(CLASS_TEMPERATURESENSOR) || $device->isDeviceClass(CLASS_THERMOSTAT) || $device->isDeviceClass(CLASS_POWERSWITCH))
            {
                // add endpoint health capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.EndpointHealth";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "connectivity";
                $property->proactivelyReported = false;
                $property->retrievable = true;
                
                $capability->properties = $property;
                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
            }          

            // templates
            if($device->isDeviceClass(CLASS_TEMPLATE))
            {
                $endpoint->displayCategories[] = "SCENE_TRIGGER";
                unset($endpoint->additionalAttributes);

                // add power controller capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.SceneController";
                $capability->version = "3";
                $capability->supportsDeactivation = false;

                unset($capability->configuration);
                unset($capability->properties);
                unset($endpoint->connections);
                $endpoint->capabilities[] = $capability;
            }
            
            // add cookie
            $endpoint->cookie["customerId"] = $customer->CustomerId;
            
            // add endpoint
            $this->endpoints[] = $endpoint;  
               
            // If device is in the virtual device list, create a powercontroller for it
            if($device->AdditionalSwitch)
            {
                $endpoint = new AlexaEndpoint();
                
                // endpoint name
                $endpoint->endpointId = $device->Identifier . "-virtual";
                $endpoint->manufacturerName = $device->Manufacturer;
                $endpoint->friendlyName = $device->Name . " S";
                $endpoint->description = $device->ProductName . " S";

                // add additional attributes
                $endpoint->additionalAttributes = new AlexaAdditionalAttributes();
                $endpoint->additionalAttributes->manufacturer = $device->Manufacturer;
                $endpoint->additionalAttributes->model = $device->ProductName;
                $endpoint->additionalAttributes->customIdentifier = $device->Identifier;

                if(!empty($device->Firmware))
                {
                    $endpoint->additionalAttributes->firmwareVersion = $device->Firmware;
                }

                $endpoint->displayCategories[] = "SMARTPLUG";
                
                $endpoint->connections[]= array("type" => "UNKNOWN", "value" => "VIRTUAL");

                // add power controller capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.PowerController";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "powerState";
                $property->proactivelyReported = false;
                $property->retrievable = false;
                
                $capability->properties = $property;

                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
                
                // add endpoint health capability
                $capability = new AlexaCapability();
                $capability->interface = "Alexa.EndpointHealth";
                $capability->version = "3";
                
                $property = new AlexaProperty();
                $property->supported[]["name"] = "connectivity";
                $property->proactivelyReported = false;
                $property->retrievable = false;
                
                $capability->properties = $property;

                unset($capability->configuration);
                $endpoint->capabilities[] = $capability;
                
                // add cookie
                $endpoint->cookie["customerId"] = $customer->CustomerId;
                
                // add endpoint
                $this->endpoints[] = $endpoint;
            }
        }  
    }
}

class AlexaDeleteReportPayload
{
    public $endpoints = array();
    public $scope;
    
    function __construct($oldCustomer, $customer) {
        $this->scope = new AlexaScope($customer);
        
        // run through old device list and check if any device has been deleted
        foreach($oldCustomer->DeviceList as $device)
		{
            $result = $customer->hasDevice($device->Identifier);
            // if device is not in current device list, set as deleted
            if($result == false)
            {
                $this->endpoints[]["endpointId"] = $device->Identifier;
                
                if($device->AdditionalSwitch)
                {
                    $this->endpoints[]["endpointId"] = $device->Identifier . "-virtual";
                }
            }
            else
            {
                if($oldCustomer->hasVirtual($device->Identifier) == true && $customer->hasVirtual($device->Identifier) == false)
                {
                    $this->endpoints[]["endpointId"] = $device->Identifier . "-virtual";
                }
            }
        }
    }
}

class AlexaStateReportEndpoint
{
    public $scope;
    public $endpointId;
    public $cookie;
        
    function __construct($customer, $device) {
        $this->scope = new AlexaScope($customer);
        $this->endpointId = $device->Identifier;
        $this->cookie["customerId"] = $customer->CustomerId;
    }       
}

class AlexaStateReportContext
{
    public $properties;

    function __construct($customer, $device) {
        // add properties

        // power switch
        if($device->isDeviceClass(CLASS_POWERSWITCH))
        {
            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.PowerController";
            $property->name = "powerState";
            $property->value = $device->SwitchedOn ? "ON" : "OFF";
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;

            if($device->isDeviceClass(CLASS_TEMPERATURESENSOR))
            {
                $property = new AlexaReportProperty();
                $property->namespace = "Alexa.TemperatureSensor";
                $property->name = "temperature";
                $property->value = new AlexaValue($device->Temperature, "CELSIUS");
                $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
                $property->uncertaintyInMilliseconds = 900000;
                $this->properties[] = $property;
            }
        }
        // thermostat
        elseif($device->isDeviceClass(CLASS_THERMOSTAT))
        {
            // Temperature
            $temperature = 0;
            if($device->IsGroup)
            {
                $temperature = $device->TemperatureActualValue;
                if(count($device->GroupMembers) > 0)
                {
                    $firstGroupMember = $customer->getDevice($device->GroupMembers[0]);
                    if ($firstGroupMember !== null)
                    {
                        $temperature = $firstGroupMember->Temperature;
                    }
                }
            }
            else {
                $temperature = $device->Temperature;
            }

            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.TemperatureSensor";
            $property->name = "temperature";
            $property->value = new AlexaValue($device->Temperature, "CELSIUS");
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;

            // Temperature setpoint
            $temperatureSetpoint = 0;
            if($device->TemperatureSetpoint == TEMPERATURE_ON)
            {
                $temperatureSetpoint = 28;
            }
            elseif($device->TemperatureSetpoint == TEMPERATURE_OFF)
            {
                $temperatureSetpoint = 8;
            }
            else
            {
                $temperatureSetpoint = $device->TemperatureSetpoint;
            }

            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.ThermostatController";
            $property->name = "targetSetpoint";
            $property->value = new AlexaValue($temperatureSetpoint, "CELSIUS");
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;

            // Thermostat mode
            $thermostatMode = THERMOSTATMODE_OFF;
            if($device->TemperatureSetpoint == TEMPERATURE_OFF)
            {
                $thermostatMode = "OFF";
            }
            elseif($device->TemperatureSetpoint <= $device->TemperatureSetback
                    && $device->TemperatureSetback < $device->TemperatureComfort
                    && $device->TemperatureSetback != TEMPERATURE_OFF
                    && $device->TemperatureSetback != TEMPERATURE_ON)
            {
                $thermostatMode = "ECO";
            }
            else {
                $thermostatMode = "HEAT";
            }

            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.ThermostatController";
            $property->name = "thermostatMode";
            $property->value = $thermostatMode;
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;

            // Power state
            $powerstate = "OFF";
            if($device->TemperatureSetpoint == TEMPERATURE_OFF || $device->TemperatureSetpoint == $device->TemperatureOff)
            {
                $powerstate = "OFF";
            }
            else
            {
                $powerstate = "ON";
            }

            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.PowerController";
            $property->name = "powerState";
            $property->value = $powerstate;
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;
        }
        // temperature sensor
        elseif($device->isDeviceClass(CLASS_TEMPERATURESENSOR))
        {
            $property = new AlexaReportProperty();
            $property->namespace = "Alexa.TemperatureSensor";
            $property->name = "temperature";
            $property->value = new AlexaValue($device->Temperature, "CELSIUS");
            $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
            $property->uncertaintyInMilliseconds = 900000;
            $this->properties[] = $property;
        }

        // endpoint health
        $property = new AlexaReportProperty();
        $property->namespace = "Alexa.EndpointHealth";
        $property->name = "connectivity";
        $property->value = new AlexaValue($device->Present ? "OK" : "UNREACHABLE");
        $property->timeOfSample = gmdate("Y-m-d\TH:i:s\Z");
        $property->uncertaintyInMilliseconds = 900000;
        $this->properties[] = $property;
    }
}

class AlexaReportProperty
{
    public $namespace;
    public $name;
    public $value;
    public $timeOfSample;
    public $uncertaintyInMilliseconds;
}

class AlexaValue
{
    public $value;
    public $scale;

    function __construct($value, $scale = null) {
        $this->value = $value;
        if ($scale === null)
        {
            unset($this->scale);
        }
        else
        {
            $this->scale = $scale;
        }
    }
}

class AlexaScope
{
    public $type = "BearerToken";
    public $token;
    
	function __construct($customer) {
		$this->token = $customer->AccessToken;
	}
}

class AlexaCapability
{
    public $type = "AlexaInterface";
    public $interface;
    public $version;
    public $properties;
    public $configuration;
}

class AlexaConfiguration
{
    public $supportsScheduling;
    public $supportedModes = array();
}

class AlexaProperty
{
    public $supported = array();
    public $proactivelyReported;
    public $retrievable;
}

class AlexaEndpoint
{
    public $endpointId = ""; 
    public $friendlyName = "";
    public $description = "";
    public $manufacturerName = "";
    public $additionalAttributes;
    public $displayCategories = array(); 
    public $cookie = null; 
    public $capabilities = array();  
    public $connections = array();
}

class AlexaAdditionalAttributes
{
    public $manufacturer = "";
    public $model = "";
    public $customIdentifier = "";
    public $firmwareVersion = "0";
}

class AlexaAddOrUpdateEvent
{
    public $header;
    public $payload;
    
	function __construct($customer) {
		$this->header = new AlexaAddOrUpdateHeader();
        $this->payload = new AlexaAddOrUpdatePayload($customer);
	}
}

class AlexaDeleteReportEvent
{
    public $header;
    public $payload;
    
	function __construct($oldCustomer, $customer) {
		$this->header = new AlexaDeleteReportHeader();
        $this->payload = new AlexaDeleteReportPayload($oldCustomer, $customer);
	}
}

class AlexaStateReportEvent
{
    public $header;
    public $endpoint;
    public $payload = array();
    
	function __construct($correlationToken, $customer, $device) {
		$this->header = new AlexaStateReportHeader($correlationToken);
        $this->endpoint = new AlexaStateReportEndpoint($customer, $device);
	}
}
class AlexaAddOrUpdate
{
    public $event;
	function __construct($customer) {
		$this->event = new AlexaAddOrUpdateEvent($customer);
	}
}    

class AlexaDeleteReport
{
    public $event;
	function __construct($oldCustomer, $customer) {
		$this->event = new AlexaDeleteReportEvent($oldCustomer, $customer);
	}
} 

class AlexaStateReport
{
    public $event;
    public $context;

	function __construct($correlationToken, $customer, $device) {
        $this->event = new AlexaStateReportEvent($correlationToken, $customer, $device);
        $this->context = new AlexaStateReportContext($customer, $device);
	}
} 

function sendStateReportMessage($correlationToken, &$customer, $device)
{
    $sendMessage = true;
    if(!empty(trim($customer->AccessToken)))
    {
        // Check if access token is still valid. If not, try to update first
        if($customer->TokenValidity < time())
        {
            $result = refreshAccessToken($customer);
            
            if($result == null)
            {
                // Updating token failed, stop sending message
                error_log("Updating token failed");
                $sendMessage = false;
            }
            else
            {
                // Else update customer
                $customer = $result;
            }           
        }
        
        $url = "";
        // If still sending message, get customer region
        if($sendMessage)
        {
            
            // Check for region
            if(trim($customer->UserRegion) === "EU")
            {
                $url = "https://api.eu.amazonalexa.com/v3/events";
            }
            elseif(trim($customer->UserRegion) === "NA")
            {
                $url = "https://api.amazonalexa.com/v3/events";
            }
            else
            {
                error_log("Could not get region");
                $sendMessage = false;
            }
        }
        
        // If still sending message, send message to event gateway
        if($sendMessage)
        {
            $header = array(
                'Content-Type: application/json',
                'Authorization: Bearer ' . $customer->AccessToken
            );
            
            $data = new AlexaStateReport($correlationToken, $customer, $device);
            $jsonData = json_encode($data);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);  
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
            $result = curl_exec($ch);
            
            // If event gateway returns 401 Unauthorized, try updating the access token and try again
            if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 401)
            {
                $result = refreshAccessToken($customer);
                
                if($result == null)
                {
                    // Updating token failed, stop sending message
                    error_log("Refreshing token failed");
                    $sendMessage = false;
                }
                else
                {
                    // Else update customer
                    $customer = $result;
                }

                if($sendMessage)
                {
                    $header = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer ' . $customer->AccessToken
                    );
                    
                    $data = new AlexaStateReport($correlationToken, $customer, $device);
                    $jsonData = json_encode($data);
                    
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
                    $result = curl_exec($ch);
                }
            }
            // If event gateway returns 403 Forbidden, delete region
            elseif(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
            {
                $customer->UserRegion = "";
                
                error_log("No valid token anymore");
                $sendMessage = false;
            }
            
            // If event gateway returns anything other than 202 Accepted, do not continue sending
            if(curl_getinfo($ch, CURLINFO_RESPONSE_CODE) != 202)
            {
                error_log("Gateway did not accept: ". curl_getinfo($ch, CURLINFO_RESPONSE_CODE));
                $sendMessage = false;
            }
            
            curl_close($ch);
            unset($ch);
        }    
    }
}
