<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

function generateCacheLink($filename)
{
	$timestamp = filemtime($filename);
	return $filename . "?" . $timestamp;
}

function generateTimestamp($filename)
{
	$timestamp = filemtime($filename);
	return $timestamp;
}

$clientId = rawurlencode(filter_input(INPUT_GET,"client_id",FILTER_SANITIZE_STRING));
$state = rawurlencode(filter_input(INPUT_GET,"state",FILTER_SANITIZE_STRING));
$redirectURI = rawurlencode(filter_input(INPUT_GET,"redirect_uri",FILTER_SANITIZE_STRING));

if (Locale::lookup(["de"], locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE'])) == 'de' && !isset($_GET["noredirect"]))
{
    header('Location: https://www.fbsmarthome.site/oauthgoogle-de.php?client_id=' . $clientId . '&scope=profile:user_id&response_type=code&state=' . $state . '&redirect_uri=' . $redirectURI);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Integrating FRITZ!Box Smart Home devices in Google. Supported are all FRITZ!Box connected Smart Plugs and Thermostats.">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
	<link rel="manifest" href="/img/manifest.json">
	<link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon.ico">
    <link rel="alternate" hreflang="x-default" href="https://www.fbsmarthome.site/oauthgoogle.php" />
    <link rel="alternate" hreflang="de" href="https://www.fbsmarthome.site/oauthgoogle-de.php" />
    <link rel="alternate" hreflang="en" href="https://www.fbsmarthome.site/oauthgoogle.php" />
	<meta name="msapplication-config" content="/img/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<meta name="application-name" content="FB Smart Home"/>
	<meta property="og:url" content="https://www.fbsmarthome.site/oauthgoogle.php" />
	<meta property="og:title" content="FB Smart Home: Google for FRITZ!Box Smart Home" />
	<meta property="og:description" content="Integrating FRITZ!Box Smart Home devices in Google. Supported are all FRITZ!Box connected Smart Plugs and Thermostats." />
	<meta property="og:image" content="https://www.fbsmarthome.site/img/icon.jpg" />
	<meta property="og:locale" content="en_US" />
    <meta property="og:locale:alternate" content="de_DE" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="399738323796137" />
	
	<title>FB Smart Home: Google for FRITZ!Box Smart Home</title>


	<link rel="stylesheet" href="<?php echo generateCacheLink("css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/template.min.css"); ?>">
    
</head>

<body>
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mt-1">
					<h2 class="section-heading text-dark">FB Smart Home</h2>
                    <img src="img/108.jpg" alt="FB Smart Home"/>
                    <div class="col-10 mt-2 mb-3 mx-auto text-center">
                        <small><a href="oauthgoogle-de.php?noredirect&client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>">Deutsch</a>&emsp;|&emsp;<a href="oauthgoogle.php?noredirect&client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>">English</a></small>
                    </div>
					<hr class="my-2 mb-3">
				</div>
			</div>
			<div class="row">
				<div class="col-10 mx-auto text-center">
					<p>
						<strong>Login to allow Google access to control your smart home devices.</strong>
                        <div class="row">
                            <div class="col-10 mt-4 mx-auto text-center">
                                <div id="amazon-root"></div>
                                <a href="https://www.amazon.com/ap/oa?client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>" id="LoginWithAmazon">
                                    <img border="0" alt="Login with Amazon"
                                    src="img/lwaSmall.png"
                                    width="195" height="46" />
                                </a>
                            </div>
                            <div class="col-10 mt-3 mx-auto text-center">
                                <small><a href="https://www.fbsmarthome.site/?lng=en#datenschutz" target="_blank">Privacy Statement</a></small>
                            </div>
                        </div>
                        <br><br>
                        Before using this Action you have to link your FRITZ!Box at <br><br> <a href="https://www.fbsmarthome.site/?lng=en" target="_blank">https://www.fbsmarthome.site</a>
					</p>
				</div>
			</div>
		</div>	

    <!-- Bootstrap core JavaScript -->
	<script src="<?php echo generateCacheLink("js/jquery.min.js"); ?>"></script>
	<script src="<?php echo generateCacheLink("js/bootstrap.bundle.min.js"); ?>"></script>
</body>

</html>