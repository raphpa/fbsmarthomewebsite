<?php
    /**
     * Simple method to use the API from https://www.troyhunt.com/ive-just-launched-pwned-passwords-version-2/
     * Written by Jim Westergren and released to public domain
     * https://gist.github.com/JimWestergren/a4baf4716bfad6da989417a10e1ccc5f
     */
     
    $basedir = realpath(__DIR__);
    
	require_once($basedir . '/fritzbox/auxiliary.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
    require_once($basedir . '/fritzbox/connection.inc.php');
    
    header("X-Robots-Tag: noindex", true);
    
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		return;
	}
    
    session_set_cookie_params(0, "/", "", true, true);
	session_start();

    if (!isset($_SESSION['customer']) || !isset($_SESSION['connectionNumber'])) 
    {
        echo "0";
        return;
    }
    
    $sha1 = strtoupper(sha1($_SESSION['customer']->getConnection($_SESSION['connectionNumber'])->FritzBoxPassword));
    $data = file_get_contents('https://api.pwnedpasswords.com/range/'.substr($sha1, 0, 5));
    if (FALSE !== strpos($data, substr($sha1, 5))) {
        $data = explode(substr($sha1, 5).':', $data);
        $count = (int) $data[1];
        error_log("password on data breach.");
    }
    
    echo strval($count ?? 0);
?>