<?php
global $basedir;
require_once($basedir . '/vendor/autoload.php');
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/postgresql.inc.php');

use \Firebase\JWT\JWT;

function getHomegraphToken(&$ch)
{
    $json = file_get_contents("/run/secrets/homegraph");
    
    $keydata = json_decode($json);
    
    // generate request token
    $token = array(
        "iss" => $keydata->client_email,
        "scope" => "https://www.googleapis.com/auth/homegraph",
        "aud" => "https://accounts.google.com/o/oauth2/token",
        "iat" => time(),
        "exp" => time() + 3600
    );

    // sign token
    $jwt = JWT::encode($token, $keydata->private_key, 'RS256');

    // send request token
    $header = array(
        "Content-Type" => "application/x-www-form-urlencoded"
    );
    
    $data = array(
        "grant_type" => "urn:ietf:params:oauth:grant-type:jwt-bearer",
        "assertion" => $jwt
    );
    
    $url = "https://accounts.google.com/o/oauth2/token";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    $result = curl_exec($ch);
    
    // if request was ok, process received access token data
    if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200) {
        $tokendata = json_decode($result);
        return $tokendata->access_token;
    } else {
        error_log("Could not get homegraph access token: " . $result);
        return null;
    }
}

function requestSync(&$ch, $customer)
{
    $header = array(
        "Content-Type: application/json"
    );
    
    $data = array(
        "agentUserId" => $customer->CustomerId,
        "async" => true
    );

    $url = 'https://homegraph.googleapis.com/v1/devices:requestSync?key=' . $_SERVER['GOOGLE_API_KEY'];
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    $result = curl_exec($ch);

    if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200) {
        return true;
    }
    elseif (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 404) {
        error_log("Customer not connected to assistant anymore. Deleting.");
        $customer->IsGoogleAssistant = false;
        saveCustomerRDS($customer);
        return false;
    } else {
        error_log("Could not request sync: ". $result);
        return false;
    }
}

function reportHomegraphState($customer, $nosync = false, $deviceId = null)
{
    $ch = curl_init();

    // get access token
    $token = getHomegraphToken($ch);

    if ($token === null) {
        return;
    }

    // send message to homegraph
    $header = array(
        "Content-Type: application/json",
        "Authorization: Bearer " . $token,
        "X-GFE-SSL: yes"
    );
    
    $data = array(
        "requestId" => guidv4(openssl_random_pseudo_bytes(16)),
        "agentUserId" => $customer->CustomerId
    );
    
    $data["payload"] = array();
    $data["payload"]["devices"] = array();
    $data["payload"]["devices"]["states"] = array();

    if ($deviceId !== null) {
        // get device
        $device = $customer->getDevice($deviceId);

        $state = getHomegraphDeviceState($customer, $device);
        if ($state !== null) {
            $data["payload"]["devices"]["states"][$device->Identifier] = $state;
        }
    } else {
        foreach ($customer->DeviceList as $device) {
            $state = getHomegraphDeviceState($customer, $device);
            if ($state !== null) {
                $data["payload"]["devices"]["states"][$device->Identifier] = $state;
            }
        }
    }
    
    $url = "https://homegraph.googleapis.com/v1/devices:reportStateAndNotification";
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
    $result = curl_exec($ch);

    if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 200) {
        $state = true;
    } elseif (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) === 404 && $nosync === false) {
        // If device could not be found, try to sync with homegraph
        error_log("Report state failed, try to sync.");
        requestSync($ch, $customer);
        $state = true;
    } else {
        error_log("Could not update homegraph: ". $result);
        $state = false;
    }

    curl_close($ch);
    unset($ch);

    return $state;
}

function getHomegraphDeviceState($customer, $device)
{
    $data = array();

    // power switch
    if ($device->isDeviceClass(CLASS_POWERSWITCH)) {
        $data["on"] = $device->SwitchedOn;

        if ($device->isDeviceClass(CLASS_TEMPERATURESENSOR)) {
            $data["thermostatTemperatureAmbient"] = $device->Temperature;
        }
    }

    // thermostat
    elseif ($device->isDeviceClass(CLASS_THERMOSTAT)) {
        // Temperature
        $temperature = 0;
        if ($device->IsGroup) {
            $temperature = $device->TemperatureActualValue;
            if (count($device->GroupMembers) > 0) {
                $firstGroupMember = $customer->getDevice($device->GroupMembers[0]);
                if ($firstGroupMember !== null) {
                    $temperature = $firstGroupMember->Temperature;
                }
            }
        } else {
            $temperature = $device->Temperature;
        }
        $data["thermostatTemperatureAmbient"] = $temperature;

        // Temperature setpoint
        $temperatureSetpoint = 0;
        if ($device->TemperatureSetpoint == TEMPERATURE_ON) {
            $temperatureSetpoint = 28;
        } elseif ($device->TemperatureSetpoint == TEMPERATURE_OFF) {
            $temperatureSetpoint = 8;
        } else {
            $temperatureSetpoint = $device->TemperatureSetpoint;
        }
        $data["thermostatTemperatureSetpoint"] = $temperatureSetpoint;

        // Thermostat mode
        $thermostatMode = THERMOSTATMODE_OFF;
        if ($device->TemperatureSetpoint == TEMPERATURE_OFF) {
            $thermostatMode = "off";
        } elseif ($device->TemperatureSetpoint == TEMPERATURE_ON) {
            $thermostatMode = "on";
        } elseif ($device->TemperatureSetpoint <= $device->TemperatureSetback
            && $device->TemperatureSetback < $device->TemperatureComfort
            && $device->TemperatureSetback != TEMPERATURE_OFF
            && $device->TemperatureSetback != TEMPERATURE_ON) {
            $thermostatMode = "eco";
        } else {
            $thermostatMode = "heat";
        }
        $data["thermostatMode"] = $thermostatMode;

        // Power state
        $powerstate = false;
        if ($device->TemperatureSetpoint == TEMPERATURE_OFF || $device->TemperatureSetpoint == $device->TemperatureOff) {
            $powerstate = false;
        } else {
            $powerstate = true;
        }
        $data["on"] = $powerstate;
    }
    // temperature sensor
    elseif ($device->isDeviceClass(CLASS_TEMPERATURESENSOR)) {
        $data["thermostatTemperatureAmbient"] = $device->Temperature;
    } else {
        $data = null;
    }

    return $data;
}
