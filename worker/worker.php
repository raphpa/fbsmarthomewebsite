<?php
if (php_sapi_name() != 'cli') {
    die();
}

set_time_limit(3600);
$GLOBALS['callbackCount'] = 0;
$timeout = 10;

$basedir = realpath(__DIR__);
require_once($basedir . '/vendor/autoload.php');
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/postgresql.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/fritzbox.inc.php');
require_once($basedir . '/assistant/homegraph.inc.php');

// On start delete outdated customers
$affected = clearOutdatedEntries();
if($affected !== false) {
    echo "Cleared " . $affected . " customers\n";
}

// Open redis connection
$redis = new Predis\Client([
    'scheme'   => 'tcp',
    'host'     => 'redis',
    'port'     => 6379,
    'database' => REDIS_LOCKS,
    'password' => $_ENV['REDIS_PASS']
]);


// Open RabbitMQ connection
$connection = new PhpAmqpLib\Connection\AMQPStreamConnection('rabbitmq', 5672, $_ENV['MQ_USER'], $_ENV['MQ_PASS'], '/', false, 'AMQPLAIN', null, 'en_US', 3.0, 120.0, null, true, 60);

$channel = $connection->channel();
$channel->basic_qos(null, 1, null);

// Declare queue
$channel->exchange_declare('fbsmarthome-exchange', 'direct', false, false, false);
$channel->queue_declare('fbsmarthome', false, false, false, false, false,
                            array
                            (
                                'x-max-priority' => array('I',10),
                                'x-dead-letter-exchange' => array('S','fbsmarthome-retry-exchange'), 
                                'x-dead-letter-routing-key' => array('S','fbsmarthome-retry')
                            )
                        );
$channel->queue_bind('fbsmarthome','fbsmarthome-exchange','fbsmarthome');

// Declare retry queue
$channel->exchange_declare('fbsmarthome-retry-exchange', 'direct', false, false, false);
$channel->queue_declare('fbsmarthome-retry', false, false, false, false, false,
                            array
                            (
                                'x-max-priority' => array('I',10),
                                'x-dead-letter-exchange' => array('S','fbsmarthome-exchange'), 
                                'x-dead-letter-routing-key' => array('S','fbsmarthome'),
                                'x-message-ttl'=>array('I', 1000)
                            )
                        );
$channel->queue_bind('fbsmarthome-retry','fbsmarthome-retry-exchange','fbsmarthome-retry');            
                
// Callback function when new message arrives
$callback = function($message) use (&$redis) {

    $correlationToken = null;
    $customerId = null;
    $deviceId = null;
    $temperature = null;
    $commandList = null;

    // Read message redelivery counter
    $properties = $message->get_properties();
    $applicationHeaders = isset($properties['application_headers']) ? $properties['application_headers'] : null;
    $xdeath = isset($applicationHeaders) ? $applicationHeaders->getNativeData()['x-death'] : null;
    $retryCount = isset($xdeath) ? $xdeath[0]['count'] : 0;
    
    // If message is redelivered 90 times (about 1.5 min), ack message and delete it from queue
    if ($retryCount >= 90)
    {
        echo "Processing: " . trim($customerId) . ", Deleting after timeout\n"; 
        
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
        return;
    }
    
    // decode received message
    $messageDecoded = json_decode($message->body, true);

    // Get message type
    $messageType = $messageDecoded['message'];
    
    // decode customer, device, temperature, commandlist depending on message type
    if($messageType === "GetDeviceList")
    {
        $customerId = trim($messageDecoded['customer']);
    }
    elseif($messageType === "TurnOnDevice" || $messageType === "TurnOffDevice")
    {
        $customerId = trim($messageDecoded['customer']);
        $deviceId = trim($messageDecoded['device']);
    }
    elseif($messageType === "SetTemperature")
    {
        $customerId = trim($messageDecoded['customer']);
        $deviceId = trim($messageDecoded['device']);
        $temperature = floatval(trim($messageDecoded['temperature']));
    }
    elseif($messageType === "ApplyTemplate")
    {
        $customerId = trim($messageDecoded['customer']);
        $deviceId = trim($messageDecoded['device']);
    }
    elseif($messageType === "ReportState")
    {
        $correlationToken = trim($messageDecoded['correlationToken']);
        $customerId = trim($messageDecoded['customer']);
        $deviceId = trim($messageDecoded['device']);
    }
    elseif($messageType === "HomeGraph")
    {
        $customerId = trim($messageDecoded['customer']);
    }
    elseif($messageType  === "MultipleCommands")
    {
        $customerId = trim($messageDecoded['customer']);
        $commandList = json_decode($messageDecoded['commandList'], true);
    }
    else
    {
        error_log("No valid smart home message");
    }  
    
    // Handle GetDeviceList request
    if($messageType === "GetDeviceList" && !empty($customerId))
    {        
        // Try to fetch lock
        $lock = $redis->get($customerId);
        if ($lock === null)
        {
            // If no lock is set, set lock
            $lock = $redis->set($customerId, "GetDeviceList", 'EX', '60', 'NX');
        }
        elseif($lock === "GetDeviceList")
        {
            // If lock is set and is GetDeviceList, delete message from queue
            $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);  
            return;
        }
        else
        {
            // If lock is set and is not GetDeviceList, put message into the dead letter queue
            // and redeliver it later, another worker thread is working on it
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", GetDeviceList\n"; 
        
        // get device list from FRITZ!Box and save customer
        foreach($customer->ConnectionList as $connection)
        {
            getDevices($customer, $connection, false);
        }
        saveCustomerRDS($customer);
    }
    // Handle TurnOnDevice request
    elseif($messageType === "TurnOnDevice" && !empty($customerId) && !empty($deviceId))
    {
        // Try to lock customer
        $lock = $redis->set($customerId, "TurnOn", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        // Get device
        $device = null;
        if ($customer != null)
        {
            $device = $customer->getDevice($deviceId);
            if ($device != null)
            {
                echo "Processing: " . $customerId . ", " . strval($device->ConnectionNumber) . ", TurnOn\n"; 
                
                // handle turn on request
                handleTurnOn($device, $customer);

                // if customer uses google assistant, inform homegraph about the change
                if($customer->IsGoogleAssistant)
                {
                    echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", Homegraph for " . $deviceId . "\n";
                    reportHomegraphState($customer);
                }
            }
            else
            {
                error_log("Could not find device");
            }
        }
        else
        {
            error_log("Could not find customer");
        }
    }
    // Handle TurnOffDevice request
    elseif($messageType === "TurnOffDevice" && !empty($customerId) && !empty($deviceId))
    {
        // Try to lock customer
        $lock = $redis->set($customerId, "TurnOff", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        // Get device
        $device = null;
        if ($customer != null)
        {
            $device = $customer->getDevice($deviceId);
            if ($device != null)
            {
                echo "Processing: " . $customerId . ", " . strval($device->ConnectionNumber) . ", TurnOff\n"; 
                
                // handle turn off request
                handleTurnOff($device, $customer);

                // if customer uses google assistant, inform homegraph about the change
                if($customer->IsGoogleAssistant)
                {
                    echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", Homegraph for " . $deviceId . "\n";
                    reportHomegraphState($customer);
                }
            }
            else
            {
                error_log("Could not find device");
            }
        }
        else
        {
            error_log("Could not find customer");
        }
    }
    // Handle SetTemperature request
    elseif($messageType === "SetTemperature" && !empty($customerId) && !empty($deviceId) && !empty($temperature))
    {
        // Try to lock customer
        $lock = $redis->set($customerId, "SetTemperature", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        // Get device
        $device = null;
        if ($customer != null)
        {
            $device = $customer->getDevice($deviceId);
            if ($device != null)
            {
                echo "Processing: " . $customerId . ", " . strval($device->ConnectionNumber) . ", SetTemperature\n"; 
                
                // handle set temperature request
                handleSetTemperature($device, $temperature, $customer);

                // if customer uses google assistant, inform homegraph about the change
                if($customer->IsGoogleAssistant)
                {
                    echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", Homegraph for " . $deviceId . "\n";
                    reportHomegraphState($customer);
                }
            }
            else
            {
                error_log("Could not find device");
            }
        }
        else
        {
            error_log("Could not find customer");
        }
    }
    // Handle ApplyTemplate request
    elseif($messageType === "ApplyTemplate" && !empty($customerId) && !empty($deviceId))
    {
        // Try to lock customer
        $lock = $redis->set($customerId, "ApplyTemplate", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        // Get device
        $device = null;
        if ($customer != null)
        {
            $device = $customer->getDevice($deviceId);
            if ($device != null)
            {
                echo "Processing: " . $customerId . ", " . strval($device->ConnectionNumber) . ", ApplyTemplate\n"; 
                
                // handle turn on request
                handleApplyTemplate($device, $customer);
            }
            else
            {
                error_log("Could not find device");
            }
        }
        else
        {
            error_log("Could not find customer");
        }
    }
    elseif($messageType === "ReportState" && !empty($correlationToken) && !empty($customerId) && !empty($deviceId))
    {        
        // Try to lock customer
        $lock = $redis->set($customerId, "ReportState", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        
        // Get device
        $device = null;
        if ($customer != null)
        {
            $device = $customer->getDevice($deviceId);
            if ($device != null)
            {

                echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", ReportState for " . $deviceId . "\n"; 
        
                getDevices($customer, $customer->getConnection($device->ConnectionNumber), false);
                sendStateReportMessage($correlationToken, $customer, $device);
                saveCustomerRDS($customer);               
            }
            else
            {
                error_log("Could not find device");
            }
        }
        else
        {
            error_log("Could not find customer");
        }
    }
    elseif($messageType === "HomeGraph" && !empty($customerId))
    {   
        // Get customer
        $customer = getCustomerRDS($customerId);
        
        if ($customer != null)
        {
                echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", Homegraph\n"; 
        
                // If customer is using google, send report to homegraph
                if($customer->IsGoogleAssistant)
                {
                    reportHomegraphState($customer, true);
                }
                else
                {
                    error_log("Customer not using google");
                }
        }
        else
        {
            error_log("Could not find customer");
        }

        // prevent from unlocking (was not locked)
        $customerId = "";
    }
    elseif($messageType === "MultipleCommands" && !empty($customerId) && !empty($commandList))
    {
        // Try to lock customer
        $lock = $redis->set($customerId, "MultipleCommands", 'EX', '60', 'NX');
        
        // If adding failed, customer is already processing in another worker thread
        // Reject message and put it into the dead letter queue for redelivering later
        if ($lock === null)
        {
            $message->delivery_info['channel']->basic_nack($message->delivery_info['delivery_tag']);
            return;
        }

        // Get customer
        $customer = getCustomerRDS($customerId);
        if ($customer != null)
        {
            echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", MultipleCommands\n"; 
            
            // handle multiple commands
            handleMultipleCommands($commandList, $customer);

            // if customer uses google assistant, inform homegraph about the change
            if($customer->IsGoogleAssistant)
            {
                echo "Processing: " . $customerId . ", " . strval(count($customer->ConnectionList)) . ", Homegraph for multiple\n";
                reportHomegraphState($customer);
            }
        }
        else
        {
            error_log("Could not find customer");
        } 
    }
    else
    {
        error_log("Could not decode message");
        print_r(json_decode($message->body, true), true);
    }

    // Unlock customer
    if (!empty($customerId))
    {
        $redis->del($customerId);
    }
    
    // Acknowledge message to RabbitMQ
    $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);    
    
    // Increase callback count
    $GLOBALS['callbackCount'] += 1;
    
    // Cleanup to prevent memory leaks
    unset($properties, $applicationHeaders, $xdeath, $retryCount);
    unset($message, $messageDecoded, $messageType);
    unset($customer, $device, $temperature, $commandList, $correlationToken);
    unset($customerId, $deviceId);
    gc_collect_cycles();
};

$channel->basic_consume('fbsmarthome', '', false, false, false, false, $callback);
while(count($channel->callbacks)) {
    try
    {
        $channel->wait(null, false , $timeout);
    }
    catch(PhpAmqpLib\Exception\AMQPTimeoutException $e)
    {

    }
    catch(PhpAmqpLib\Exception\AMQPRuntimeException $e)
    {
        echo 'Runtime exception: ',  $e->getMessage(), "\n";
        break;
    }
    catch(ErrorException $e)
    {
        echo 'Error exception: ',  $e->getMessage(), "\n";
        die();
    }

    if($GLOBALS['callbackCount'] >= 60)
    {
        echo "Callback limit reached\n";
        break;
    }
}

$channel->close();
$connection->close();
?>