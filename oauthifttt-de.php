<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

function generateCacheLink($filename)
{
	$timestamp = filemtime($filename);
	return $filename . "?" . $timestamp;
}

function generateTimestamp($filename)
{
	$timestamp = filemtime($filename);
	return $timestamp;
}

$clientId = rawurlencode(filter_input(INPUT_GET,"client_id",FILTER_SANITIZE_STRING));
$state = rawurlencode(filter_input(INPUT_GET,"state",FILTER_SANITIZE_STRING));
$redirectURI = rawurlencode(filter_input(INPUT_GET,"redirect_uri",FILTER_SANITIZE_STRING));

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Einbindung von FRITZ!Box Smart Home Geräten in IFTTT. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate.">
	<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
	<link rel="manifest" href="/img/manifest.json">
	<link rel="mask-icon" href="/img/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="/img/favicon.ico">
    <link rel="alternate" hreflang="x-default" href="https://www.fbsmarthome.site/oauthifttt-de.php" />
    <link rel="alternate" hreflang="de" href="https://www.fbsmarthome.site/oauthifttt-de.php" />
    <link rel="alternate" hreflang="en" href="https://www.fbsmarthome.site/oauthifttt.php" />
	<meta name="msapplication-config" content="/img/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
	<meta name="application-name" content="FB Smart Home"/>
	<meta property="og:url" content="https://www.fbsmarthome.site/oauthifttt-de.php" />
	<meta property="og:title" content="FB Smart Home: IFTTT für FRITZ!Box Smart Home" />
	<meta property="og:description" content="Einbindung von FRITZ!Box Smart Home Geräten in IFTTT. Unterstützt werden an die FRITZ!Box angebundene schaltbare Steckdosen und Heizkörperthermostate." />
	<meta property="og:image" content="https://www.fbsmarthome.site/img/icon.jpg" />
	<meta property="og:locale" content="de_DE" />
    <meta property="og:locale:alternate" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="fb:app_id" content="399738323796137" />
	
	<title>FB Smart Home: IFTTT für FRITZ!Box Smart Home</title>
	
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" href="<?php echo generateCacheLink("css/template.min.css"); ?>">

</head>

<body>
		<div class="container">
			<div class="row">
				<div class="col-12 text-center mt-1">
					<h2 class="section-heading text-dark">FB Smart Home</h2>
                    <img src="img/108.jpg" alt="FB Smart Home"/>
                    <div class="col-10 mt-2 mb-3 mx-auto text-center">
                        <small><a href="oauthifttt-de.php?noredirect&client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>">Deutsch</a>&emsp;|&emsp;<a href="oauthifttt.php?noredirect&client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>">English</a></small>
                    </div>
					<hr class="my-2 mb-3">
				</div>
			</div>
			<div class="row">
				<div class="col-10 mx-auto text-center">
					<p>
						<strong>Melden Sie sich an, um IFTTT den Zugriff auf Ihre Smart Home Geräte zu erlauben.</strong>
                        <div class="row">
                            <div class="col-10 mt-4 mx-auto text-center">
                                <div id="amazon-root"></div>
                                <a href="https://www.amazon.com/ap/oa?client_id=<?php echo $clientId; ?>&scope=profile:user_id&response_type=code&state=<?php echo $state; ?>&redirect_uri=<?php echo $redirectURI; ?>" id="LoginWithAmazon">
                                    <img border="0" alt="Login with Amazon"
                                    src="img/lwaSmall.png"
                                    width="195" height="46" />
                                </a>
                            </div>
                            <div class="col-10 mt-3 mx-auto text-center">
                                <small><a href="https://www.fbsmarthome.site/?lng=de#datenschutz" target="_blank">Datenschutzhinweise</a></small>
                            </div>
                        </div>
                        <br><br>
                        Bevor Sie diesen Skill benutzen können, müssen Sie Ihre FRITZ!Box verbinden auf <br><br> <a href="https://www.fbsmarthome.site" target="_blank">https://www.fbsmarthome.site</a>
					</p>
				</div>
			</div>
		</div>	

    <!-- Bootstrap core JavaScript -->
	<script src="<?php echo generateCacheLink("js/jquery.min.js"); ?>"></script>
	<script src="<?php echo generateCacheLink("js/bootstrap.bundle.min.js"); ?>"></script>
</body>

</html>