<?php
global $basedir;
require_once($basedir . '/vendor/autoload.php');

require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/connection.inc.php');

function getCustomerRDS($customerId)
{
    // connect to database
    $dbConnection = pg_connect("host={$_SERVER['DB_SERVER']} port={$_SERVER['DB_PORT']} dbname={$_SERVER['DB_DATABASE']} user={$_SERVER['DB_USER']} password={$_SERVER['DB_PASSWORD']}")
                        or die('DB connection failed');
              
    // query for customer
    $result = pg_query_params($dbConnection, 'SELECT * FROM fbsmarthome WHERE customerid = $1', array($customerId));
    
    // close database connection
    pg_close($dbConnection);   
    
    $customer = new Customer();
    
    // get customer
    // If no customer was found, return empty customer
    if($result != null)
    {
        // get results
        $row = pg_fetch_assoc($result);
    }
    
    $customer->CustomerId = trim($customerId);
    $customer->IsGoogleAssistant = isset($row['isgoogleassistant']) ? boolval($row['isgoogleassistant']) : false;
    $customer->UserRegion = isset($row['userregion']) ? trim($row['userregion']) : "";
    $deviceList = isset($row['devicelist']) ? json_decode($row['devicelist'], true) : array();
    $connectionList = isset($row['connectionlist']) ? json_decode($row['connectionlist'], true) : array();
    $customer->TTL = isset($row['ttl']) ? intval($row['ttl']) : 0;
    $customer->AccessToken = isset($row['accesstoken']) ? trim($row['accesstoken']) : "";
    $customer->RefreshToken = isset($row['refreshtoken']) ? trim($row['refreshtoken']) : "";
    $customer->TokenValidity = isset($row['tokenvalidity']) ? strtotime($row['tokenvalidity']) : 0;
    $customer->ApiKey = isset($row['apikey']) ? trim($row['apikey']) : "";
    
    $customer->DeviceList = array();
    foreach($deviceList as $deviceItem)
    {
        $device = createDevice($deviceItem);
        $customer->DeviceList[] = $device;
    }

    $customer->ConnectionList = array();
    foreach($connectionList as $connectionItem)
    {
        $connection = createConnection($connectionItem);
        $customer->ConnectionList[] = $connection;
    }

    if(empty($customer->ConnectionList))
    {
        $connection = new Connection();
        $connection->Number = CONNECTION_DEFAULT;
        $customer->ConnectionList[] = $connection;
    }
    
    // clear memory
    pg_free_result($result);
    unset($dbConnection, $row, $deviceList, $connectionList);

    return $customer;
}

function saveCustomerRDS($customer)
{
    if ($customer == null)
    {
        return null;
    }
    
    $connectionList = arrayCopy($customer->ConnectionList);
    foreach($connectionList as $connection)
    {
        $connection->BlockedUntil = gmdate("Y-m-d\TH:i:s.000\Z", $connection->BlockedUntil);
        $connection->LastUpdatedDevices = gmdate("Y-m-d\TH:i:s.000\Z", $connection->LastUpdatedDevices);
        $connection->LastUpdatedSessionId = gmdate("Y-m-d\TH:i:s.000\Z", $connection->LastUpdatedSessionId);
    }
    
    $customer->TTL = strtotime('+3 months');
    
    // connect to database
    $dbConnection = pg_connect("host={$_SERVER['DB_SERVER']} port={$_SERVER['DB_PORT']} dbname={$_SERVER['DB_DATABASE']} user={$_SERVER['DB_USER']} password={$_SERVER['DB_PASSWORD']}")
                        or die('DB connection failed');
    
    // create data
    $data = array(trim($customer->CustomerId), $customer->UserRegion, ($customer->IsGoogleAssistant) ? "1" : "0", 
                  json_encode($connectionList), json_encode($customer->DeviceList), $customer->AccessToken, $customer->RefreshToken,
                  gmdate("Y-m-d\TH:i:s.000\Z", $customer->TokenValidity), $customer->ApiKey, (string)$customer->TTL);
    // upsert data
    $result = pg_query_params($dbConnection, "INSERT INTO fbsmarthome (customerid, userregion, isgoogleassistant, 
                                                                       connectionlist, devicelist, accesstoken, refreshtoken, 
                                                                       tokenvalidity, apikey, ttl)
                                              VALUES ($1, $2, $3,
                                                      $4, $5, $6, $7,
                                                      $8, $9, $10)
                                              ON CONFLICT (customerid)
                                              DO
                                                UPDATE
                                                  SET userregion = $2, isgoogleassistant = $3,
                                                      connectionlist = $4, devicelist = $5, accesstoken = $6, refreshtoken = $7,
                                                      tokenvalidity = $8, apikey = $9, ttl = $10", $data 
    );

    // close database connection
    pg_close($dbConnection);   
    
    if($result == null)
    {
        unset($dbConnection, $data, $connectionListArray);
        return null;
    }
    
    $affectedRows = pg_affected_rows($result);

    // clear memory
    pg_free_result($result);
    unset($dbConnection, $data, $connectionListArray);
    
    if ($affectedRows > 0)
    {
        return $customer;
    } 
    else
    {
        return null;
    }
}

function deleteCustomerRDS($customer)
{
    // connect to database
    $dbConnection = pg_connect("host={$_SERVER['DB_SERVER']} port={$_SERVER['DB_PORT']} dbname={$_SERVER['DB_DATABASE']} user={$_SERVER['DB_USER']} password={$_SERVER['DB_PASSWORD']}")
                        or die('DB connection failed');
              
    // delete data
    $result = pg_query_params($dbConnection, "UPDATE fbsmarthome SET 
                                              devicelist = '[]',
                                              connectionlist = '[]',
                                              apikey = ''
                                              WHERE customerid = $1", 
                                              array(trim($customer->CustomerId))
    );

    // close database connection
    pg_close($dbConnection);   
    
    if($result == null)
    {
        unset($dbConnection);
        return false;
    }
    
    $affectedRows = pg_affected_rows($result);
    
    // clear memory
    pg_free_result($result);
    unset($dbConnection);
    
	return $affectedRows > 0;
}

function clearOutdatedEntries()
{
    // connect to database
    $dbConnection = pg_connect("host={$_SERVER['DB_SERVER']} port={$_SERVER['DB_PORT']} dbname={$_SERVER['DB_DATABASE']} user={$_SERVER['DB_USER']} password={$_SERVER['DB_PASSWORD']}")
    or die('DB connection failed');

    // delete data
    $result = pg_query($dbConnection, "DELETE FROM fbsmarthome WHERE ttl < (extract(epoch from now()))");

    // close database connection
    pg_close($dbConnection);   
    
    if($result === false)
    {
        unset($dbConnection);
        return false;
    }
    
    $affectedRows = pg_affected_rows($result);
    
    // clear memory
    pg_free_result($result);
    unset($dbConnection);
    
	return $affectedRows;
}

?>