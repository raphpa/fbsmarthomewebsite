<?php
global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/connection.inc.php');
require_once($basedir . '/fritzbox/postgresql.inc.php');
require_once($basedir . '/alexa/oauth.inc.php');
require_once($basedir . '/alexa/eventgateway.inc.php');

// Login to FRITZ!Box
function login(&$customer, &$connection, $ch, $forceLogin = false) {   
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));  
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            return $connectionState;
        }
        
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    // Get challenge	
	$url = $requestUrl . "login_sid.lua?sid=" . $connection->FritzBoxSessionId;
	curl_setopt($ch, CURLOPT_URL, $url);
	$challengeXML = curl_exec($ch);
    
	if ($challengeXML === false)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on getting login challenge");
            
			return CONN_TIMEOUT;
		} 
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on getting login challenge");
			return CONN_UNREACHABLE;
		}
	}
	
	$challengeObject = @simplexml_load_string($challengeXML);
    
    if(!isset($challengeObject->SID) || !isset($challengeObject->BlockTime))
    {
        error_log("Could not get SID or Blocktime, device is no Fritz!Box");
		return CONN_UNREACHABLE;
    }
    
    // Get blocktime
	$sid = trim((string)$challengeObject->SID);
	$blocktime = intval(trim((string)$challengeObject->BlockTime));
	
    // If login is blocked, fail
	if ($blocktime > 0)
	{
		error_log("Login blocked for ". $blocktime . " seconds");
        
        $connection->BlockedUntil = time() + $blocktime + 10;
        
		return CONN_BLOCKED;
	}
	
    // If no SID was received, fail
	if (!isset($sid) || trim($sid) === '')
	{
		error_log("No SID received");
		return CONN_UNREACHABLE;
	}
	
    // If SID is 0 or login is forced, login
	if ($forceLogin || $sid == NOSID)
	{
        // Create challenge response
		$challenge = trim((string)$challengeObject->Challenge);
		
		$challengeResponse = $challenge . "-" . $connection->FritzBoxPassword;
		$encodedChallenge = md5(mb_convert_encoding($challengeResponse, "UCS-2LE", "UTF-8"));
		
        // Send challenge response
		$url = $requestUrl . "login_sid.lua?username=" . $connection->FritzBoxUsername . "&response=" . $challenge . "-" . $encodedChallenge;
		curl_setopt($ch, CURLOPT_URL, $url);
		$challengeXML = curl_exec($ch);	

		if ($challengeXML === false)
		{
			if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
			{
				error_log("Connection timed out on logging in");
                return CONN_TIMEOUT;
			} 
			else
			{
				error_log("Connection failed with " . curl_errno($ch) . " on logging in");
                return CONN_UNREACHABLE;
			}
		}
		
		$challengeObject= @simplexml_load_string($challengeXML);
        
        if (!isset($challengeObject->SID))
        {
            error_log("No SID received");
            return CONN_UNREACHABLE;
        }
        
        // Get blocktime
        $blocktime = intval(trim((string)$challengeObject->BlockTime));
        
        // If login is blocked, fail
        if ($blocktime > 0)
        {
            error_log("Login blocked for ". $blocktime . " seconds");
            
            $connection->BlockedUntil = time() + $blocktime + 10;
            
            return CONN_BLOCKED;
        }
        
		$sid = trim((string)$challengeObject->SID);

		if ($sid == NOSID)
		{
			error_log("Login failed");			
			return CONN_NOT_AUTHORIZED;
		}
    }

    $index = 0;
    $writeAccess = false;

    // Check access rights
    foreach ($challengeObject->Rights->Name as $name) {
        
        if((string)$name == "HomeAuto" && (string)$challengeObject->Rights->Access[$index] == "2") {
            $writeAccess = true;
        }
        $index++;
    }
    
    if (!$writeAccess)
    {
        error_log("No SmartHome rights");
        return CONN_NOT_AUTHORIZED;
    }

    $connection->FritzBoxSessionId = $sid;
    $connection->LastUpdatedSessionId = time();
    $connection->LastConnectionState = CONN_OK;
    
    return CONN_OK;
}

// Get device list
function getDevices(&$customer, &$connection, $saveCustomer) {
    // If login is still blocked, fail
    if ($connection->BlockedUntil > time())
    {
        error_log("Login still blocked. Failing.");
        return CONN_BLOCKED;
    }

    // If customer has legagy device, return
    if ($connection->Legacy)
    {
        return $connection->LastConnectionState;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);    
 
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
 
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    $connectionState = CONN_UNKNOWN;
    $skipFirstTry = false;
    
    // If last connection is >15 minutes ago, skip try and login directly
    if(strtotime('+15 minutes', $connection->LastUpdatedSessionId) < time())
    {
        $skipFirstTry = true;
    }
    else
    {
        $connection->LastUpdatedSessionId = time();
    }

    // Else grab device list
    if(!$skipFirstTry)
    {
        $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getdevicelistinfos&sid=" . $connection->FritzBoxSessionId;
        curl_setopt($ch, CURLOPT_URL, $url);
        $deviceXML = curl_exec($ch);
    }
	
    // If error code is 403, try to login
	$login = false;
	if (!isset($deviceXML) || $deviceXML === false || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on getting device list");
			$connectionState = CONN_TIMEOUT;
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on getting device list");
			$connectionState = CONN_UNREACHABLE;
		}
		elseif ($skipFirstTry || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on getting device list");
			$connectionState = CONN_UNREACHABLE;
		}
		
		if (!$login && $connectionState != CONN_TIMEOUT)
		{
			$connectionState = CONN_UNREACHABLE;
		}
	}
    else
    {
        $connectionState = CONN_OK;
    }
    
    // try to login if access failed    
	if ($login)
	{
		$loginConnection = login($customer, $connection, $ch);
        
        // If login was OK, get device list again
        if($loginConnection == CONN_OK)
        {
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getdevicelistinfos&sid=" . $connection->FritzBoxSessionId;
            curl_setopt($ch, CURLOPT_URL, $url);
            $deviceXML = curl_exec($ch);
            
            if ($deviceXML === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on getting device list");
                    $connectionState = CONN_TIMEOUT;
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on getting device list");
                    $connectionState = CONN_UNREACHABLE;
                }
            }
            else
            {
                $connectionState = CONN_OK;
            }
        }
        else
        {
            error_log("Could not log in.");
            $connectionState = $loginConnection;
        }
	}

    // If getting list was OK, check if list is valid
    if($connectionState == CONN_OK)
    {
        $deviceList = @simplexml_load_string($deviceXML);
        if(!$deviceList)
        {
            error_log("No device list");
            $connectionState = CONN_UNREACHABLE;	
        }
        elseif ($deviceList->attributes()->version != "1")
        {
            error_log("Wrong device list");
            $connectionState = CONN_UNREACHABLE;	
        }
        // parse device list
        else
        {
            $deviceListArray = array();
            
            foreach($deviceList->device as $device)
            {
                $deviceListArray[] = parseDevice($device);
            }
            
            foreach($deviceList->group as $group)
            {
                $deviceListArray[] = parseDevice($group);
            }
            
            //Get template list
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=gettemplatelistinfos&sid=" . $connection->FritzBoxSessionId;
            curl_setopt($ch, CURLOPT_URL, $url);
            $templateXML = curl_exec($ch);
            
            if ($templateXML === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on getting template list");
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on getting template list");
                }
            }
            else
            {
                $templateList = @simplexml_load_string($templateXML);
                if(!$templateList)
                {
                    //error_log("No template list");
                }
                elseif ($templateList->attributes()->version != "1")
                {
                    error_log("Wrong template list");
                }
                else
                {
                    foreach($templateList->template as $template)
                    {
                        $deviceListArray[] = parseTemplate($template);
                    }
                } 
            }
   
            foreach($deviceListArray as $device)
            {
                // set connection number
                $device->ConnectionNumber = $connection->Number;
                
                // transfer user input values if possible
                if(!$device->isDeviceClass(CLASS_THERMOSTAT))
                {
                    continue;
                }
                
                $customerDevice = $customer->getDevice($device->Identifier);
                if ($customerDevice != null) 
                {
                    $device->TemperatureOff = $customerDevice->TemperatureOff;
                    $device->TemperatureOn = $customerDevice->TemperatureOn;
                    $device->AdditionalSwitch = $customerDevice->AdditionalSwitch;
                }
            }
            
            // Only save device list if it is not empty. FRITZ!Box might have sent faulty list
            if (count($deviceListArray) > 0)
            {
                $customer->removeDevices($connection->Number);
                
                // Delete devices which are duplicate in other connections
                foreach($deviceListArray as $device)
                {
                    $customer->removeDevice($device);
                }
                
                $customer->DeviceList = array_merge($customer->DeviceList, $deviceListArray);
                $connection->LastUpdatedDevices = time();
            }
            
            $connectionState = CONN_OK;
        }
    }

    $connection->LastConnectionState = $connectionState;
    $customer->updateConnection($connection);

    // Delete devices with an unknown connection number
    foreach($customer->DeviceList as $device)
    {
        if(!$customer->isConnection($device->ConnectionNumber))
        {
            $customer->removeDevice($device);
        }            
    }
    
    // If requested, save customer
    if ($saveCustomer)
    {
        saveCustomerRDS($customer); 
    }
    
    curl_close($ch);
    
    return $connectionState;
}

// Handle Turn On Request
function handleTurnOn($device, &$customer, $noUpdate = false) {
    // Get connection
    $connection = $customer->getConnection($device->ConnectionNumber);
    
    // If login is still blocked, fail
    if ($connection->BlockedUntil > time())
    {
        error_log("Login still blocked. Failing.");
        return CONN_BLOCKED;
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));  
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
 
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
 
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    $connectionState = CONN_UNKNOWN;
    $skipFirstTry = false;
    
    // If last connection is >15 minutes ago, skip try and login directly
    if(strtotime('+15 minutes', $connection->LastUpdatedSessionId) < time())
    {
        $skipFirstTry = true;
    }
    else
    {
        $connection->LastUpdatedSessionId = time();
    }
    
    // Else try to switch device
    if(!$skipFirstTry)
    {
        $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=setswitchon&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
        curl_setopt($ch, CURLOPT_URL, $url);
        $switchOn = curl_exec($ch);
    }
    
    // If error code is 403, try to login
	$login = false;
	if (!isset($switchOn) || $switchOn === false || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on switching device on");
			$connectionState = CONN_TIMEOUT;
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on switching device on");
			$connectionState = CONN_UNREACHABLE;
		}
		elseif ($skipFirstTry || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on switching device on");
			$connectionState = CONN_UNREACHABLE;
		}
		
		if (!$login && $connectionState != CONN_TIMEOUT)
		{
			$connectionState = CONN_UNREACHABLE;
		}
	}
    else
    {
        $connectionState = CONN_OK;
    }
    
    // try to login if access failed    
	if ($login)
	{
		$loginConnection = login($customer, $connection, $ch);
        
        // If login was OK, try to switch again
        if($loginConnection == CONN_OK)
        {
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=setswitchon&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
            curl_setopt($ch, CURLOPT_URL, $url);
            $switchOn = curl_exec($ch);
            
            if ($switchOn === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on switching device on");
                    $connectionState = CONN_TIMEOUT;
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on switching device on");
                    $connectionState = CONN_UNREACHABLE;
                }
            }
            else
            {
                $connectionState = CONN_OK;
            }
        }
        else
        {
            error_log("Could not log in.");
            $connectionState = $loginConnection;
        }
	}
    
    // If switching was OK, check if command was valid
    if($connectionState == CONN_OK)
    {
        if(trim($switchOn) != "1")
        {
            error_log("Could not switch on");
            $connectionState = CONN_FAILED;	
        }
    }
    
    // If connection was OK, try to update device list
    if(($connectionState == CONN_OK || $connectionState == CONN_FAILED) && !$noUpdate)
    {
        $connectionState = getDevices($customer, $connection, false);
    }

    // If all is ok, update cache with new value
    if($connectionState == CONN_OK)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->SwitchedOn = true;
                $customer->DeviceList[$key]->Present = true;
                break;
            }
        }
        
        // If item is group, update all members
        if($device->IsGroup)
        {
            foreach($device->GroupMembers as $member)
            {
                foreach($customer->DeviceList as $key => $deviceItem)
                {
                    // Update device
                    if($deviceItem->Identifier == $member)
                    {
                        $customer->DeviceList[$key]->SwitchedOn = true;
                        $customer->DeviceList[$key]->Present = true;
                        break;
                    }
                }
            }
        }
    }
    // Connection failed, set device to not present
    elseif($connectionState == CONN_FAILED)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->Present = false;
                break;
            }
        }
    }
    
    $connection->LastConnectionState = $connectionState;
    $customer->updateConnection($connection);
    
    // Update database
    if (!$noUpdate)
    {
        saveCustomerRDS($customer); 
    }
    
    curl_close($ch);
    
    return $connectionState;
}

// Handle Turn Off Request
function handleTurnOff($device, &$customer, $noUpdate = false) {
    // Get connection
    $connection = $customer->getConnection($device->ConnectionNumber);
    
    // If login is still blocked, fail
    if ($connection->BlockedUntil > time())
    {
        error_log("Login still blocked. Failing.");
        return CONN_BLOCKED;
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));  
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
    
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
 
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    $connectionState = CONN_UNKNOWN;
    $skipFirstTry = false;
    
    // If last connection is >15 minutes ago, skip try and login directly
    if(strtotime('+15 minutes', $connection->LastUpdatedSessionId) < time())
    {
        $skipFirstTry = true;
    }
    else
    {
        $connection->LastUpdatedSessionId = time();
    }
    
    // Else try to switch device
    if(!$skipFirstTry)
    {
        $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=setswitchoff&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
        curl_setopt($ch, CURLOPT_URL, $url);
        $switchOff = curl_exec($ch);
    }
    
    // If error code is 403, try to login
	$login = false;
	if (!isset($switchOff) || $switchOff === false || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on switching device off");
			$connectionState = CONN_TIMEOUT;
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on switching device off");
			$connectionState = CONN_UNREACHABLE;
		}
		elseif ($skipFirstTry || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on switching device off");
			$connectionState = CONN_UNREACHABLE;
		}
		
		if (!$login && $connectionState != CONN_TIMEOUT)
		{
			$connectionState = CONN_UNREACHABLE;
		}
	}
    else
    {
        $connectionState = CONN_OK;
    }
    
    // try to login if access failed    
	if ($login)
	{
		$loginConnection = login($customer, $connection, $ch);
        
        // If login was OK, try to switch again
        if($loginConnection == CONN_OK)
        {
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=setswitchoff&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
            curl_setopt($ch, CURLOPT_URL, $url);
            $switchOff = curl_exec($ch);
            
            if ($switchOff === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on switching device off");
                    $connectionState = CONN_TIMEOUT;
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on switching device off");
                    $connectionState = CONN_UNREACHABLE;
                }
            }
            else
            {
                $connectionState = CONN_OK;
            }
        }
        else
        {
            error_log("Could not log in.");
            $connectionState = $loginConnection;
        }
	}
    
    // If switching was OK, check if command was valid
    if($connectionState == CONN_OK)
    {
        if(trim($switchOff) != "0")
        {
            error_log("Could not switch off");
            $connectionState = CONN_FAILED;	
        }
    }
    
    // If connection was OK, try to update device list
    if(($connectionState == CONN_OK || $connectionState == CONN_FAILED) && !$noUpdate)
    {
        $connectionState = getDevices($customer, $connection, false);
    }

    // If all is ok, update cache with new value
    if($connectionState == CONN_OK)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->SwitchedOn = false;
                $customer->DeviceList[$key]->Present = true;
                break;
            }
        }
        
        // If item is group, update all members
        if($device->IsGroup)
        {
            foreach($device->GroupMembers as $member)
            {
                foreach($customer->DeviceList as $key => $deviceItem)
                {
                    // Update device
                    if($deviceItem->Identifier == $member)
                    {
                        $customer->DeviceList[$key]->SwitchedOn = false;
                        $customer->DeviceList[$key]->Present = true;
                        break;
                    }
                }
            }
        }
    }
    // Connection failed, set device to not present
    elseif($connectionState == CONN_FAILED)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->Present = false;
                break;
            }
        }
    }
    
    $connection->LastConnectionState = $connectionState;
    $customer->updateConnection($connection);
    
    // Update database
    if (!$noUpdate)
    {
        saveCustomerRDS($customer); 
    }
    
    curl_close($ch);
    
    return $connectionState;
}

// Handle Set Temperature Request
function handleSetTemperature($device, $temperature, &$customer, $noUpdate = false) {
    // Get connection
    $connection = $customer->getConnection($device->ConnectionNumber);
    
    // If login is still blocked, fail
    if ($connection->BlockedUntil > time())
    {
        error_log("Login still blocked. Failing.");
        return CONN_BLOCKED;
    }
    
    if ($temperature == TEMPERATURE_OFF || $temperature == TEMPERATURE_ON)
    {
        $temperatureSetpoint = intval($temperature);
    }
    else
    {
        $temperatureSetpoint = intval($temperature * 2.0);
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));  
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
 
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
 
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    $connectionState = CONN_UNKNOWN;
    $skipFirstTry = false;
    
    // If last connection is >15 minutes ago, skip try and login directly
    if(strtotime('+15 minutes', $connection->LastUpdatedSessionId) < time())
    {
        $skipFirstTry = true;
    }
    else
    {
        $connection->LastUpdatedSessionId = time();
    }
    
    // Else try to set device
    if(!$skipFirstTry)
    {
        $url = $requestUrl . "webservices/homeautoswitch.lua?sid=" . $connection->FritzBoxSessionId . "&switchcmd=sethkrtsoll&ain=" . $device->Identifier . "&param=" . $temperatureSetpoint;
        curl_setopt($ch, CURLOPT_URL, $url);
        $setTemperature = curl_exec($ch);
    }
    
    // If error code is 403, try to login
	$login = false;
	if (!isset($setTemperature) || $setTemperature === false || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on switching device temperature");
			$connectionState = CONN_TIMEOUT;
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on switching device temperature");
			$connectionState = CONN_UNREACHABLE;
		}
		elseif ($skipFirstTry || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on switching device temperature");
			$connectionState = CONN_UNREACHABLE;
		}
		
		if (!$login && $connectionState != CONN_TIMEOUT)
		{
			$connectionState = CONN_UNREACHABLE;
		}
	}
    else
    {
        $connectionState = CONN_OK;
    }
    
    // try to login if access failed    
	if ($login)
	{
		$loginConnection = login($customer, $connection, $ch);
        
        // If login was OK, try to switch again
        if($loginConnection == CONN_OK)
        {
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=sethkrtsoll&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier . "&param=" . $temperatureSetpoint;
            curl_setopt($ch, CURLOPT_URL, $url);
            $setTemperature = curl_exec($ch);
            
            if ($setTemperature === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on switching device temperature");
                    $connectionState = CONN_TIMEOUT;
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on switching device temperature");
                    $connectionState = CONN_UNREACHABLE;
                }
            }
            else
            {
                $connectionState = CONN_OK;
            }
        }
        else
        {
            error_log("Could not log in.");
            $connectionState = $loginConnection;
        }
	}
    
    // If switching was OK, check if command was valid
    if($connectionState == CONN_OK)
    {
        if(intval(trim($setTemperature)) != $temperatureSetpoint)
        {
            error_log("Could not set temperature");
            $connectionState = CONN_FAILED;	
        }
    }
    
    // If connection was OK, try to update device list
    if(($connectionState == CONN_OK || $connectionState == CONN_FAILED) && !$noUpdate)
    {
        $connectionState = getDevices($customer, $connection, false);
    }

    // If all is ok, update cache with new value
    if($connectionState == CONN_OK)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->TemperatureSetpoint = $temperature;
                $customer->DeviceList[$key]->Present = true;
                break;
            }
        }
        
        // If item is group, update all members
        if($device->IsGroup)
        {
            foreach($device->GroupMembers as $member)
            {
                foreach($customer->DeviceList as $key => $deviceItem)
                {
                    // Update device
                    if($deviceItem->Identifier == $member)
                    {
                        $customer->DeviceList[$key]->TemperatureSetpoint = temperature;
                        $customer->DeviceList[$key]->Present = true;
                        break;
                    }
                }
            }
        }
    }
    // Connection failed, set device to not present
    elseif($connectionState == CONN_FAILED)
    {
        // search device in customer device list
        foreach($customer->DeviceList as $key => $deviceItem)
		{
            // Update device
            if($deviceItem->Identifier == $device->Identifier)
            {
                $customer->DeviceList[$key]->Present = false;
                break;
            }
        }
    }
    
    $connection->LastConnectionState = $connectionState;
    $customer->updateConnection($connection);
    
    // Update database
    if (!$noUpdate)
    {
        saveCustomerRDS($customer); 
    }
    
    curl_close($ch);
    
    return $connectionState;
    
}

// Handle Apply Template Request
function handleApplyTemplate($device, &$customer, $noUpdate = false) {
    // Get connection
    $connection = $customer->getConnection($device->ConnectionNumber);
    
    // If login is still blocked, fail
    if ($connection->BlockedUntil > time())
    {
        error_log("Login still blocked. Failing.");
        return CONN_BLOCKED;
    }
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 40); 
    curl_setopt($ch, CURLOPT_TIMEOUT, 40);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    $hostname = strtolower(parse_url($connection->FritzBoxUrl, PHP_URL_HOST));  
    $port = parse_url($connection->FritzBoxUrl, PHP_URL_PORT);
 
    $requestUrl = "";
    if($connection->UseIPv6)
    {
        $ip6address = @dns_get_record($hostname, DNS_AAAA);
        
        if(!isset($ip6address[0]['ipv6']))
        {
            error_log("Could not get ipv6");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
 
        $requestUrl = "https://[" . $ip6address[0]['ipv6'] . "]";
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip6address[0]['ipv6']));
    }
    else
    {
        $ip4address = @dns_get_record($hostname, DNS_A);
        
        if(!isset($ip4address[0]['ip']))
        {
            error_log("Could not get ip");
            $connectionState = CONN_UNREACHABLE;
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
            saveCustomerRDS($customer); 
            curl_close($ch);
            return $connectionState;
        }
        
        $requestUrl = "https://" . $ip4address[0]['ip'];
        if (strlen(trim($port)) > 0)
        {
            $requestUrl .= ":";
            $requestUrl .= $port;
        }
        $requestUrl .= "/";
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Host: " . $ip4address[0]['ip']));
    }
    
    $connectionState = CONN_UNKNOWN;
    $skipFirstTry = false;
    
    // If last connection is >15 minutes ago, skip try and login directly
    if(strtotime('+15 minutes', $connection->LastUpdatedSessionId) < time())
    {
        $skipFirstTry = true;
    }
    else
    {
        $connection->LastUpdatedSessionId = time();
    }
    
    // Else try to switch device
    if(!$skipFirstTry)
    {
        $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=applytemplate&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
        curl_setopt($ch, CURLOPT_URL, $url);
        $applyTemplate = curl_exec($ch);
    }
    
    // If error code is 403, try to login
	$login = false;
	if (!isset($applyTemplate) || $applyTemplate === false || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on applying template");
			$connectionState = CONN_TIMEOUT;
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on applying template");
			$connectionState = CONN_UNREACHABLE;
		}
		elseif ($skipFirstTry || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on applying template");
			$connectionState = CONN_UNREACHABLE;
		}
		
		if (!$login && $connectionState != CONN_TIMEOUT)
		{
			$connectionState = CONN_UNREACHABLE;
		}
	}
    else
    {
        $connectionState = CONN_OK;
    }
    
    // try to login if access failed    
	if ($login)
	{
		$loginConnection = login($customer, $connection, $ch);
        
        // If login was OK, try to switch again
        if($loginConnection == CONN_OK)
        {
            $url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=applytemplate&sid=" . $connection->FritzBoxSessionId . "&ain=" . $device->Identifier;
            curl_setopt($ch, CURLOPT_URL, $url);
            $applyTemplate = curl_exec($ch);
            
            if ($applyTemplate === false)
            {
                if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
                {
                    error_log("Connection timed out on applying template");
                    $connectionState = CONN_TIMEOUT;
                } 
                else
                {
                    error_log("Connection failed with " . curl_errno($ch) . " on applying template");
                    $connectionState = CONN_UNREACHABLE;
                }
            }
            else
            {
                $connectionState = CONN_OK;
            }
        }
        else
        {
            error_log("Could not log in.");
            $connectionState = $loginConnection;
        }
	}
    
    // If switching was OK, check if command was valid
    if($connectionState == CONN_OK)
    {
        if(trim($applyTemplate) != trim($device->Id))
        {
            error_log("Could not apply template");
            $connectionState = CONN_FAILED;	
        }
    }
    
    // If connection was OK, try to update device list
    if(($connectionState == CONN_OK || $connectionState == CONN_FAILED) && !$noUpdate)
    {
        $connectionState = getDevices($customer, $connection, false);
    }
    
    $connection->LastConnectionState = $connectionState;
    $customer->updateConnection($connection);
    
    // Update database
    if (!$noUpdate)
    {
        saveCustomerRDS($customer); 
    }
    
    curl_close($ch);
    
    return $connectionState;
}


// Handle multiple commands
function handleMultipleCommands($commandList, &$customer) {
    
    // Create response list
    $returnConnectionList = array();
    
    // Create list of already updated connections
    $updatedConnections = array();
    
    foreach($commandList as $device => $command)
    {
        // Search device
        $deviceItem = $customer->getDevice($device);
        
        // Check if device exists
        if($deviceItem == null)
        {
            $returnConnectionList[$device] = CONN_UNKNOWN;
            continue;
        }
        
        // Get connection
        $connection = $customer->getConnection($deviceItem->ConnectionNumber);
        
        // Check connection and update device list. Only update if not updated yet
        if(!array_key_exists($connection->Number, $updatedConnections))
        {   
            // Update device list
            $updateConnection = getDevices($customer, $connection, false);
            $updatedConnections[$connection->Number] = $updateConnection;
        }
        
        // If connection is known to be not ok, update return list and continue
        if ($updatedConnections[$connection->Number] != CONN_OK)
        {
            $returnConnectionList[$device] = $updatedConnections[$connection->Number];
            continue;
        }

        $connectionState = CONN_UNKNOWN;
        
        // If command is on/off and device is thermostat, update command to set temperature
        if(is_bool($command) && $deviceItem->isDeviceClass(CLASS_THERMOSTAT))
        {
            $onTemperature = $deviceItem->TemperatureComfort;
            $offTemperature = TEMPERATURE_OFF;
            
            // check for user set temperature. If not set, use comfort temperature
            if (($deviceItem->TemperatureOn >= TEMPERATURE_MINVALUE && $deviceItem->TemperatureOn <= TEMPERATURE_MAXVALUE)
                || ($deviceItem->TemperatureOn >= TEMPERATURE_NOCHANGE && $deviceItem->TemperatureOn <= TEMPERATURE_COMFORT))
            {
                if ($deviceItem->TemperatureOn == TEMPERATURE_NOCHANGE)
                {
                    $onTemperature = $deviceItem->TemperatureSetpoint;
                }
                elseif ($deviceItem->TemperatureOn == TEMPERATURE_ECO)
                {
                    $onTemperature = $deviceItem->TemperatureSetback;
                }
                elseif ($deviceItem->TemperatureOn == TEMPERATURE_COMFORT)
                {
                    $onTemperature = $deviceItem->TemperatureComfort;
                }
                else
                {
                    $onTemperature = $deviceItem->TemperatureOn;
                }
            }
            
            // check for user set temperature. If not set, set thermostat off
            if (($deviceItem->TemperatureOff >= TEMPERATURE_MINVALUE && $deviceItem->TemperatureOff <= TEMPERATURE_MAXVALUE)
                || ($deviceItem->TemperatureOff >= TEMPERATURE_NOCHANGE && $deviceItem->TemperatureOff <= TEMPERATURE_COMFORT))
            {
                if ($deviceItem->TemperatureOff == TEMPERATURE_NOCHANGE)
                {
                    $offTemperature = $deviceItem->TemperatureSetpoint;
                }
                elseif ($deviceItem->TemperatureOff == TEMPERATURE_ECO)
                {
                    $offTemperature = $deviceItem->TemperatureSetback;
                }
                elseif ($deviceItem->TemperatureOff == TEMPERATURE_COMFORT)
                {
                    $offTemperature = $deviceItem->TemperatureComfort;
                }
                else
                {
                    $offTemperature = $deviceItem->TemperatureOff;
                }
            }
            
            $temperature = 0;
            if($command === true)
            {
                $temperature = $onTemperature;
            }
            else
            {
                $temperature = $offTemperature;
            }
            
            // If no change is requested, just update response
            if ($command === true && $deviceItem->TemperatureOn == TEMPERATURE_NOCHANGE)
            {
                $returnConnectionList[$device] = $customer->getConnection($deviceItem->ConnectionNumber)->LastConnectionState;
                continue;
            }
            elseif ($command === false && $deviceItem->TemperatureOff == TEMPERATURE_NOCHANGE)
            {
                $returnConnectionList[$device] = $customer->getConnection($deviceItem->ConnectionNumber)->LastConnectionState;
                continue;
            }
            else
            {
                // else update temperature
                $command = $temperature;
            }
        }
        
        // If command is on and device is template, apply template
        if(is_bool($command) && $deviceItem->isDeviceClass(CLASS_TEMPLATE))
        {
            // Command is apply template
            $connectionState = handleApplyTemplate($deviceItem, $customer, true);
        }
        // Check if command is on/off
        elseif($command === true)
        {
            // Command is turn on
            $connectionState = handleTurnOn($deviceItem, $customer, true);
        }
        elseif($command === false)
        {
            // Command is turn off
            $connectionState = handleTurnOff($deviceItem, $customer, true);
        }
        elseif(is_numeric($command))
        {
            // Command is set temperature command
            $connectionState = handleSetTemperature($deviceItem, floatval($command), $customer, true);
        }
    
        $returnConnectionList[$device] = $connectionState;  
    }    
    
    // Update connections
    foreach($returnConnectionList as $device => $connectionState)
    {
        $deviceItem = $customer->getDevice($device);
        if($deviceItem != null)
        {
            $connection = $customer->getConnection($deviceItem->ConnectionNumber);
            $connection->LastConnectionState = $connectionState;
            $customer->updateConnection($connection);
        }
    }

    // Update customer
    saveCustomerRDS($customer); 
    
    return $returnConnectionList;
}

// Get switches from legacy device
function getLegacySwitches() {
	global $ch, $sid, $requestUrl;
	
	$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchlist&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$switchList = curl_exec($ch);
	
	$login = false;
	if (!$switchList || curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
	{
		if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
		{
			error_log("Connection timed out on getting switch list");
			echo "NOK - Connection timed out on getting switch list";
		} 
		elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
		{
			error_log("Connection was refused on getting switch list");
			echo "NOK - Connection was refused on getting switch list";
		}
		elseif (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 403)
		{
			$login = true;
		}
		else
		{
			error_log("Connection failed with " . curl_errno($ch) . " on getting switch list");
			echo "NOK - Connection failed with " . curl_errno($ch) . " on getting switch list";
		}
		
		if (!$login)
		{
			curl_close($ch);
			return;
		}
	}
	
	if ($login)
	{
		login($customer, $ch, true);
		
		$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchlist&sid=" . $sid;
		curl_setopt($ch, CURLOPT_URL, $url);
		$switchList = curl_exec($ch);

		if (!$switchList)
		{
			if (curl_errno($ch) == CURLE_OPERATION_TIMEDOUT) 
			{
				error_log("Connection timed out on getting switch list login retry");
				echo "NOK - Connection timed out on getting switch list login retry";
			} 
			elseif (curl_errno($ch) == CURLE_COULDNT_CONNECT) 
			{
				error_log("Connection was refused on getting switch list login retry");
				echo "NOK - Connection was refused on getting switch list login retry";
			}
			else
			{
				error_log("Connection failed with " . curl_errno($ch) . " on getting switch list login retry");
				echo "NOK - Connection failed with " . curl_errno($ch) . " on getting switch list login retry";
			}
			
			curl_close($ch);
			return;
		}	
	}
	
	$switchArray = array_filter(explode(',', $switchList), 'strlen');

	$switchListArray = array();
	
	foreach($switchArray as $switch)
	{
		$switchListArray[] = parseSwitch(trim($switch));
	}
	
	return $switchListArray;
}

// Parse switches from legacy device
function parseSwitch($switch) {

	global $ch, $sid, $requestUrl;

	$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchname&ain=" . $switch . "&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$switchName = curl_exec($ch);

	$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchpresent&ain=" . $switch . "&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$switchPresent = curl_exec($ch);

	$url = $requestUrl . "webservices/homeautoswitch.lua?switchcmd=getswitchstate&ain=" . $switch . "&sid=" . $sid;
	curl_setopt($ch, CURLOPT_URL, $url);
	$switchState = curl_exec($ch);

	$device = new Device();
	$device->Identifier = is_string($switch) ? $switch : "";
	$device->Id = "";
	$device->Firmware = "";
	$device->Manufacturer = "Fritz!Box";
	$device->ProductName = "";
	$device->Name = is_string($switchName) ? $switchName : "";
	$device->Present = (is_string($switchPresent) && strlen(trim($switchPresent)) == 1) ? boolval(trim($switchPresent)) : false;

	$device->DeviceClasses[] = CLASS_POWERSWITCH;

	$device->SwitchedOn = (is_string($switchState) && strlen(trim($switchState)) == 1) ? boolval(trim($switchState)) : false;
	$device->OperationMode = MODE_NONE;

	$device->Locked = 0;

	return $device;
}

// Parse device list
function parseDevice($deviceArray) {
	$device = new Device();
	$device->Identifier = isset($deviceArray->attributes()->identifier) ? preg_replace('/\s+/', '', $deviceArray->attributes()->identifier->__toString()) : "";
	$device->Id = isset($deviceArray->attributes()->id) ? $deviceArray->attributes()->id->__toString() : "";
	$device->Firmware = isset($deviceArray->attributes()->fwversion) ? $deviceArray->attributes()->fwversion->__toString() : "";
	$device->Manufacturer = "Fritz!Box";
	$device->ProductName = isset($deviceArray->attributes()->productname) ? $deviceArray->attributes()->productname->__toString() : "";
	$device->Name = isset($deviceArray->name) ? $deviceArray->name->__toString() : "";
	$device->Present = isset($deviceArray->present) ? boolval(trim($deviceArray->present->__toString())) : false;

    // Remove non-ASCII characters
    $device->Identifier = preg_replace('/[^\x20-\xff]/', '', $device->Identifier);
    $device->Id = preg_replace('/[^\x20-\xff]/', '', $device->Id);
    $device->Firmware = preg_replace('/[^\x20-\xff]/', '', $device->Firmware);
    $device->ProductName = preg_replace('/[^\x20-\xff]/', '', $device->ProductName);
    $device->Name = preg_replace('/[^\x20-\xff]/', '', $device->Name);
    
	$functionbitmask = isset($deviceArray->attributes()->functionbitmask) ? intval(trim($deviceArray->attributes()->functionbitmask)) : 0;

	if(isDeviceClass($functionbitmask, CLASS_ALARMSENSOR))
	{
		$device->DeviceClasses[] = CLASS_ALARMSENSOR;
		
		$deviceAlert = isset($deviceArray->alert->state) ? boolval(trim($deviceArray->alert->state->__toString())) : false;
	}

	if(isDeviceClass($functionbitmask, CLASS_THERMOSTAT))
	{
		$device->DeviceClasses[] = CLASS_THERMOSTAT;
		
		$temperature = isset($deviceArray->hkr->tist) ? intval(trim($deviceArray->hkr->tist)) : 0;
		if($temperature == TEMPERATURE_ON || $temperature == TEMPERATURE_OFF)
		{
			(float)$device->TemperatureActualValue = $temperature;
		}
		else
		{
			(float)$device->TemperatureActualValue = (float)$temperature / 2.0;
		}

		$temperature = isset($deviceArray->hkr->tsoll) ? intval(trim($deviceArray->hkr->tsoll)) : 0;
		if($temperature == TEMPERATURE_ON || $temperature == TEMPERATURE_OFF)
		{
			(float)$device->TemperatureSetpoint = $temperature;
		}
		else
		{
			(float)$device->TemperatureSetpoint = (float)$temperature / 2.0;
		}

		$temperature = isset($deviceArray->hkr->komfort) ? intval(trim($deviceArray->hkr->komfort)) : 0;
		if($temperature == TEMPERATURE_ON || $temperature == TEMPERATURE_OFF)
		{
			(float)$device->TemperatureComfort = $temperature;
		}
		else
		{
			(float)$device->TemperatureComfort = (float)$temperature / 2.0;
		}

		$temperature = isset($deviceArray->hkr->absenk) ? intval(trim($deviceArray->hkr->absenk)) : 0;
		if($temperature == TEMPERATURE_ON || $temperature == TEMPERATURE_OFF)
		{
			(float)$device->TemperatureSetback = $temperature;
		}
		else
		{
			(float)$device->TemperatureSetback = (float)$temperature / 2.0;
		}

		$device->Locked = isset($deviceArray->hkr->lock) ? boolval(trim($deviceArray->hkr->lock->__toString())) : false;
		$device->BatteryLow = isset($deviceArray->hkr->batterylow) ?  boolval(trim($deviceArray->hkr->batterylow->__toString())) : false;
	}

	if(isDeviceClass($functionbitmask, CLASS_ENERGYMETER))
	{
		$device->DeviceClasses[] = CLASS_ENERGYMETER;

		$power = isset($deviceArray->powermeter->power) ? floatval(trim($deviceArray->powermeter->power)) : 0.0;
		(float)$device->Power = $power / 1000.0;

		$energy = isset($deviceArray->powermeter->energy) ? floatval(trim($deviceArray->powermeter->energy)) : 0.0;
		(float)$device->Energy = $energy / 1.0;
	}

	if(isDeviceClass($functionbitmask, CLASS_TEMPERATURESENSOR))
	{
		$device->DeviceClasses[] = CLASS_TEMPERATURESENSOR;

		$temperature = isset($deviceArray->temperature->celsius) ? floatval(trim($deviceArray->temperature->celsius)) : 0.0;
		(float)$device->Temperature = $temperature / 10.0;
	}

	if(isDeviceClass($functionbitmask, CLASS_POWERSWITCH))
	{
		$device->DeviceClasses[] = CLASS_POWERSWITCH;

		$device->SwitchedOn = isset($deviceArray->switch->state) ? boolval(trim($deviceArray->switch->state->__toString())) : false;
		$mode =  isset($deviceArray->switch->mode) ? $deviceArray->switch->mode->__toString() : "";
		if ($mode == "auto")
		{
			$device->OperationMode = MODE_AUTO;
		}
		elseif ($mode == "manuell")
		{
			$device->OperationMode = MODE_MANUAL;
		}
		else
		{
			$device->OperationMode = MODE_NONE;
		}

		$device->Locked = isset($deviceArray->switch->lock) ? boolval(trim($deviceArray->switch->lock->__toString())) : false;
	}
	
	if(isDeviceClass($functionbitmask, CLASS_DECTREPEATER))
	{
		$device->DeviceClasses[] = CLASS_DECTREPEATER;
	}

	if (isset($deviceArray->groupinfo))
	{
		$device->IsGroup = true;
		$device->ProductName = "Gruppe";
		$members = isset($deviceArray->groupinfo->members) ? $deviceArray->groupinfo->members->__toString() : "";
		$device->GroupMembers = explode(",", $members);
	}

	return $device;
}

// Parse template list
function parseTemplate($templateArray) {
	$template = new Device();
	$template->Identifier = isset($templateArray->attributes()->identifier) ? preg_replace('/\s+/', '', $templateArray->attributes()->identifier->__toString()) : "";
	$template->Id = isset($templateArray->attributes()->id) ? $templateArray->attributes()->id->__toString() : "";
	$template->Manufacturer = "Fritz!Box";
    $template->ProductName = "Vorlage";
	$template->Name = isset($templateArray->name) ? $templateArray->name->__toString() : "";
    $template->Present = true;

    // Remove non-ASCII characters
    $template->Identifier = preg_replace('/[^\x20-\xff]/', '', $template->Identifier);
    $template->Id = preg_replace('/[^\x20-\xff]/', '', $template->Id);
    $template->Name = preg_replace('/[^\x20-\xff]/', '', $template->Name);
    
	$template->DeviceClasses[] = CLASS_TEMPLATE;

	return $template;
}

function isDeviceClass($functionbitmask, $class) {
	$check = pow(2, $class);
	return $functionbitmask & $check;
}
?>