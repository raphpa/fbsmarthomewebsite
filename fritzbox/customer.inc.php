<?php

global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');

class Customer
{
	public $CustomerId = "";
    public $IsGoogleAssistant = false;
    public $UserRegion = "";
	public $DeviceList = array();
    public $ConnectionList = array();
	public $TTL = 0;
    public $AccessToken = "";
    public $RefreshToken = "";
    public $TokenValidity = 0;
    public $ApiKey = "";

	function __construct() {
		$this->TTL = strtotime('+14 days');
	}

	function __toString() {
		return $this->customerId;
	}
	
	function hasVirtual($identifier)
	{
        $device = $this->getDevice($identifier);
        
		if($device == null || $device->AdditionalSwitch == false)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
    
    function hasDevice($identifier)
    {
        foreach($this->DeviceList as $device)
		{
            if ($identifier == $device->Identifier) {
                return true;
            }
        }
        
        return false;
    }
    
    function getDevice($identifier)
    {
        foreach($this->DeviceList as $device)
		{
            if ($identifier == $device->Identifier) {
                return $device;
            }
        }
        
        return null;
    }
    
    function removeDevices($connectionNumber)
    {
        foreach($this->DeviceList as $key => $device)
		{
            if ($connectionNumber == $device->ConnectionNumber) {
                unset($this->DeviceList[$key]);
            }
        }
        $this->DeviceList = array_values($this->DeviceList);  
    }
    
    function removeDevice($device)
    {
        foreach($this->DeviceList as $key => $deviceItem)
		{
            if ($device->Identifier == $deviceItem->Identifier) {
                unset($this->DeviceList[$key]);
            }
        }
        $this->DeviceList = array_values($this->DeviceList);  
    }
	
    function getConnection($connectionNumber)
    {
        foreach($this->ConnectionList as $connection)
		{
            if ($connectionNumber == $connection->Number) {
                return $connection;
            }
        }
        
        // If connection was not found, return default connection if array is not empty. Else
        // create new connection
        foreach($this->ConnectionList as $connection)
		{
            if (CONNECTION_DEFAULT == $connection->Number) {
                return $connection;
            }
        }
        
        return new Connection();
    }
    
    function isConnection($connectionNumber)
    {
        foreach($this->ConnectionList as $connection)
		{
            if ($connectionNumber == $connection->Number) {
                return true;
            }
        }
        return false;
    }
    
    function removeConnection($connectionNumber)
    {
        foreach($this->ConnectionList as $key => $connection)
		{
            if ($connectionNumber == $connection->Number) {
                unset($this->ConnectionList[$key]);
            }
        }
        $this->ConnectionList = array_values($this->ConnectionList);
    }
    
    function addOrUpdateConnection($connection)
    {
        foreach($this->ConnectionList as $key => $connectionListItem)
		{
            if ($connection->Number == $connectionListItem->Number) {
                $this->ConnectionList[$key] = $connection;
                return;
            }
        }
        $this->ConnectionList[] = $connection;
    }
    
    function updateConnection($connection)
    {
        foreach($this->ConnectionList as $key => $connectionListItem)
		{
            if ($connection->Number == $connectionListItem->Number) {
                $this->ConnectionList[$key] = $connection;
                return;
            }
        }
    }
    
	function getDeviceListAsArray()
	{		
		return getListAsArray($this->DeviceList);
	}
    
	function getConnectionListAsArray()
	{
		return getListAsArray($this->ConnectionList);
	}
   
    function getJson()
    {
        $json = clone $this;
        
        $json->LastUpdatedDevices = gmdate("Y-m-d\TH:i:s.000\Z", $json->LastUpdatedDevices);
        $json->LastUpdatedSessionId = gmdate("Y-m-d\TH:i:s.000\Z", $json->LastUpdatedSessionId);
        $json->BlockedUntil = gmdate("Y-m-d\TH:i:s.000\Z", $json->BlockedUntil);
        $json->TokenValidity = gmdate("Y-m-d\TH:i:s.000\Z", $json->TokenValidity);

        return json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }
}

function createCustomer($customerArray)
{
    $customer = new Customer();
    $customer->CustomerId = trim($customerArray['CustomerId']);
    $customer->IsGoogleAssistant = array_key_exists('IsGoogleAssistant', $customerArray) ? boolval($customerArray['IsGoogleAssistant']) : false;
    $customer->UserRegion = array_key_exists('UserRegion', $customerArray) ? trim($customerArray['UserRegion']) : "";
    $deviceList = (array_key_exists('DeviceList', $customerArray) && is_array($customerArray['DeviceList'])) ? $customerArray['DeviceList'] : array();
    $connectionList = (array_key_exists('ConnectionList', $customerArray) && is_array($customerArray['ConnectionList'])) ? $customerArray['ConnectionList'] : array();
    $customer->AccessToken = array_key_exists('AccessToken', $customerArray) ? trim($customerArray['AccessToken']) : "";
    $customer->RefreshToken = array_key_exists('RefreshToken', $customerArray) ? trim($customerArray['RefreshToken']) : "";
    $customer->TokenValidity = array_key_exists('TokenValidity', $customerArray) ? strtotime($customerArray['TokenValidity']) : 0;
    $customer->ApiKey = array_key_exists('ApiKey', $customerArray) ? trim($customerArray['ApiKey']) : "";
    
    $customer->DeviceList = array();
    foreach($deviceList as $deviceItem)
    {
        $device = createDevice($deviceItem);
        $customer->DeviceList[] = $device;
    }
    
    $customer->ConnectionList = array();
    foreach($connectionList as $connectionItem)
    {
        $connection = createConnection($connectionItem);
        $customer->ConnectionList[] = $connection;
    }
    
    if(empty($customer->ConnectionList))
    {
        $connection = new Connection();
        $connection->Number = CONNECTION_DEFAULT;
        $customer->ConnectionList[] = $connection;
    }
    
    return $customer;
}

?>