<?php
const NOSID = "0000000000000000";
const CLASS_NONE = 0;
const CLASS_ALARMSENSOR = 4;
const CLASS_THERMOSTAT = 6;
const CLASS_ENERGYMETER = 7;
const CLASS_TEMPERATURESENSOR = 8;
const CLASS_POWERSWITCH = 9;
const CLASS_DECTREPEATER = 10;
const CLASS_TEMPLATE = 99;

const MODE_NONE = 0;
const MODE_AUTO = 1;
const MODE_MANUAL = 2;

const THERMOSTATMODE_AUTO = 0;
const THERMOSTATMODE_COOL = 1;
const THERMOSTATMODE_HEAT = 2;
const THERMOSTATMODE_ECO = 3;
const THERMOSTATMODE_OFF = 4;
const THERMOSTATMODE_CUSTOM = 5;

const TEMPERATURE_MINVALUE = 8;
const TEMPERATURE_MAXVALUE = 28;
const TEMPERATURE_NOCHANGE = 252;
const TEMPERATURE_OFF = 253;
const TEMPERATURE_ON = 254;
const TEMPERATURE_ECO = 255;
const TEMPERATURE_COMFORT = 256;

const CONN_OK = 0;
const CONN_TIMEOUT = 1;
const CONN_UNREACHABLE = 2;
const CONN_NOT_AUTHORIZED = 3;
const CONN_BLOCKED = 4;
const CONN_FAILED = 5;
const CONN_EXCEPTION = 6;
const CONN_UNKNOWN = 7;
const CONN_NOTCONFIGURED = 8;

const REDIS_TOKENS = 1;
const REDIS_LOCKS = 2;
const REDIS_CUSTOMERS = 3;
const REDIS_APILIMIT = 4;

const CONNECTION_DEFAULT = 1;
const CONNECTION_MAX = 5;

function guidv4($data)
{
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function getListAsArray(&$list)
{
    $array = array();
    foreach($list as $item)
    {
        $itemArray = array();
        foreach($item as $key => $value) 
        { 
            if (is_bool($value))
            {
                $itemArray[$key] = $value ? 1 : 0;
            }
            elseif(is_string($value) && strlen($value) <= 0)
            {
                $itemArray[$key] = " ";
            }
            elseif(is_array($value))
            {
                array_walk_recursive($value, function(&$arrayValue, $arrayKey)
                    {
                        if(is_string($arrayValue) && strlen($arrayValue) <= 0)
                        {
                            $arrayValue = " ";
                        }
                    }
                );
                $itemArray[$key]  = $value;
            }
            else
            {
                $itemArray[$key]  = $value;
            }
        }
        $array[] = $itemArray;
    }
    return $array;
}

function prettyPrint($data) {
	highlight_string("<?php\n\$data =\n" . var_export($data, true) . ";\n?>");
}

function arrayCopy(array &$array ) {
        $result = array();
        foreach( $array as $key => $val ) {
            if( is_array( $val ) ) {
                $result[$key] = arrayCopy( $val );
            } elseif ( is_object( $val ) ) {
                $result[$key] = clone $val;
            } else {
                $result[$key] = $val;
            }
        }
        return $result;
}

date_default_timezone_set('UTC');
?>