<?php

global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');

class Connection
{
    public $Number = CONNECTION_DEFAULT;
	public $FritzBoxUrl = "";
	public $FritzBoxUsername = "";
	public $FritzBoxPassword = "";
	public $FritzBoxSessionId = NOSID;
    public $LastUpdatedDevices = 0;
    public $LastUpdatedSessionId = 0;
    public $LastConnectionState = CONN_UNREACHABLE;
    public $BlockedUntil = 0;
    public $Legacy = false;
    public $UseIPv6 = false;
}

function createConnection($connectionArray)
{
    $connection = new Connection();
    $connection->Number = array_key_exists('Number', $connectionArray) ? intval($connectionArray['Number']) : CONNECTION_DEFAULT;
    $connection->FritzBoxUrl = array_key_exists('FritzBoxUrl', $connectionArray) ? trim($connectionArray['FritzBoxUrl']) : "";
    $connection->FritzBoxUsername = array_key_exists('FritzBoxUsername', $connectionArray) ? trim($connectionArray['FritzBoxUsername']) : "";
    $connection->FritzBoxPassword = array_key_exists('FritzBoxPassword', $connectionArray) ? trim($connectionArray['FritzBoxPassword']) : "";
    $connection->FritzBoxSessionId = array_key_exists('FritzBoxSessionId', $connectionArray) ? trim($connectionArray['FritzBoxSessionId']) : "";
    $connection->LastUpdatedDevices = array_key_exists('LastUpdatedDevices', $connectionArray) ? strtotime($connectionArray['LastUpdatedDevices']) : 0;
    $connection->LastUpdatedSessionId = array_key_exists('LastUpdatedSessionId', $connectionArray) ? strtotime($connectionArray['LastUpdatedSessionId']) : 0;
    $connection->LastConnectionState = array_key_exists('LastConnectionState', $connectionArray) ? intval($connectionArray['LastConnectionState']) : CONN_UNREACHABLE;
    $connection->BlockedUntil = array_key_exists('BlockedUntil', $connectionArray) ? strtotime($connectionArray['BlockedUntil']) : 0;
    $connection->Legacy = array_key_exists('Legacy', $connectionArray) ? boolval($connectionArray['Legacy']) : false;
    $connection->UseIPv6 = array_key_exists('UseIPv6', $connectionArray) ? boolval($connectionArray['UseIPv6']) : false;
    
    return $connection;
}
?>