<?php
global $basedir;
require_once($basedir . '/vendor/autoload.php');

require_once($basedir . '/fritzbox/auxiliary.inc.php');
require_once($basedir . '/fritzbox/customer.inc.php');
require_once($basedir . '/fritzbox/device.inc.php');
require_once($basedir . '/fritzbox/connection.inc.php');

use Aws\DynamoDb\DynamoDbClient;
use Aws\DynamoDb\Marshaler;
use Aws\DynamoDb\Exception\DynamoDbException;

//Set aws config
$awsConfig = [
  'region'  => 'eu-west-1',
  'version' => 'latest',
  'http'    => [
        'connect_timeout' => 15,
        'timeout' => 15
    ]
];

function getCustomer($customerId)
{
	global $awsConfig;

	$marshaler = new Marshaler();

	$dbClient = new DynamoDbClient($awsConfig);
	try{
		$response = $dbClient->getItem(array(
			'TableName' => 'FBSmartHome',
			'Key' => array(
				'CustomerId'  => array('S' => trim($customerId))
			)
		));

		$customer = new Customer();

		$customer->CustomerId = trim($customerId);
        $customer->IsGoogleAssistant = isset($response['Item']['IsGoogleAssistant']['N']) ? boolval($response['Item']['IsGoogleAssistant']['N']) : false;
        $customer->UserRegion = isset($response['Item']['UserRegion']['S']) ? trim($response['Item']['UserRegion']['S']) : "";
		$deviceList = isset($response['Item']['DeviceList']['L'][0]) ? $marshaler->unmarshalItem($response['Item']['DeviceList']['L']) : array();
        $connectionList = isset($response['Item']['ConnectionList']['L'][0]) ? $marshaler->unmarshalItem($response['Item']['ConnectionList']['L']) : array();
        $customer->TTL = isset($response['Item']['TTL']['N']) ? intval($response['Item']['TTL']['N']) : 0;
        $customer->AccessToken = isset($response['Item']['AccessToken']['S']) ? trim($response['Item']['AccessToken']['S']) : "";
        $customer->RefreshToken = isset($response['Item']['RefreshToken']['S']) ? trim($response['Item']['RefreshToken']['S']) : "";
        $customer->TokenValidity = isset($response['Item']['TokenValidity']['S']) ? strtotime($response['Item']['TokenValidity']['S']) : 0;
        $customer->ApiKey = isset($response['Item']['ApiKey']['S']) ? trim($response['Item']['ApiKey']['S']) : "";
        
		$customer->DeviceList = array();
		foreach($deviceList as $deviceItem)
		{
            $device = createDevice($deviceItem);
            $customer->DeviceList[] = $device;
		}

        $customer->ConnectionList = array();
        foreach($connectionList as $connectionItem)
        {
            $connection = createConnection($connectionItem);
            $customer->ConnectionList[] = $connection;
        }

        if(empty($customer->ConnectionList))
        {
            $connection = new Connection();
            $connection->Number = CONNECTION_DEFAULT;
            $connection->FritzBoxUrl = isset($response['Item']['FritzBoxUrl']['S']) ? trim($response['Item']['FritzBoxUrl']['S']) : "";
            $connection->FritzBoxUsername = isset($response['Item']['FritzBoxUsername']['S']) ? trim($response['Item']['FritzBoxUsername']['S']) : "";
            $connection->FritzBoxPassword = isset($response['Item']['FritzBoxPassword']['S']) ? trim($response['Item']['FritzBoxPassword']['S']) : "";
            $connection->FritzBoxSessionId = isset($response['Item']['FritzBoxSessionId']['S']) ? trim($response['Item']['FritzBoxSessionId']['S']) : NOSID;
            $connection->LastUpdatedDevices = isset($response['Item']['LastUpdatedDevices']['S']) ? strtotime($response['Item']['LastUpdatedDevices']['S']) : 0;
            $connection->LastUpdatedSessionId = isset($response['Item']['LastUpdatedSessionId']['S']) ? strtotime($response['Item']['LastUpdatedSessionId']['S']) : 0;
            $connection->LastConnectionState = isset($response['Item']['LastConnectionState']['N']) ? intval($response['Item']['LastConnectionState']['N']) : CONN_UNREACHABLE;
            $connection->BlockedUntil = isset($response['Item']['BlockedUntil']['S']) ? strtotime($response['Item']['BlockedUntil']['S']) : 0;
            $connection->Legacy = isset($response['Item']['Legacy']['N']) ? boolval($response['Item']['Legacy']['N']) : false;
            $connection->UseIPv6 = isset($response['Item']['UseIPv6']['N']) ? boolval($response['Item']['UseIPv6']['N']) : false;
            $customer->ConnectionList[] = $connection;
        }

	} catch (DynamoDbException $e) {
		error_log($e->getMessage());
		return null;
	}
    
    unset($marshaler, $response, $deviceList, $connectionList);

	return $customer;
}

function saveCustomer($customer)
{
    if ($customer == null)
    {
        return null;
    }

    $connectionListArray = $customer->getConnectionListAsArray();
    foreach($connectionListArray as $key => $connectionArray)
    {
        $connectionArray['BlockedUntil'] = gmdate("Y-m-d\TH:i:s.000\Z", $connectionArray['BlockedUntil']);
        $connectionArray['LastUpdatedDevices'] = gmdate("Y-m-d\TH:i:s.000\Z", $connectionArray['LastUpdatedDevices']);
        $connectionArray['LastUpdatedSessionId'] = gmdate("Y-m-d\TH:i:s.000\Z", $connectionArray['LastUpdatedSessionId']);
        $connectionListArray[$key] = $connectionArray;
    }

    $customer->TTL = strtotime('+3 months');
    
	global $awsConfig;

	$marshaler = new Marshaler();

	$item = array(
		"TableName" => "FBSmartHome",
		"Key" => array("CustomerId" => array("S" => trim($customer->CustomerId))),

		"UpdateExpression" => "SET 	#isgoogleassistant = :isgoogleassistant, #userregion = :userregion,
									#devicelist = :devicelist, #connectionlist = :connectionlist, #ttl = :ttl,
                                    #accesstoken = :accesstoken, #refreshtoken = :refreshtoken, #tokenvalidity = :tokenvalidity,
                                    #apikey = :apikey",
//                            REMOVE  BlockedUntil, FritzBoxPassword, FritzBoxSessionId, FritzBoxUrl, FritzBoxUsername, VirtualDevices
//                                    LastConnectDuration, LastConnectionState, LastUpdatedDevices, LastUpdatedSessionId, Legacy, UseIPv6, ConnectionMethod",

		"ExpressionAttributeNames" => array("#isgoogleassistant" => "IsGoogleAssistant", "#userregion" => "UserRegion", 
                                            "#devicelist" => "DeviceList", "#connectionlist" => "ConnectionList", "#ttl" => "TTL",
                                            "#accesstoken" => "AccessToken", "#refreshtoken" => "RefreshToken", "#tokenvalidity" => "TokenValidity",
                                            "#apikey" => "ApiKey"
                                            ),

		"ExpressionAttributeValues" => array(":isgoogleassistant" => array("N" => ($customer->IsGoogleAssistant) ? "1" : "0"), 
                                             ":userregion" => array("S" => empty($customer->UserRegion) ? " " : $customer->UserRegion),
											 ":devicelist" => array("L" => $marshaler->marshalItem($customer->getDeviceListAsArray())),
                                             ":connectionlist" => array("L" => $marshaler->marshalItem($connectionListArray)),
                                             ":ttl" => array("N" => (string)$customer->TTL),
                                             ":accesstoken" => array("S" => empty($customer->AccessToken) ? " " : $customer->AccessToken), 
                                             ":refreshtoken" => array("S" => empty($customer->RefreshToken) ? " " : $customer->RefreshToken),
                                             ":tokenvalidity" => array("S" => gmdate("Y-m-d\TH:i:s.000\Z", $customer->TokenValidity)),
                                             ":apikey" => array("S" => empty($customer->ApiKey) ? " " : $customer->ApiKey) 
											)
	);

	$dbClient = new DynamoDbClient($awsConfig);
	try {
		$response = $dbClient->updateItem($item);
	} catch (DynamoDbException $e) {
		error_log($e->getMessage());
		return null;
	}

    unset($marshaler, $response, $item);

	return $customer;
}

function deleteCustomer($customer)
{
	global $awsConfig;

	$marshaler = new Marshaler();

	$item = array(
		"TableName" => "FBSmartHome",
		"Key" => array("CustomerId" => array("S" => trim($customer->CustomerId))),

		"UpdateExpression" => "REMOVE FritzBoxUrl, FritzBoxUsername, FritzBoxPassword, FritzBoxSessionId, UseIPv6, LastUpdatedDevices, LastUpdatedSessionId,
                                      LastConnectionState, Legacy, DeviceList, ConnectionList, BlockedUntil, ApiKey",

		"ExpressionAttributeValues" => array(":customerId" => array("S" => trim($customer->CustomerId))),

		"ConditionExpression" => "CustomerId = :customerId"
	);

	$dbClient = new DynamoDbClient($awsConfig);
	try {
		$response = $dbClient->updateItem($item);
	} catch (DynamoDbException $e) {
		if ($e->getAwsErrorCode() == 'ConditionalCheckFailedException')
		{
			return true;
		}
		error_log($e->getMessage());
		return false;
	}
    
    unset($marshaler, $response, $item);

	return true;
}

?>
