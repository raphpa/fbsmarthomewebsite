<?php
global $basedir;
require_once($basedir . '/fritzbox/auxiliary.inc.php');

class Device
{
	public $Identifier = "";
	public $Id = "";
	public $IsGroup = false;
	public $GroupMembers = array();
	public $Firmware = "";
	public $Manufacturer = "";
	public $ProductName = "";
	public $DeviceClasses = array();
	public $Name = "";
	public $Present = false;
	public $Locked = false;
	public $BatteryLow = false;
	public $Alert = false;
	public $SwitchedOn = false;
	public $OperationMode = MODE_NONE;
	public $Temperature = 0.0;
	public $TemperatureActualValue = 0.0;
	public $TemperatureSetpoint = 0.0;
	public $TemperatureSetback = 0.0;
	public $TemperatureComfort = 0.0;
    public $TemperatureOff = TEMPERATURE_OFF;
    public $TemperatureOn = TEMPERATURE_COMFORT;
	public $Power = 0.0;
	public $Energy = 0.0;
    public $ConnectionNumber = CONNECTION_DEFAULT;
    public $AdditionalSwitch = false;

	function __toString() {
		return $this->Name;
	}

	function addDeviceClass($deviceClass)
	{
		if(!$this->isDeviceClass($deviceClass))
		{
			$this->DeviceClasses[] = $deviceClass;
		}
	}

	function removeDeviceClass($deviceClass)
	{
		$position = array_search($deviceClass, $this->DeviceClasses);
		if ($position !== false)
		{
			unset($this->DeviceClasses[$position]);
			$this->deviceClasses = array_values($this->DeviceClasses);
		}
	}

	function isDeviceClass($deviceClass)
	{
		return in_array($deviceClass, $this->DeviceClasses);
	}
}

function createDevice($deviceArray)
{
    $device = new Device();
    $device->Identifier = array_key_exists('Identifier', $deviceArray) ? trim($deviceArray['Identifier']) : "";
    $device->Id = array_key_exists('Id', $deviceArray) ? trim($deviceArray['Id']) : "";
    $device->IsGroup = array_key_exists('IsGroup', $deviceArray) ? boolval($deviceArray['IsGroup']) : false;
    $device->GroupMembers = (array_key_exists('GroupMembers', $deviceArray) && is_array($deviceArray['GroupMembers'])) ? $deviceArray['GroupMembers'] : array();
    $device->Firmware = array_key_exists('Firmware', $deviceArray) ? trim($deviceArray['Firmware']) : "";
    $device->Manufacturer = array_key_exists('Manufacturer', $deviceArray) ? trim($deviceArray['Manufacturer']) : "";
    $device->ProductName = array_key_exists('ProductName', $deviceArray) ? trim($deviceArray['ProductName']) : "";
    $device->DeviceClasses = (array_key_exists('DeviceClasses', $deviceArray) && is_array($deviceArray['DeviceClasses'])) ? $deviceArray['DeviceClasses'] : array();
    $device->Name = array_key_exists('Name', $deviceArray) ? trim($deviceArray['Name']) : "";
    $device->Present = array_key_exists('Present', $deviceArray) ? boolval($deviceArray['Present']) : false;
    $device->Locked = array_key_exists('Locked', $deviceArray) ? boolval($deviceArray['Locked']) : false;
    $device->BatteryLow = array_key_exists('BatteryLow', $deviceArray) ? boolval($deviceArray['BatteryLow']) : false;
    $device->Alert = array_key_exists('Alert', $deviceArray) ? boolval($deviceArray['Alert']) : false;
    $device->SwitchedOn = array_key_exists('SwitchedOn', $deviceArray) ? boolval($deviceArray['SwitchedOn']) : false;
    $device->OperationMode = array_key_exists('OperationMode', $deviceArray) ? intval($deviceArray['OperationMode']) : MODE_NONE;
    $device->Temperature = array_key_exists('Temperature', $deviceArray) ? floatval($deviceArray['Temperature']) : 0.0;
    $device->TemperatureActualValue = array_key_exists('TemperatureActualValue', $deviceArray) ? floatval($deviceArray['TemperatureActualValue']) : 0.0;
    $device->TemperatureSetpoint = array_key_exists('TemperatureSetpoint', $deviceArray) ? floatval($deviceArray['TemperatureSetpoint']) : 0.0;
    $device->TemperatureSetback = array_key_exists('TemperatureSetback', $deviceArray) ? floatval($deviceArray['TemperatureSetback']) : 0.0;
    $device->TemperatureComfort = array_key_exists('TemperatureComfort', $deviceArray) ? floatval($deviceArray['TemperatureComfort']) : 0.0;
    $device->TemperatureOff = array_key_exists('TemperatureOff', $deviceArray) ? floatval($deviceArray['TemperatureOff']) : TEMPERATURE_OFF;
    $device->TemperatureOn = array_key_exists('TemperatureOn', $deviceArray) ? floatval($deviceArray['TemperatureOn']) : TEMPERATURE_COMFORT;
    $device->Power = array_key_exists('Power', $deviceArray) ? floatval($deviceArray['Power']) : 0.0;
    $device->Energy = array_key_exists('Energy', $deviceArray) ? floatval($deviceArray['Energy']) : 0.0;
    $device->ConnectionNumber = array_key_exists('ConnectionNumber', $deviceArray) ? intval($deviceArray['ConnectionNumber']) : CONNECTION_DEFAULT;
    $device->AdditionalSwitch = array_key_exists('AdditionalSwitch', $deviceArray) ? boolval($deviceArray['AdditionalSwitch']) : false;
    
    return $device;
}
?>