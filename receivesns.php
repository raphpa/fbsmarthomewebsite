<?php
$basedir = realpath(__DIR__);

if ('POST' !== $_SERVER['REQUEST_METHOD']) {
    http_response_code(405);
    die;
}

$_SERVER['HTTP_X_AMZ_SNS_MESSAGE_TYPE'] = "x-amz-sns-message-type";

require_once($basedir . '/vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\Exception\InvalidSnsMessageException;



// Validate the message and log errors if invalid.
try {
    $message = Message::fromRawPostData();
    $validator = new MessageValidator();
    $validator->validate($message);
} catch (Exception $e) {
   // Pretend we're not here if the message is invalid.
   http_response_code(404);
   error_log("SNS Message Validation Error: " . $e->getMessage());
   die();
}

// Check the type of the message and handle the subscription.
if ($message['Type'] === 'SubscriptionConfirmation' || $message['Type'] === 'UnsubscribeConfirmation') {
   // Confirm the subscription by sending a GET request to the SubscribeURL
   file_get_contents($message['SubscribeURL']);
}
elseif ($message['Type'] === 'Notification') {
    // Get message
    $queueMessage = array();
    if($message['Message'] === "GetDeviceList")
    {
        $queueMessage['message'] = $message['Message'];
        $queueMessage['customer'] = $message['MessageAttributes']['customer']['Value'];
    }
    elseif($message['Message'] === "TurnOnDevice" || $message['Message'] === "TurnOffDevice")
    {
        $queueMessage['message'] = $message['Message'];
        $queueMessage['customer'] = $message['MessageAttributes']['customer']['Value'];
        $queueMessage['device'] = $message['MessageAttributes']['device']['Value'];
    }
    elseif($message['Message'] === "SetTemperature")
    {
        $queueMessage['message'] = $message['Message'];
        $queueMessage['customer'] = $message['MessageAttributes']['customer']['Value'];
        $queueMessage['device'] = $message['MessageAttributes']['device']['Value'];
        $queueMessage['temperature'] = $message['MessageAttributes']['temperature']['Value'];
    }
    elseif($message['Message'] === "MultipleCommands")
    {
        $queueMessage['message'] = $message['Message'];
        $queueMessage['customer'] = $message['MessageAttributes']['customer']['Value'];
        $queueMessage['commandList'] = $message['MessageAttributes']['commandList']['Value'];
    }
    else
    {
        error_log("No valid smart home message");
        http_response_code(200);
        die();
    }
    
    // Send message to rabbitmq
    $mqMessage = new AMQPMessage(json_encode($queueMessage));
    $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
    $channel = $connection->channel();
    $channel->queue_declare('fbsmarthome', false, false, false, false);
    $channel->basic_publish($mqMessage, '', 'fbsmarthome');
    $channel->close();
    $connection->close();
}
?>