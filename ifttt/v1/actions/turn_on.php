<?php
    $basedir = realpath($_SERVER['DOCUMENT_ROOT']);
    require_once($basedir . '/vendor/autoload.php');
    require_once($basedir . '/fritzbox/auxiliary.inc.php');
    require_once($basedir . '/fritzbox/postgresql.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
	require_once($basedir . '/fritzbox/device.inc.php');
    require_once($basedir . '/fritzbox/connection.inc.php');
    
    function getAuthorizationHeader(){
            $headers = null;
            if (isset($_SERVER['Authorization'])) {
                $headers = trim($_SERVER["Authorization"]);
            }
            else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
                $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
            } elseif (function_exists('apache_request_headers')) {
                $requestHeaders = apache_request_headers();
                // Server-side fix for bug in old Android versions
                $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
                if (isset($requestHeaders['Authorization'])) {
                    $headers = trim($requestHeaders['Authorization']);
                }
            }
            return $headers;
        }

    function getBearerToken() {
        $headers = getAuthorizationHeader();
        // Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    $bearerToken = getBearerToken();
    
    // If no token was transmitted, exit with 401
    if ($bearerToken == null)
    {
        http_response_code(401);
        
        $jsonarray = array(
            "errors" => array(array(
                "message" => "No token was transmitted"
            ))
        );
    
        echo json_encode($jsonarray);
        exit;
    }
    
    // decode request
    $request = json_decode(file_get_contents("php://input"), true);
    
    if(!isset($request["actionFields"]))
    {
        // actionFields is missing
        http_response_code(400);
         
        $jsonarray = array(
            "errors" => array(array(
                "message" => "ActionFields is missing"
            ))
        );

        echo json_encode($jsonarray);
        exit;  
    }
    elseif(!isset($request["actionFields"]["smart_home_device"]))
    {
        // smart_home_device is missing
        http_response_code(400);
         
        $jsonarray = array(
            "errors" => array(array(
                "message" => "Smart Home Device is missing"
            ))
        );

        echo json_encode($jsonarray);
        exit;  
    }
    
    // If token is test token, do action on test device
    if ($bearerToken == "Testtoken")
    {
        
        if($request["actionFields"]["smart_home_device"] != "Testid")
        {
            // Device ist not test device
            http_response_code(400);
             
            $jsonarray = array(
                "errors" => array(array(
                    "message" => "Device not found"
                ))
            );
    
            echo json_encode($jsonarray);
            exit;  
        }
        
        $jsonarray = array(
            "data" => array(array(
                "id" => $request["actionFields"]["smart_home_device"] 
            ))
        );
        
        echo json_encode($jsonarray);
        exit;
    }

    $url = 'https://api.amazon.com/auth/o2/tokeninfo?access_token=' . urlencode($bearerToken);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 13); 
   	curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    
    $result = curl_exec($ch);
    $decoded = json_decode($result);
    $httpCode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
    
    curl_close($ch);
    if ($httpCode != 200)
    {
        //If response code is not 200, generate error 401
        http_response_code(401);
        
        $jsonarray = array(
            "errors" => array(array(
                "message" => "Could not get access token"
            ))
        );
    
        echo json_encode($jsonarray);
        exit;
    }
    // verify that the access token belongs to us
    elseif (isset($decoded->aud) && $decoded->aud == $_SERVER['CLIENT_ID']) {
        //if it does, return information
        $customerId = $decoded->user_id;
        
        //load user from database
        $customer = getCustomerRDS($customerId);
        
        // check if customer exist
        if($customer == null)
        {
            http_response_code(401);
            
            $jsonarray = array(
                "errors" => array(array(
                    "message" => "Customer not in database"
                ))
            );
        
            echo json_encode($jsonarray);
            exit;
        }
        // check if device exists
        elseif(!$customer->hasDevice($request["actionFields"]["smart_home_device"]))
        {
            // Device not found
            http_response_code(400);
             
            $jsonarray = array(
                "errors" => array(array(
                    "message" => "Device not found"
                ))
            );
    
            echo json_encode($jsonarray);
            exit;  
        }
        
        // publish request to RabbitMQ
        $jsonarray = array(
            "message" => "TurnOnDevice",
            "customer" => $customerId,
            "device" => $request["actionFields"]["smart_home_device"]
        );
        
        $message = new PhpAmqpLib\Message\AMQPMessage(json_encode($jsonarray),array('expiration' => 60000));
        $connection = new PhpAmqpLib\Connection\AMQPStreamConnection('rabbitmq', 5672, $_SERVER['MQ_USER'], $_SERVER['MQ_PASS'], '/', false, 'AMQPLAIN', null, 'en_US', 3.0, 120.0, null, false, 60);
        $channel = $connection->channel();
        $channel->basic_publish($message, 'fbsmarthome-exchange', 'fbsmarthome');
        $channel->close();
        $connection->close();
        
        // All OK, return device id
        $jsonarray = array(
            "data" => array(array(
                "id" => $request["actionFields"]["smart_home_device"]
            ))
        );
        
        echo json_encode($jsonarray);
        exit;
    }  
    else
    {
        // If anything went wrong, return 401
        http_response_code(401);
        
        $jsonarray = array(
            "errors" => array(array(
                "message" => "Could not turn on device"
            ))
        );
    
        echo json_encode($jsonarray);
        exit;
    }
?>
