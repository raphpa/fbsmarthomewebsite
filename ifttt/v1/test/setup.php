<?php

//If any error occurs, return error 503
http_response_code(503);

if(!isset($_SERVER['HTTP_IFTTT_SERVICE_KEY']) || $_SERVER['HTTP_IFTTT_SERVICE_KEY'] != $_SERVER['IFTTT_SERVICE_KEY'])
{
    //If service key does not match, return error 401
    http_response_code(401);
    
    $jsonarray = array(
        "errors" => array(array(
            "message" => "Service key does not match"
        ))
    );

    echo json_encode($jsonarray);
    exit;
}
else
{
    http_response_code(200);
    
    $jsonarray = array(
        "data" => array(
            "accessToken" => "Testtoken",
            "samples" => array(
                "actions" => array(
                    "turn_on" => array(
                        "smart_home_device" => "Testid"
                    ),
                    "turn_off" => array(
                        "smart_home_device" => "Testid"
                    ),
                    "set_temperature" => array(
                        "smart_home_device" => "Testid",
                        "temperature" => "22"
                    ),
                    "apply_template" => array(
                        "template" => "Testid"
                    )
                )
            )
        )
    );
    
    echo json_encode($jsonarray);
    exit;
}

?>
