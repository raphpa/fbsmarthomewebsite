<?php
$basedir = realpath($_SERVER['DOCUMENT_ROOT']);
require_once($basedir . '/vendor/autoload.php');

//If any error occurs, return error 503
http_response_code(503);

if(!isset($_SERVER['HTTP_IFTTT_SERVICE_KEY']) || $_SERVER['HTTP_IFTTT_SERVICE_KEY'] != $_SERVER['IFTTT_SERVICE_KEY'])
{
    //If service key does not match, return error 401
    http_response_code(401);
        
    $jsonarray = array(
        "errors" => array(array(
            "message" => "Service key does not match"
        ))
    );

    echo json_encode($jsonarray);
    exit;
}
else
{
    // Try to connect to RabbitMQ
    try
    {
        $connection = new PhpAmqpLib\Connection\AMQPStreamConnection('rabbitmq', 5672, $_SERVER['MQ_USER'], $_SERVER['MQ_PASS'], '/', false, 'AMQPLAIN', null, 'en_US', 3.0, 120.0, null, false, 60);
        $channel = $connection->channel();
        $channel->close();
        $connection->close();
    }
    catch(Exception $e)
    {
        http_response_code(503);   
        
        $jsonarray = array(
            "errors" => array(array(
                "message" => "RabbitMQ is offline"
            ))
        );

        echo json_encode($jsonarray);
        exit;
    }

    
    http_response_code(200);
}

?>
