<?php
    $basedir = realpath(__DIR__);
    
	require_once($basedir . '/fritzbox/auxiliary.inc.php');
	require_once($basedir . '/fritzbox/customer.inc.php');
	require_once($basedir . '/fritzbox/postgresql.inc.php');

    header("X-Robots-Tag: noindex", true);
    
    if(empty($_SERVER['HTTP_X_REQUESTED_WITH']))
	{
		return;
	}
    
    session_set_cookie_params(0, "/", "", true, true);
	session_start();
    
    $connectionNumber = intval(filter_input(INPUT_POST,"connectionNumber",FILTER_SANITIZE_NUMBER_INT));
    if($connectionNumber >= CONNECTION_DEFAULT && $connectionNumber <= CONNECTION_MAX)
    {
        $_SESSION['connectionNumber'] = $connectionNumber;
    }
    
    if(!isset($_SESSION['customer']) || !is_int($_SESSION['connectionNumber']) || !$_SESSION['customer']->isConnection($_SESSION['connectionNumber']))
    {
        $_SESSION['connectionNumber'] = CONNECTION_DEFAULT;
    }
    
    if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "addConnection")
    {
        if($_SESSION['customer']->getConnection(CONNECTION_DEFAULT)->LastConnectionState != CONN_OK)
        {
            echo "0";
            return;
        }
        // Find next free connection
        for ($i = CONNECTION_DEFAULT + 1; $i <= CONNECTION_MAX; $i++)
        {
            if (!$_SESSION['customer']->isConnection($i))
            {
                $connection = new Connection();
                $connection->Number = $i;
                $connection->LastConnectionState = CONN_NOTCONFIGURED;
                $_SESSION['customer']->ConnectionList[] = $connection;
                $_SESSION['connectionNumber'] = $i;
                echo strval($i);
                return;
            }
        }
        echo "0";
        return;
    }
    
    if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "deleteConnection")
    {
        $connectionNumber = intval(filter_input(INPUT_POST,"connection",FILTER_SANITIZE_NUMBER_INT));
        
        if($connectionNumber == $_SESSION['connectionNumber'] 
            && $connectionNumber > CONNECTION_DEFAULT 
            && $connectionNumber <= CONNECTION_MAX)
        {
            $_SESSION['customer']->removeDevices($connectionNumber);
            $_SESSION['customer']->removeConnection($connectionNumber);
            $_SESSION['connectionNumber'] = CONNECTION_DEFAULT;
            echo strval($connectionNumber);
            return;
        }
        echo "0";
        return;
    }
    
    if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "generateApiKey")
    {
        $_SESSION['customer']->ApiKey = bin2hex(openssl_random_pseudo_bytes(8));
        echo trim($_SESSION['customer']->ApiKey);
        return;
    }
?>    

<?php
    //Set aws config
    $awsConfig = [
      'region'  => 'eu-west-1',
      'version' => 'latest',
      'http'    => [
            'connect_timeout' => 15,
            'timeout' => 15
        ]
    ];

	if(filter_input(INPUT_POST,"action",FILTER_SANITIZE_STRING) == "login") {
		//Get authorization
        $token = filter_input(INPUT_POST,"accessToken",FILTER_SANITIZE_STRING);
        $state = filter_input(INPUT_POST,"state",FILTER_SANITIZE_STRING);
        $code = filter_input(INPUT_POST,"code",FILTER_SANITIZE_STRING);
        
        //Token was Amazon token
        if (strpos($token, "Atza") === 0) {
            $url = 'https://api.amazon.com/auth/o2/tokeninfo?access_token=' . urlencode($token);
            $result = file_get_contents($url); 
            $decoded = json_decode($result);
        
            // verify that the access token belongs to us
            if ($decoded->aud == $_SERVER['CLIENT_ID'] && isset($_SESSION['state']) && $state == $_SESSION['state']) {
                //if it does, save user_id to session
                $_SESSION['customerId'] = $decoded->user_id;
            }  
        }
        elseif(strpos($code, "LWA") === 0)
        {
            // Code from LWA
            $url = 'https://api.amazon.com/auth/o2/token/';
            $data = array(
                'grant_type' => 'authorization_code',
                'client_id' => $_SERVER['CLIENT_ID'],
                'client_secret' => $_SERVER['CLIENT_SECRET'],
                'code' => substr($code, 4)
             );
             $options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded;charset=UTF-8\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($data)
                )
            );
            $context  = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            $decoded = json_decode($result);

            // Got access token
             if (isset($decoded->access_token) && strpos($decoded->access_token, "Atza") === 0) {
                $url = 'https://api.amazon.com/auth/o2/tokeninfo?access_token=' . urlencode($decoded->access_token);
                $result = file_get_contents($url); 
                $decoded = json_decode($result);
                
                // verify that the access token belongs to us
                if ($decoded->aud == $_SERVER['CLIENT_ID'] && isset($_SESSION['state']) && $state == $_SESSION['state']) {
                    //if it does, save user_id to session
                    $_SESSION['customerId'] = $decoded->user_id;
                }  
             }
        }
	}
    
	if(filter_input(INPUT_GET,"action",FILTER_SANITIZE_STRING) == "logout") {
		$_SESSION = array();
	}

	if(isset($_SESSION['customerId'])) {
		//User is logged in, check for session data
		//If data is not yet available, try to pull from DynamoDB
		if(!isset($_SESSION['customer']))
		{

			$customer = getCustomerRDS($_SESSION['customerId']);
				
			if ($customer == null)
			{
				?>
				<script>
					$("#error").html('<div class="alert alert-danger alert-dismissable fade show">' + 
								     '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + 
									 '<strong>Fehler!</strong><br>Daten konnten nicht korrekt aus Datenbank gelesen werden.</div>');
				</script>				
				<?php
			}
			else
			{
				$_SESSION['customer'] = $customer;
			}
		}
		
?>
<div class="row">
	<div class="col-11 mx-auto">
            Verbindung
            <!-- Nav tabs -->
            <nav>      
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
<?php
            // sort by connection number
            usort($_SESSION['customer']->ConnectionList, function($a, $b) 
            {
                return $a->Number <=> $b->Number;
            });
            
            foreach($_SESSION['customer']->ConnectionList as $connection)
            {
?>
                <a class="nav-item nav-link <?php echo ($_SESSION['connectionNumber'] == $connection->Number) ? 'active' : '';?>" 
                    data-toggle="tab" href="#nav" data-connection="<?php echo $connection->Number; ?>"><?php echo $connection->Number; ?>
<?php
                    if($connection->Number > CONNECTION_DEFAULT && $connection->Number == $_SESSION['connectionNumber'])
                    {
?>
                        <button class="btn btn-secondary btn-sm ml-3" id="btn-delete-connection" type="button" 
                            data-connection="<?php echo $connection->Number; ?>">
                        -</button>
<?php
                    }
?>
                </a>
<?php
                }                
?>
<?php
            if(count($_SESSION['customer']->ConnectionList) <= (CONNECTION_MAX - CONNECTION_DEFAULT))
            {
?>
                <button id="btn-add-connection" type="button" class="btn btn-primary ml-5 mb-1 mt-1">+</button>
<?php
            }
?>
              </div>            
            </nav>
		<form id="userdata" class="mb-4 mt-4">
		  <div class="form-group">
			<label class="col-form-label mr-3" for="customerId">Benutzer-ID</label>
			<button type="button" class="btn btn-danger clickable mb-2" id="logout">Abmelden</button>
			<div class="input-group">
				<input type="text" class="form-control" id="customerId" value="<?php echo htmlspecialchars($_SESSION['customerId']) ?>" readonly>
				<script>
				$('#logout').click(function(event) {
					$( "#usersettings" ).load( "usersettings.php?action=logout" );
                    $( "#devices" ).html('');
                    //auth.signOut();
                    amazon.Login.logout();
				});
				</script>
			</div>
		  </div>

			<label class="col-form-label" for="fritzboxAdress">MyFRITZ!-Adresse <small>(https://xxx.myfritz.net:4xxx)</small></label>
			<div class="input-group">
				<input type="text" class="form-control" id="fritzboxAdress" value="<?php if (isset($_SESSION['customer'])) { echo htmlspecialchars(trim($_SESSION['customer']->getConnection($_SESSION['connectionNumber'])->FritzBoxUrl)); } ?>">
				<div class="input-group-append">
					<span class="input-group-text">
						<a href="img/user1.png" tabindex="-1" class="js-smartphoto2" data-group="1" id="user1"><img src="img/user1.png" width="0" alt="MyFritz-Adresse der FritzBox"/><span style="font-size: 1.5rem;"class=" far fa-question-circle fa-lg"></span></a>
					</span>
				</div>
			</div>

			<label class="col-form-label" for="fritzboxUser">Benutzer</label>
			<div class="input-group">
				<input type="text" class="form-control" id="fritzboxUser" value="<?php if (isset($_SESSION['customer'])) { echo htmlspecialchars(trim($_SESSION['customer']->getConnection($_SESSION['connectionNumber'])->FritzBoxUsername)); } ?>">
				<div class="input-group-append">
					<span class="input-group-text">
						<a href="img/user2.png" tabindex="-1" class="js-smartphoto2" data-group="1" id="user2"><img src="img/user2.png" width="0" alt="Username für Alexa-Benutzer auf der FritzBox"/><span style="font-size: 1.5rem;" class="far fa-question-circle fa-lg"></span></a>
					</span>
				</div>
			</div>
		 
			<label class="col-form-label" for="fritzboxPassword">Passwort</label>
			<div class="input-group">
				<input type="text" class="form-control" id="fritzboxPassword" value="<?php if (isset($_SESSION['customer'])) { echo htmlspecialchars(trim($_SESSION['customer']->getConnection($_SESSION['connectionNumber'])->FritzBoxPassword)); } ?>">
				<div class="input-group-append">
					<span class="input-group-text">
						<a href="img/user3.png" tabindex="-1" class="js-smartphoto2" data-group="1" id="user3"><img src="img/user3.png" width="0" alt="Passwort für Alexa-Benutzer auf der FritzBox"/><span style="font-size: 1.5rem;" class="far fa-question-circle fa-lg"></span></a>
					</span>
				</div>
			</div>

			<div class="row mt-3">
				<div class="col-sm-6 mb-5"><button id="fritzBoxTest" type="submit" class="btn btn-primary clickable">Verbindung testen</button></div>
			</div>
		</form>

		<script>
            $('#user1').attr('href', 'img/' + i18nextify.i18next.t('user1.png'));
            $('#user2').attr('href', 'img/' + i18nextify.i18next.t('user2.png'));
            $('#user3').attr('href', 'img/' + i18nextify.i18next.t('user3.png'));
        
			$('#deleteDataButton').show();
			$('#deleteDataText').show();
			
			$(".js-smartphoto2").SmartPhoto({
				nav: false
			});

            $(".nav-link").click(function() {
                $connection = $(this).data("connection");
    
                if ($connection == "" || $connection == null || $connection == <?php echo $_SESSION['connectionNumber']; ?>)
                {
                    return;
                }
                
                $.ajax({
                    url: '/usersettings.php',
                    type: 'post',
                    data: {
                        'connectionNumber': $connection
                    },
                    success: function (data, status) {
                        $( "#usersettings" ).load( "usersettings.php" );
                    },
                    error: function (xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                    }
                }); // end .ajax
            });
            
            $("#btn-add-connection").click(function() {

                $.ajax({
                    url: '/usersettings.php',
                    type: 'post',
                    data: {
                        'action': 'addConnection'
                    },
                    success: function (data, status) {
                        if (data.trim() != "0")
                        {
                            $( "#usersettings" ).load( "usersettings.php" );
                        }
                    },
                    error: function (xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                    }
                }); // end .ajax
            });
            
            $("#btn-delete-connection").click(function() {
                $connection = $(this).data("connection");
    
                if ($connection == "" || $connection == null || $connection != <?php echo $_SESSION['connectionNumber']; ?>)
                {
                    return;
                }
                
                $.ajax({
                    url: '/usersettings.php',
                    type: 'post',
                    data: {
                        'action': 'deleteConnection',
                        'connection': $connection
                    },
                    success: function (data, status) {
                        if (data.trim() != "0")
                        {
                            $( "#usersettings" ).load( "usersettings.php" );
                            $( "#devices" ).load( "createdevicelist.php" );
                        }
                    },
                    error: function (xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                    }
                }); // end .ajax
            });

		  $('#userdata').submit(function(event) {
			  $('#fritzBoxAlert').hide();
              $('#passwordError').hide();
              $('#fritzBoxOK4').hide();
              $('#fritzBoxOK6').hide();
			  $('#fritzBoxTest').prop('disabled', true);
			  $('#fritzBoxTest').html(i18nextify.i18next.t('Wird getestet...'));
			  event.preventDefault();
			  $( "#devices" ).html('');
			  $.ajax({
					url: '/fritzboxtest.php',
					type: 'post',
					data: {
						'customerId' : $("#customerId").val(),
						'fritzBoxUrl' : $("#fritzboxAdress").val(),
						'fritzBoxUsername' : $("#fritzboxUser").val(),
						'fritzBoxPassword' : $("#fritzboxPassword").val(),
                        'connectionNumber' : <?php echo $_SESSION['connectionNumber']; ?>
					},
				success: function (data, status) {
						console.log(data);
						if (data.trim() != "OK4" && data.trim() != "OK6")
						{
							$('#fritzBoxAlertMessage').html('<strong>Fehler!</strong><br>' + i18nextify.i18next.t(data.trim()));
							$('#fritzBoxOK4').hide();
                            $('#fritzBoxOK6').hide();
							$('#fritzBoxAlert').fadeIn();
						} else {

							$('#fritzBoxAlert').hide();
							if (data.trim() == "OK6")
							{
								$('#fritzBoxOK6').fadeIn();
							} else {
								$('#fritzBoxOK4').fadeIn();
							}
							$( "#devices" ).load( "createdevicelist.php" );
                            
                            $.ajax({
                                url: '/checkpassword.php',
                                type: 'post',
                                success: function (data, status) {
                                    console.log(data);
                                    if (data.trim() != "0")
                                    {
                                        $('#passwordError').fadeIn();
                                    }
                                }
                            }); // end .ajax
						}

						setTimeout(function() {
								$('#fritzBoxTest').prop('disabled', false);
								$('#fritzBoxTest').html(i18nextify.i18next.t('Verbindung testen'));
							}, 1000);
				},
				error: function (xhr, desc, err) {
					$('#fritzBoxTest').prop('disabled', false);
					$('#fritzBoxTest').html(i18nextify.i18next.t('Verbindung testen'));
				}
			  }); // end .ajax
		  });
		</script>
		<div id="fritzBoxAlert" class="alert alert-danger hidden" role="alert">
			<button type="button" class="close" onclick="$('#fritzBoxAlert').fadeOut();" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div id="fritzBoxAlertMessage"></div>
		</div>
        <div id="passwordError" class="alert alert-warning hidden" role="alert">
			<button type="button" class="close" onclick="$('#passwordError').fadeOut();" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div id="passwordErrorMessage"><strong>Dieses Passwort ist in der Vergangenheit während eines <a href="https://haveibeenpwned.com/PwnedWebsites" target="_blank">Datenlecks im Internet</a> bekannt geworden und sollte nicht mehr genutzt werden!
            Wenn Sie es bereits zuvor verwendet haben, sollten Sie es ändern.</strong><br><br><small>Es handelt sich <strong>nicht</strong> um ein Datenleck seitens FB Smart Home</small></div>
		</div>
		<div id="fritzBoxOK4" class="alert alert-success hidden" role="alert">
			<button type="button" class="close" onclick="$('#fritzBoxOK4').fadeOut();" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<div id="fritzBoxOKMessage4"><strong>Verbindung erfolgreich</strong></div>
		</div>
		<div id="fritzBoxOK6" class="alert alert-success hidden" role="alert">
			<button type="button" class="close" onclick="$('#fritzBoxOK6').fadeOut();" aria-label="Close">
					<span aria-hidden="true">&times;</span>
			</button>
			<div id="fritzBoxOKMessage6"><strong>Verbindung erfolgreich</strong> über IPv6</div>
		</div>
	</div>
</div>
<?php
			
	} else {
        $state = bin2hex(openssl_random_pseudo_bytes(35));
        $_SESSION['state'] = $state;
?>
<div class="row">
    <a id="login" style="padding-top: 70px; margin-top: -70px;"></a>
	<div class="mx-auto text-center">
		<p>Um Skill-Einstellungen vornehmen zu können, melden Sie sich bitte mit Ihren Amazon Zugangsdaten an<br>
		<strong>Stellen Sie sicher, dass dies die gleichen Zugangsdaten sind, die Sie auch für Alexa nutzen!</strong><br><br>
		Bitte nehmen Sie zur Kenntnis: Die Website nutzt <a href="#Datenschutz5">Session-Cookies</a> zum Schutz Ihrer Zugangsdaten</p>

		<a href="#" id="LoginWithOauth">
			<img border="0" alt="Login with Amazon"
			src="img/lwaBig.png"
			width="312" height="64" />
		</a>
		<script>
        /*
        document.getElementById('LoginWithOauth').onclick = function () {
            auth.getSession();
        }
        */

        document.getElementById('LoginWithOauth').onclick = function () {
            options = {
                scope: 'profile:user_id',
                state: '<?php echo $state; ?>',
                response_type: 'code'
            };
            amazon.Login.authorize(options, function (response) {
                console.log(response);
                if (response.error) {
                    alert('Amazon OAuth Fehler: ' + response.error);
                    return;
                }
                $.ajax({
                    url: '/usersettings.php',
                    type: 'post',
                    data: {
                        'action': 'login',
                        'accessToken': response.access_token,
                        'state': response.state,
                        'code': 'LWA|' + response.code
                    },
                    success: function (data, status) {
                        $( "#usersettings" ).load( "usersettings.php" );

                    },
                    error: function (xhr, desc, err) {
                            console.log(xhr);
                            console.log("Details: " + desc + "\nError:" + err);
                    }
                }); // end .ajax
            });
            return false;
        };
        
		$('#deleteDataButton').hide();
		$('#deleteDataText').hide();

        var params = [];
        var query = window.location.hash.substr(1)
        var pairs = query.split('&');
        for(var i in pairs) {
            var couplet = pairs[i].split("=");
            params[couplet[0]] = couplet[1];
        }
        
        if(typeof params['access_token'] !== 'undefined')
        {
            $.ajax({
                url: '/usersettings.php',
                type: 'post',
                data: {
                    'action': 'login',
                    'accessToken': decodeURI(params['access_token'])
                },
                success: function (data, status) {
                    $( "#usersettings" ).load( "usersettings.php" );
                    
                    window.location.hash = 'loggedin';
                },
                error: function (xhr, desc, err) {
                        console.log(xhr);
                        console.log("Details: " + desc + "\nError:" + err);
                }
            }); // end .ajax         
        }
		</script>
	</div>
</div>
<?php
	
	}

?>